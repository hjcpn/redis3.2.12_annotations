# redis3.2.12源码注释

## 已完成部分(主要是单机相关的代码)

### 底层数据结构

* adlist.c
* dict.c
* intset.c
* quicklist.c
* sds.c
* ziplist.c

### redis对象系统以及数据类型的命令实现

* object.c
* t_hash.c
* t_list.c
* t_set.c
* t_string.c

### 网络部分

* ae.c
* ae_kqueue.c
* networking.c

### 其他

* 持久化(aof.c)
* 数据库相关(db.c)
* 事务(multi.c)
* 发布订阅(pubsub.c)
* 慢日志slowlog.c
* server.c部分代码

## TODO

### 跳跃表和有序集合

### rdb持久化

### 主从相关

### 集群相关


