/* Slowlog implements a system that is able to remember the latest N
 * queries that took more than M microseconds to execute.
 *
 * The execution time to reach to be logged in the slow log is set
 * using the 'slowlog-log-slower-than' config directive, that is also
 * readable and writable using the CONFIG SET/GET command.
 *
 * The slow queries log is actually not "logged" in the Redis log file
 * but is accessible thanks to the SLOWLOG command.
 *
 * ----------------------------------------------------------------------------
 *
 * Copyright (c) 2009-2012, Salvatore Sanfilippo <antirez at gmail dot com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Redis nor the names of its contributors may be used
 *     to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


#include "server.h"
#include "slowlog.h"

/* Create a new slowlog entry.
 * Incrementing the ref count of all the objects retained is up to
 * this function. */
//创建慢查询日志节点
slowlogEntry *slowlogCreateEntry(robj **argv, int argc, long long duration) {
    slowlogEntry *se = zmalloc(sizeof(*se));
    int j, slargc = argc;

    //命令参数个数不能超过SLOWLOG_ENTRY_MAX_ARGC
    if (slargc > SLOWLOG_ENTRY_MAX_ARGC) {
        slargc = SLOWLOG_ENTRY_MAX_ARGC;
    }

    se->argc = slargc;
    //创建命令参数对象数组
    se->argv = zmalloc(sizeof(robj *) * slargc);

    for (j = 0; j < slargc; j++) {
        /* Logging too many arguments is a useless memory waste, so we stop
         * at SLOWLOG_ENTRY_MAX_ARGC, but use the last argument to specify
         * how many remaining arguments there were in the original command. */
        //当命令参数太多时，只记录SLOWLOG_ENTRY_MAX_ARGC个参数
        //且最后一个参数为string对象，其内容为"XXX more arguments"，即还剩下多少命令没显示
        if (slargc != argc && j == slargc - 1) {
            se->argv[j] = createObject(OBJ_STRING,
                                       sdscatprintf(sdsempty(), "... (%d more arguments)",
                                                    argc - slargc + 1));
        }
        else {
            /* Trim too long strings as well... */
            //如果命令参数过长，也只记录最多SLOWLOG_ENTRY_MAX_STRING个字符
            //然后在结尾添加"XXX more bytes"，即还有多少没显示
            if (argv[j]->type == OBJ_STRING &&
                sdsEncodedObject(argv[j]) &&
                sdslen(argv[j]->ptr) > SLOWLOG_ENTRY_MAX_STRING) {
                sds s = sdsnewlen(argv[j]->ptr, SLOWLOG_ENTRY_MAX_STRING);
                s = sdscatprintf(s, "... (%lu more bytes)",
                                 (unsigned long) sdslen(argv[j]->ptr) - SLOWLOG_ENTRY_MAX_STRING);
                se->argv[j] = createObject(OBJ_STRING, s);
            }
            else {
                //否则正常记录，增加对象引用计数
                se->argv[j] = argv[j];
                incrRefCount(argv[j]);
            }
        }
    }

    //命令执行时间近似为记录时间？
    se->time = time(NULL);
    //命令执行时长
    se->duration = duration;
    //记录并更新日志id
    se->id = server.slowlog_entry_id++;

    return se;
}

/* Free a slow log entry. The argument is void so that the prototype of this
 * function matches the one of the 'free' method of adlist.c.
 *
 * This function will take care to release all the retained object. */
//用于释放慢查询日志节点
void slowlogFreeEntry(void *septr) {
    slowlogEntry *se = septr;
    int j;

    //减少命令参数对象的引用计数
    for (j = 0; j < se->argc; j++)
        decrRefCount(se->argv[j]);

    //释放命令参数数组
    zfree(se->argv);

    //释放节点本身
    zfree(se);
}

/* Initialize the slow log. This function should be called a single time
 * at server startup. */
//初始化慢查询链表和id
void slowlogInit(void) {
    server.slowlog = listCreate();
    server.slowlog_entry_id = 0;
    listSetFreeMethod(server.slowlog, slowlogFreeEntry);
}

/* Push a new entry into the slow log.
 * This function will make sure to trim the slow log accordingly to the
 * configured max length. */
//是否需要记录到慢查询日志？
void slowlogPushEntryIfNeeded(robj **argv, int argc, long long duration) {
    //慢查询选项没有开启
    if (server.slowlog_log_slower_than < 0) {
        return;
    } /* Slowlog disabled */

    //命令执行时间大于slowlog_log_slower_than，记录到慢查询日志
    if (duration >= server.slowlog_log_slower_than) {
        listAddNodeHead(server.slowlog, slowlogCreateEntry(argv, argc, duration));
    }

    /* Remove old entries if needed. */
    //日志链表先进先出，当超过slowlog_max_len时，删除最后一个日志节点
    while (listLength(server.slowlog) > server.slowlog_max_len)
        listDelNode(server.slowlog, listLast(server.slowlog));
}

/* Remove all the entries from the current slow log. */
//删除日志链表的所有节点
void slowlogReset(void) {
    while (listLength(server.slowlog) > 0)
        listDelNode(server.slowlog, listLast(server.slowlog));
}

/* The SLOWLOG command. Implements all the subcommands needed to handle the
 * Redis slow log. */
//命令实现
//SLOWLOG subcommand [argument]
void slowlogCommand(client *c) {
    //slowlog reset命令
    if (c->argc == 2 && !strcasecmp(c->argv[1]->ptr, "reset")) {
        slowlogReset();
        addReply(c, shared.ok);

        //slowlog len命令，返回记录条数
    }
    else if (c->argc == 2 && !strcasecmp(c->argv[1]->ptr, "len")) {
        addReplyLongLong(c, listLength(server.slowlog));

        //slowlog get [count]命令，返回count条日志
    }
    else if ((c->argc == 2 || c->argc == 3) && !strcasecmp(c->argv[1]->ptr, "get")) {
        long count = 10, sent = 0;
        listIter li;
        void *totentries;
        listNode *ln;
        slowlogEntry *se;

        //获取count
        if (c->argc == 3 && getLongFromObjectOrReply(c, c->argv[2], &count, NULL) != C_OK) {
            return;
        }

        //日志链表迭代器
        listRewind(server.slowlog, &li);

        //占位节点，要返回的日志条数，因为count可能大于现有记录条数
        totentries = addDeferredMultiBulkLength(c);

        while (count-- && (ln = listNext(&li))) {
            int j;

            se = ln->value;

            //[id, time, duration, commands]4个元素的数组
            addReplyMultiBulkLen(c, 4);

            //id, time, duration
            addReplyLongLong(c, se->id);
            addReplyLongLong(c, se->time);
            addReplyLongLong(c, se->duration);

            //命令数组长度
            addReplyMultiBulkLen(c, se->argc);

            //命令
            for (j = 0; j < se->argc; j++)
                addReplyBulk(c, se->argv[j]);

            sent++;
        }
        //将实际返回的日志条数写入占位节点
        setDeferredMultiBulkLength(c, totentries, sent);
    }
    else {
        //命令语法错误
        addReplyError(c, "Unknown SLOWLOG subcommand or wrong # of args. Try GET, RESET, LEN.");
    }
}
