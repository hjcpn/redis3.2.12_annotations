/*
 * Copyright (c) 2009-2012, Salvatore Sanfilippo <antirez at gmail dot com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Redis nor the names of its contributors may be used
 *     to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "server.h"
#include <math.h> /* isnan(), isinf() */

/*-----------------------------------------------------------------------------
 * String Commands
 *----------------------------------------------------------------------------*/

//检查要设置的字符串长度是否超过允许的范围
static int checkStringLength(client *c, long long size) {
    if (size > 512 * 1024 * 1024) {
        addReplyError(c, "string exceeds maximum allowed size (512MB)");
        return C_ERR;
    }
    return C_OK;
}

/* The setGenericCommand() function implements the SET operation with different
 * options and variants. This function is called in order to implement the
 * following commands: SET, SETEX, PSETEX, SETNX.
 *
 * 'flags' changes the behavior of the command (NX or XX, see belove).
 *
 * 'expire' represents an expire to set in form of a Redis object as passed
 * by the user. It is interpreted according to the specified 'unit'.
 *
 * 'ok_reply' and 'abort_reply' is what the function will reply to the client
 * if the operation is performed, or when it is not because of NX or
 * XX flags.
 *
 * If ok_reply is NULL "+OK" is used.
 * If abort_reply is NULL, "$-1" is used. */

#define OBJ_SET_NO_FLAGS 0
#define OBJ_SET_NX (1<<0)     /* Set if key not exists. */
#define OBJ_SET_XX (1<<1)     /* Set if key exists. */
#define OBJ_SET_EX (1<<2)     /* Set if time in seconds is given */
#define OBJ_SET_PX (1<<3)     /* Set if time in ms in given */

//set命令的底层实现
void setGenericCommand(client *c, int flags, robj *key, robj *val, robj *expire, int unit, robj *ok_reply,
                       robj *abort_reply) {
    long long milliseconds = 0; /* initialized to avoid any harmness warning */

    if (expire) {
        //尝试获取过期时间，写入milliseconds
        if (getLongLongFromObjectOrReply(c, expire, &milliseconds, NULL) != C_OK) {
            return;
        }

        //过期时间不能小于等于0
        if (milliseconds <= 0) {
            addReplyErrorFormat(c, "invalid expire time in %s", c->cmd->name);
            return;
        }

        //处理过期时间单位
        if (unit == UNIT_SECONDS) {
            milliseconds *= 1000;
        }
    }

    //命令带上了nx选项，但是key已经存在
    //或命令带上了xx选项，但是key不存在
    //则中断命令执行，添加相关回复信息到回复缓冲
    if ((flags & OBJ_SET_NX && lookupKeyWrite(c->db, key) != NULL) ||
        (flags & OBJ_SET_XX && lookupKeyWrite(c->db, key) == NULL)) {
        addReply(c, abort_reply ? abort_reply : shared.nullbulk);
        return;
    }

    //添加/更新键值对
    setKey(c->db, key, val);

    server.dirty++;

    //添加过期时间到expires数据库
    if (expire) {
        setExpire(c->db, key, mstime() + milliseconds);
    }

    notifyKeyspaceEvent(NOTIFY_STRING, "set", key, c->db->id);

    if (expire) {
        notifyKeyspaceEvent(NOTIFY_GENERIC,
                            "expire", key, c->db->id);
    }

    addReply(c, ok_reply ? ok_reply : shared.ok);
}

//命令实现
//SET key value [EX seconds] [PX milliseconds] [NX|XX]
void setCommand(client *c) {
    int j;
    robj *expire = NULL;

    //过期时间单位
    int unit = UNIT_SECONDS;

    //nx/xx标志
    int flags = OBJ_SET_NO_FLAGS;

    //处理ex/px/nx/xx参数
    for (j = 3; j < c->argc; j++) {
        char *a = c->argv[j]->ptr;

        //如果参数是ex/px，next保存过期时间
        robj *next = (j == c->argc - 1) ? NULL : c->argv[j + 1];

        if ((a[0] == 'n' || a[0] == 'N') &&
            (a[1] == 'x' || a[1] == 'X') && a[2] == '\0' &&
            !(flags & OBJ_SET_XX)) {
            flags |= OBJ_SET_NX;
        }
        else if ((a[0] == 'x' || a[0] == 'X') &&
                 (a[1] == 'x' || a[1] == 'X') && a[2] == '\0' &&
                 !(flags & OBJ_SET_NX)) {
            flags |= OBJ_SET_XX;
        }
        else if ((a[0] == 'e' || a[0] == 'E') &&
                 (a[1] == 'x' || a[1] == 'X') && a[2] == '\0' &&
                 !(flags & OBJ_SET_PX) && next) {
            flags |= OBJ_SET_EX;
            unit = UNIT_SECONDS;
            expire = next;
            j++;
        }
        else if ((a[0] == 'p' || a[0] == 'P') &&
                 (a[1] == 'x' || a[1] == 'X') && a[2] == '\0' &&
                 !(flags & OBJ_SET_EX) && next) {
            flags |= OBJ_SET_PX;
            unit = UNIT_MILLISECONDS;
            expire = next;
            j++;
        }
        else {
            //无法识别的参数
            addReply(c, shared.syntaxerr);
            return;
        }
    }

    //尝试将string对象编码类型由raw转换为embstr或int，节省内存
    c->argv[2] = tryObjectEncoding(c->argv[2]);

    setGenericCommand(c, flags, c->argv[1], c->argv[2], expire, unit, NULL, NULL);
}

//命令实现
//SETNX key value
void setnxCommand(client *c) {
    //尝试将string对象编码类型由raw转换为embstr或int，节省内存
    c->argv[2] = tryObjectEncoding(c->argv[2]);

    setGenericCommand(c, OBJ_SET_NX, c->argv[1], c->argv[2], NULL, 0, shared.cone, shared.czero);
}

//命令实现
//SETEX key seconds value
void setexCommand(client *c) {
    //尝试将string对象编码类型由raw转换为embstr或int，节省内存
    c->argv[3] = tryObjectEncoding(c->argv[3]);

    setGenericCommand(c, OBJ_SET_NO_FLAGS, c->argv[1], c->argv[3], c->argv[2], UNIT_SECONDS, NULL, NULL);
}

//命令实现
//PSETEX key milliseconds value
void psetexCommand(client *c) {
    //尝试将string对象编码类型由raw转换为embstr或int，节省内存
    c->argv[3] = tryObjectEncoding(c->argv[3]);

    setGenericCommand(c, OBJ_SET_NO_FLAGS, c->argv[1], c->argv[3], c->argv[2], UNIT_MILLISECONDS, NULL, NULL);
}

//get命令底层实现
//GET key
int getGenericCommand(client *c) {
    robj *o;

    //尝试获取key
    if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.nullbulk)) == NULL) {
        return C_OK;
    }

    if (o->type != OBJ_STRING) {
        //类型错误
        addReply(c, shared.wrongtypeerr);
        return C_ERR;
    }
    else {
        //否则添加val到回复缓冲
        addReplyBulk(c, o);
        return C_OK;
    }
}

//命令实现
//GET key
void getCommand(client *c) {
    getGenericCommand(c);
}

//命令实现
//GETSET key value
void getsetCommand(client *c) {
    if (getGenericCommand(c) == C_ERR) { return; }
    c->argv[2] = tryObjectEncoding(c->argv[2]);
    setKey(c->db, c->argv[1], c->argv[2]);
    notifyKeyspaceEvent(NOTIFY_STRING, "set", c->argv[1], c->db->id);
    server.dirty++;
}

//命令实现
//SETRANGE key offset value
void setrangeCommand(client *c) {
    robj *o;
    long offset;
    sds value = c->argv[3]->ptr;

    //获取offset
    if (getLongFromObjectOrReply(c, c->argv[2], &offset, NULL) != C_OK) {
        return;
    }

    //offset不合法
    if (offset < 0) {
        addReplyError(c, "offset is out of range");
        return;
    }

    //获取待setrange的对象
    o = lookupKeyWrite(c->db, c->argv[1]);

    //key不存在
    if (o == NULL) {
        /* Return 0 when setting nothing on a non-existing string */
        //要设置的value长度为0
        if (sdslen(value) == 0) {
            addReply(c, shared.czero);
            return;
        }

        /* Return when the resulting string exceeds allowed size */
        //检查要设置的字符串长度是否超过允许的范围
        if (checkStringLength(c, offset + sdslen(value)) != C_OK) {
            return;
        }

        //添加新的键值对
        //这里尚未写入value，要等if else结束后再写入
        o = createObject(OBJ_STRING, sdsnewlen(NULL, offset + sdslen(value)));
        dbAdd(c->db, c->argv[1], o);
    }
    else {
        //key已经存在

        size_t olen;

        /* Key exists, check type */
        //类型检查
        if (checkType(c, o, OBJ_STRING)) {
            return;
        }

        /* Return existing string length when setting nothing */
        //如果现有数据长度为0，返回长度0给用户
        olen = stringObjectLen(o);
        if (sdslen(value) == 0) {
            addReplyLongLong(c, olen);
            return;
        }

        /* Return when the resulting string exceeds allowed size */
        //检查要设置的字符串长度是否超过允许的范围
        if (checkStringLength(c, offset + sdslen(value)) != C_OK) {
            return;
        }

        /* Create a copy when the object is shared or encoded. */
        //确保val可以被修改
        o = dbUnshareStringValue(c->db, c->argv[1], o);
    }

    //写入value
    if (sdslen(value) > 0) {
        //使sds已使用的空间大小为len
        o->ptr = sdsgrowzero(o->ptr, offset + sdslen(value));
        //从offset开始写入value
        memcpy((char *) o->ptr + offset, value, sdslen(value));

        signalModifiedKey(c->db, c->argv[1]);
        notifyKeyspaceEvent(NOTIFY_STRING, "setrange", c->argv[1], c->db->id);
        server.dirty++;
    }

    //写入新的数据长度到回复缓冲
    addReplyLongLong(c, sdslen(o->ptr));
}

//命令实现
//GETRANGE key start end
void getrangeCommand(client *c) {
    robj *o;
    long long start, end;
    char *str, llbuf[32];
    size_t strlen;

    //获取start
    if (getLongLongFromObjectOrReply(c, c->argv[2], &start, NULL) != C_OK) {
        return;
    }

    //获取end
    if (getLongLongFromObjectOrReply(c, c->argv[3], &end, NULL) != C_OK) {
        return;
    }

    //key不存在或类型错误
    if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.emptybulk)) == NULL || checkType(c, o, OBJ_STRING)) {
        return;
    }

    if (o->encoding == OBJ_ENCODING_INT) {
        //int编码需要先转换保存的数据为字符串
        str = llbuf;
        strlen = ll2string(llbuf, sizeof(llbuf), (long) o->ptr);
    }
    else {
        str = o->ptr;
        strlen = sdslen(str);
    }

    /* Convert negative indexes */
    //处理负数start, end
    if (start < 0 && end < 0 && start > end) {
        addReply(c, shared.emptybulk);
        return;
    }
    if (start < 0) { start = strlen + start; }
    if (end < 0) { end = strlen + end; }
    if (start < 0) { start = 0; }
    if (end < 0) { end = 0; }
    if ((unsigned long long) end >= strlen) { end = strlen - 1; }

    /* Precondition: end >= 0 && end < strlen, so the only condition where
     * nothing can be returned is: start > end. */
    //start, end无效
    if (start > end || strlen == 0) {
        addReply(c, shared.emptybulk);
    }
    else {
        //添加[start, end]字符串到回复缓冲
        //sds本质是char*，这里直接使用char* str+ start和长度作为参数
        //复制到回复缓冲类似memcpy(dst, str + start, end - start + 1)
        addReplyBulkCBuffer(c, (char *) str + start, end - start + 1);
    }
}

//命令实现
//MGET key [key ...]
void mgetCommand(client *c) {
    int j;

    //添加回复数组的长度到回复缓冲
    addReplyMultiBulkLen(c, c->argc - 1);

    //遍历key，添加对应的val到回复缓冲
    for (j = 1; j < c->argc; j++) {
        robj *o = lookupKeyRead(c->db, c->argv[j]);
        if (o == NULL) {
            addReply(c, shared.nullbulk);
        }
        else {
            if (o->type != OBJ_STRING) {
                addReply(c, shared.nullbulk);
            }
            else {
                addReplyBulk(c, o);
            }
        }
    }
}

//mset底层实现函数
//MSET key value [key value ...]
void msetGenericCommand(client *c, int nx) {
    int j, busykeys = 0;

    //命令个数必定为奇数
    if ((c->argc % 2) == 0) {
        addReplyError(c, "wrong number of arguments for MSET");
        return;
    }

    /* Handle the NX flag. The MSETNX semantic is to return zero and don't
     * set nothing at all if at least one already key exists. */
    //如果是nx命令，只要有一个key已经存在，终止命令执行
    if (nx) {
        for (j = 1; j < c->argc; j += 2) {
            if (lookupKeyWrite(c->db, c->argv[j]) != NULL) {
                //todo：这里不立即break，大概是为了更新lru时间？
                busykeys++;
            }
        }

        //至少一个key已经存在
        if (busykeys) {
            addReply(c, shared.czero);
            return;
        }
    }

    //遍历添加
    for (j = 1; j < c->argc; j += 2) {
        //尝试将string对象编码类型由raw转换为embstr或int，节省内存
        c->argv[j + 1] = tryObjectEncoding(c->argv[j + 1]);
        //添加键值对
        setKey(c->db, c->argv[j], c->argv[j + 1]);
        notifyKeyspaceEvent(NOTIFY_STRING, "set", c->argv[j], c->db->id);
    }

    server.dirty += (c->argc - 1) / 2;
    addReply(c, nx ? shared.cone : shared.ok);
}

//命令实现
//MSET key value [key value ...]
void msetCommand(client *c) {
    msetGenericCommand(c, 0);
}

//命令实现
//MSETNX key value [key value ...]
void msetnxCommand(client *c) {
    msetGenericCommand(c, 1);
}

//incr/decr/incrby/decrby命令的底层实现
//DECRBY key decrement
void incrDecrCommand(client *c, long long incr) {
    long long value, oldvalue;
    robj *o, *new;

    o = lookupKeyWrite(c->db, c->argv[1]);
    //类型错误
    if (o != NULL && checkType(c, o, OBJ_STRING)) {
        return;
    }

    //获取key的val
    if (getLongLongFromObjectOrReply(c, o, &value, NULL) != C_OK) {
        return;
    }

    oldvalue = value;
    //检查incr后是否会溢出
    if ((incr < 0 && oldvalue < 0 && incr < (LLONG_MIN - oldvalue)) ||
        (incr > 0 && oldvalue > 0 && incr > (LLONG_MAX - oldvalue))) {
        addReplyError(c, "increment or decrement would overflow");
        return;
    }
    value += incr;

    if (o && o->refcount == 1 && o->encoding == OBJ_ENCODING_INT &&
        (value < 0 || value >= OBJ_SHARED_INTEGERS) &&
        value >= LONG_MIN && value <= LONG_MAX) {
        //原对象非共享对象且编码类型为int，直接修改原对象
        new = o;
        o->ptr = (void *) ((long) value);
    }
    else {
        //否则创建新的对象
        new = createStringObjectFromLongLong(value);

        //更新或添加key
        if (o) {
            dbOverwrite(c->db, c->argv[1], new);
        }
        else {
            dbAdd(c->db, c->argv[1], new);
        }
    }

    signalModifiedKey(c->db, c->argv[1]);
    notifyKeyspaceEvent(NOTIFY_STRING, "incrby", c->argv[1], c->db->id);
    server.dirty++;

    addReply(c, shared.colon);
    addReply(c, new);
    addReply(c, shared.crlf);
}

//命令实现
//INCR key
void incrCommand(client *c) {
    incrDecrCommand(c, 1);
}

//命令实现
//DECR key
void decrCommand(client *c) {
    incrDecrCommand(c, -1);
}

//命令实现
//INCRBY key increment
void incrbyCommand(client *c) {
    long long incr;

    if (getLongLongFromObjectOrReply(c, c->argv[2], &incr, NULL) != C_OK) { return; }
    incrDecrCommand(c, incr);
}

//命令实现
//DECRBY key decrement
void decrbyCommand(client *c) {
    long long incr;

    if (getLongLongFromObjectOrReply(c, c->argv[2], &incr, NULL) != C_OK) { return; }
    incrDecrCommand(c, -incr);
}

//todo
void incrbyfloatCommand(client *c) {
    long double incr, value;
    robj *o, *new, *aux;

    o = lookupKeyWrite(c->db, c->argv[1]);
    if (o != NULL && checkType(c, o, OBJ_STRING)) { return; }
    if (getLongDoubleFromObjectOrReply(c, o, &value, NULL) != C_OK ||
        getLongDoubleFromObjectOrReply(c, c->argv[2], &incr, NULL) != C_OK) {
        return;
    }

    value += incr;
    if (isnan(value) || isinf(value)) {
        addReplyError(c, "increment would produce NaN or Infinity");
        return;
    }
    new = createStringObjectFromLongDouble(value, 1);
    if (o) {
        dbOverwrite(c->db, c->argv[1], new);
    }
    else {
        dbAdd(c->db, c->argv[1], new);
    }
    signalModifiedKey(c->db, c->argv[1]);
    notifyKeyspaceEvent(NOTIFY_STRING, "incrbyfloat", c->argv[1], c->db->id);
    server.dirty++;
    addReplyBulk(c, new);

    /* Always replicate INCRBYFLOAT as a SET command with the final value
     * in order to make sure that differences in float precision or formatting
     * will not create differences in replicas or after an AOF restart. */
    aux = createStringObject("SET", 3);
    rewriteClientCommandArgument(c, 0, aux);
    decrRefCount(aux);
    rewriteClientCommandArgument(c, 2, new);
}

//命令实现
//APPEND key value
void appendCommand(client *c) {
    size_t totlen;
    robj *o, *append;

    o = lookupKeyWrite(c->db, c->argv[1]);

    if (o == NULL) {
        /* Create the key */
        //key不存在则创建之
        c->argv[2] = tryObjectEncoding(c->argv[2]);
        dbAdd(c->db, c->argv[1], c->argv[2]);
        incrRefCount(c->argv[2]);
        totlen = stringObjectLen(c->argv[2]);
    }
    else {
        /* Key exists, check type */
        //key存在，先进行类型检查
        if (checkType(c, o, OBJ_STRING)) {
            return;
        }

        /* "append" is an argument, so always an sds */
        //要append的value
        append = c->argv[2];
        //新的数据长度
        totlen = stringObjectLen(o) + sdslen(append->ptr);
        //检查要设置的字符串长度是否超过允许的范围
        if (checkStringLength(c, totlen) != C_OK) {
            return;
        }

        /* Append the value */
        //确保对象可以被修改
        o = dbUnshareStringValue(c->db, c->argv[1], o);
        //append数据
        o->ptr = sdscatlen(o->ptr, append->ptr, sdslen(append->ptr));
        totlen = sdslen(o->ptr);
    }

    signalModifiedKey(c->db, c->argv[1]);
    notifyKeyspaceEvent(NOTIFY_STRING, "append", c->argv[1], c->db->id);
    server.dirty++;

    //写入新的长度到回复缓冲
    addReplyLongLong(c, totlen);
}

//命令实现
//STRLEN key
void strlenCommand(client *c) {
    robj *o;

    //key不存在或类型错误
    if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.czero)) == NULL || checkType(c, o, OBJ_STRING)) {
        return;
    }

    addReplyLongLong(c, stringObjectLen(o));
}
