/* Redis Object implementation.
 *
 * Copyright (c) 2009-2012, Salvatore Sanfilippo <antirez at gmail dot com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Redis nor the names of its contributors may be used
 *     to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "server.h"
#include <math.h>
#include <ctype.h>

#ifdef __CYGWIN__
#define strtold(a,b) ((long double)strtod((a),(b)))
#endif

//创建类型为type的对象
robj *createObject(int type, void *ptr) {
    robj *o = zmalloc(sizeof(*o));
    o->type = type;
    o->encoding = OBJ_ENCODING_RAW;
    o->ptr = ptr;
    o->refcount = 1;

    /* Set the LRU to the current lruclock (minutes resolution). */
    //设定lru时间
    o->lru = LRU_CLOCK();
    return o;
}

/* Create a string object with encoding OBJ_ENCODING_RAW, that is a plain
 * string object where o->ptr points to a proper sds string. */
//创建raw编码的string对象
robj *createRawStringObject(const char *ptr, size_t len) {
    return createObject(OBJ_STRING, sdsnewlen(ptr, len));
}

/* Create a string object with encoding OBJ_ENCODING_EMBSTR, that is
 * an object where the sds string is actually an unmodifiable string
 * allocated in the same chunk as the object itself. */
//为ptr指向的数据创建emtstr编码的string对象
//embstr编码的对象，robj和ptr一次性分配内存，释放的时候也一起释放
robj *createEmbeddedStringObject(const char *ptr, size_t len) {
    //sdshdr8类型足以容纳下emgstr保存的数据
    robj *o = zmalloc(sizeof(robj) + sizeof(struct sdshdr8) + len + 1);

    //指向sds结构体
    struct sdshdr8 *sh = (void *) (o + 1);

    o->type = OBJ_STRING;
    o->encoding = OBJ_ENCODING_EMBSTR;

    //指向sds buf
    o->ptr = sh + 1;

    o->refcount = 1;
    o->lru = LRU_CLOCK();

    sh->len = len;
    sh->alloc = len;
    sh->flags = SDS_TYPE_8;

    //拷贝数据到sds buf
    if (ptr) {
        memcpy(sh->buf, ptr, len);
        sh->buf[len] = '\0';
    }
    else {
        //或者清零buf
        memset(sh->buf, 0, len + 1);
    }

    return o;
}

/* Create a string object with EMBSTR encoding if it is smaller than
 * REIDS_ENCODING_EMBSTR_SIZE_LIMIT, otherwise the RAW encoding is
 * used.
 *
 * The current limit of 39 is chosen so that the biggest string object
 * we allocate as EMBSTR will still fit into the 64 byte arena of jemalloc. */
//当数据长度小于该值时，使用embstr编码
//todo：为什么是44
#define OBJ_ENCODING_EMBSTR_SIZE_LIMIT 44

//创建raw/embstr编码的string对象
robj *createStringObject(const char *ptr, size_t len) {
    if (len <= OBJ_ENCODING_EMBSTR_SIZE_LIMIT) {
        return createEmbeddedStringObject(ptr, len);
    }
    else {
        return createRawStringObject(ptr, len);
    }
}

//为long long创建string对象
robj *createStringObjectFromLongLong(long long value) {
    robj *o;
    if (value >= 0 && value < OBJ_SHARED_INTEGERS) {
        //使用共享整数
        incrRefCount(shared.integers[value]);
        o = shared.integers[value];
    }
    else {
        //创建编码类型为int的对象
        if (value >= LONG_MIN && value <= LONG_MAX) {
            o = createObject(OBJ_STRING, NULL);
            o->encoding = OBJ_ENCODING_INT;
            o->ptr = (void *) ((long) value);
        }
        else {
            //创建embstr/raw对象
            o = createObject(OBJ_STRING, sdsfromlonglong(value));
        }
    }
    return o;
}

/* Create a string object from a long double. If humanfriendly is non-zero
 * it does not use exponential format and trims trailing zeroes at the end,
 * however this results in loss of precision. Otherwise exp format is used
 * and the output of snprintf() is not modified.
 *
 * The 'humanfriendly' option is used for INCRBYFLOAT and HINCRBYFLOAT. */
//为long double创建string对象
robj *createStringObjectFromLongDouble(long double value, int humanfriendly) {
    char buf[256];
    int len;

    //无穷大/无穷小
    if (isinf(value)) {
        /* Libc in odd systems (Hi Solaris!) will format infinite in a
         * different way, so better to handle it in an explicit way. */
        if (value > 0) {
            memcpy(buf, "inf", 3);
            len = 3;
        }
        else {
            memcpy(buf, "-inf", 4);
            len = 4;
        }
    }
    else if (humanfriendly) {
        /* We use 17 digits precision since with 128 bit floats that precision
         * after rounding is able to represent most small decimal numbers in a
         * way that is "non surprising" for the user (that is, most small
         * decimal numbers will be represented in a way that when converted
         * back into a string are exactly the same as what the user typed.) */
        //double转换成字符串需要的长度
        len = snprintf(buf, sizeof(buf), "%.17Lf", value);
        /* Now remove trailing zeroes after the '.' */
        //去除末尾的0
        if (strchr(buf, '.') != NULL) {
            char *p = buf + len - 1;
            while (*p == '0') {
                p--;
                len--;
            }
            if (*p == '.') { len--; }
        }
    }
    else {
        //double转换成字符串需要的长度
        len = snprintf(buf, sizeof(buf), "%.17Lg", value);
    }
    return createStringObject(buf, len);
}

/* Duplicate a string object, with the guarantee that the returned object
 * has the same encoding as the original one.
 *
 * This function also guarantees that duplicating a small integere object
 * (or a string object that contains a representation of a small integer)
 * will always result in a fresh object that is unshared (refcount == 1).
 *
 * The resulting object always has refcount set to 1. */
//复制string对象
robj *dupStringObject(robj *o) {
    robj *d;

    serverAssert(o->type == OBJ_STRING);

    switch (o->encoding) {
        case OBJ_ENCODING_RAW:
            return createRawStringObject(o->ptr, sdslen(o->ptr));
        case OBJ_ENCODING_EMBSTR:
            return createEmbeddedStringObject(o->ptr, sdslen(o->ptr));
        case OBJ_ENCODING_INT:
            d = createObject(OBJ_STRING, NULL);
            d->encoding = OBJ_ENCODING_INT;
            d->ptr = o->ptr;
            return d;
        default:
            serverPanic("Wrong encoding.");
            break;
    }
}

//创建quicklist对象
robj *createQuicklistObject(void) {
    quicklist *l = quicklistCreate();
    robj *o = createObject(OBJ_LIST, l);
    o->encoding = OBJ_ENCODING_QUICKLIST;
    return o;
}

//创建ziplist对象
robj *createZiplistObject(void) {
    unsigned char *zl = ziplistNew();
    robj *o = createObject(OBJ_LIST, zl);
    o->encoding = OBJ_ENCODING_ZIPLIST;
    return o;
}

//创建字典实现的集合对象
robj *createSetObject(void) {
    dict *d = dictCreate(&setDictType, NULL);
    robj *o = createObject(OBJ_SET, d);
    o->encoding = OBJ_ENCODING_HT;
    return o;
}

//创建intset实现的集合对象
robj *createIntsetObject(void) {
    intset *is = intsetNew();
    robj *o = createObject(OBJ_SET, is);
    o->encoding = OBJ_ENCODING_INTSET;
    return o;
}

//创建ziplist实现的hash对象
robj *createHashObject(void) {
    unsigned char *zl = ziplistNew();
    robj *o = createObject(OBJ_HASH, zl);
    o->encoding = OBJ_ENCODING_ZIPLIST;
    return o;
}

//创建zset对象
robj *createZsetObject(void) {
    zset *zs = zmalloc(sizeof(*zs));
    robj *o;

    zs->dict = dictCreate(&zsetDictType, NULL);
    zs->zsl = zslCreate();
    o = createObject(OBJ_ZSET, zs);
    o->encoding = OBJ_ENCODING_SKIPLIST;
    return o;
}

robj *createZsetZiplistObject(void) {
    unsigned char *zl = ziplistNew();
    robj *o = createObject(OBJ_ZSET, zl);
    o->encoding = OBJ_ENCODING_ZIPLIST;
    return o;
}

//释放string对象
void freeStringObject(robj *o) {
    if (o->encoding == OBJ_ENCODING_RAW) {
        //释放内部的sds
        sdsfree(o->ptr);
    }
}

//释放list对象
void freeListObject(robj *o) {
    if (o->encoding == OBJ_ENCODING_QUICKLIST) {
        //释放内部的quicklist
        quicklistRelease(o->ptr);
    }
    else {
        serverPanic("Unknown list encoding type");
    }
}

//释放set对象
void freeSetObject(robj *o) {
    switch (o->encoding) {
        case OBJ_ENCODING_HT:
            //字典实现的set对象，释放内部的字典
            dictRelease((dict *) o->ptr);
            break;
        case OBJ_ENCODING_INTSET:
            //intset实现的set对象，释放内部的intset
            //intset的数组和结构体一起分配内存，所以不用单独释放数组
            zfree(o->ptr);
            break;
        default:
            serverPanic("Unknown set encoding type");
    }
}

//释放zset对象
void freeZsetObject(robj *o) {
    zset *zs;
    switch (o->encoding) {
        case OBJ_ENCODING_SKIPLIST:
            zs = o->ptr;
            dictRelease(zs->dict);
            zslFree(zs->zsl);
            zfree(zs);
            break;
        case OBJ_ENCODING_ZIPLIST:
            zfree(o->ptr);
            break;
        default:
            serverPanic("Unknown sorted set encoding");
    }
}

//释放hash对象
void freeHashObject(robj *o) {
    switch (o->encoding) {
        case OBJ_ENCODING_HT:
            //释放内部字典
            dictRelease((dict *) o->ptr);
            break;
        case OBJ_ENCODING_ZIPLIST:
            //释放内部ziplist
            zfree(o->ptr);
            break;
        default:
            serverPanic("Unknown hash encoding type");
            break;
    }
}

//增加对象的引用计数
void incrRefCount(robj *o) {
    o->refcount++;
}

//减少对象的引用计数
//当引用计数减少为0时，释放对象
void decrRefCount(robj *o) {
    if (o->refcount <= 0) serverPanic("decrRefCount against refcount <= 0");

    //引用计数减少为0，先释放底层的实现
    if (o->refcount == 1) {
        switch (o->type) {
            case OBJ_STRING:
                freeStringObject(o);
                break;
            case OBJ_LIST:
                freeListObject(o);
                break;
            case OBJ_SET:
                freeSetObject(o);
                break;
            case OBJ_ZSET:
                freeZsetObject(o);
                break;
            case OBJ_HASH:
                freeHashObject(o);
                break;
            default:
                serverPanic("Unknown object type");
                break;
        }
        //再释放对象本身
        zfree(o);
    }
    else {
        o->refcount--;
    }
}

/* This variant of decrRefCount() gets its argument as void, and is useful
 * as free method in data structures that expect a 'void free_object(void*)'
 * prototype for the free method. */
//减少对象的引用计数
void decrRefCountVoid(void *o) {
    decrRefCount(o);
}

/* This function set the ref count to zero without freeing the object.
 * It is useful in order to pass a new object to functions incrementing
 * the ref count of the received object. Example:
 *
 *    functionThatWillIncrementRefCount(resetRefCount(CreateObject(...)));
 *
 * Otherwise you need to resort to the less elegant pattern:
 *
 *    *obj = createObject(...);
 *    functionThatWillIncrementRefCount(obj);
 *    decrRefCount(obj);
 */
//设置对象的引用计数为0，但不释放对象
robj *resetRefCount(robj *obj) {
    obj->refcount = 0;
    return obj;
}

//检查对象o是否类型type
//不是的话添加错误信息到回复缓冲
int checkType(client *c, robj *o, int type) {
    if (o->type != type) {
        addReply(c, shared.wrongtypeerr);
        return 1;
    }
    return 0;
}

//尝试将string对象的数据转换为整数并写入llval
int isObjectRepresentableAsLongLong(robj *o, long long *llval) {
    serverAssertWithInfo(NULL, o, o->type == OBJ_STRING);
    if (o->encoding == OBJ_ENCODING_INT) {
        //int编码，强制类型转换即可
        if (llval) {
            *llval = (long) o->ptr;
        }
        return C_OK;
    }
    else {
        //否则尝试转换
        return string2ll(o->ptr, sdslen(o->ptr), llval) ? C_OK : C_ERR;
    }
}

/* Try to encode a string object in order to save space */
//尝试将string对象编码类型由raw转换为embstr或int，节省内存
robj *tryObjectEncoding(robj *o) {
    long value;

    //底层保存的sds
    sds s = o->ptr;

    size_t len;

    /* Make sure this is a string object, the only type we encode
     * in this function. Other types use encoded memory efficient
     * representations but are handled by the commands implementing
     * the type. */
    serverAssertWithInfo(NULL, o, o->type == OBJ_STRING);

    /* We try some specialized encoding only for objects that are
     * RAW or EMBSTR encoded, in other words objects that are still
     * in represented by an actually array of chars. */
    //已经是int编码类型？
    if (!sdsEncodedObject(o)) {
        return o;
    }

    /* It's not safe to encode shared objects: shared objects can be shared
     * everywhere in the "object space" of Redis and may end in places where
     * they are not handled. We handle them only as values in the keyspace. */
    //避免修改共享对象
    if (o->refcount > 1) {
        return o;
    }

    /* Check if we can represent this string as a long integer.
     * Note that we are sure that a string larger than 20 chars is not
     * representable as a 32 nor 64 bit integer. */
    //是否可以保存为整数？
    len = sdslen(s);
    if (len <= 20 && string2l(s, len, &value)) {
        /* This object is encodable as a long. Try to use a shared object.
         * Note that we avoid using shared integers when maxmemory is used
         * because every object needs to have a private LRU field for the LRU
         * algorithm to work well. */
        //如果没有启用LRU淘汰算法
        //可以使用预先创建的共享整数对象
        //否则因为每个对象都需要维护自己的lru时间，无法使用共享对象
        if ((server.maxmemory == 0 ||
             (server.maxmemory_policy != MAXMEMORY_VOLATILE_LRU &&
              server.maxmemory_policy != MAXMEMORY_ALLKEYS_LRU)) &&
            value >= 0 &&
            value < OBJ_SHARED_INTEGERS) {
            decrRefCount(o);
            incrRefCount(shared.integers[value]);
            return shared.integers[value];
        }
        else {
            //保存为整数形式，先释放原来的sds
            if (o->encoding == OBJ_ENCODING_RAW) {
                sdsfree(o->ptr);
            }
            //再修改编码类型
            o->encoding = OBJ_ENCODING_INT;
            //最后修改指针指向整数
            o->ptr = (void *) value;
            return o;
        }
    }

    /* If the string is small and is still RAW encoded,
     * try the EMBSTR encoding which is more efficient.
     * In this representation the object and the SDS string are allocated
     * in the same chunk of memory to save space and cache misses. */
    //尝试转换为embstr编码
    if (len <= OBJ_ENCODING_EMBSTR_SIZE_LIMIT) {
        robj *emb;

        //已经是embstr编码？
        if (o->encoding == OBJ_ENCODING_EMBSTR) {
            return o;
        }

        //创建embstr对象
        emb = createEmbeddedStringObject(s, sdslen(s));

        //减少原先对象的引用计数
        decrRefCount(o);

        //返回新对象
        return emb;
    }

    /* We can't encode the object...
     *
     * Do the last try, and at least optimize the SDS string inside
     * the string object to require little space, in case there
     * is more than 10% of free space at the end of the SDS string.
     *
     * We do that only for relatively large strings as this branch
     * is only entered if the length of the string is greater than
     * OBJ_ENCODING_EMBSTR_SIZE_LIMIT. */
    //无法转换为embstr或int编码，尝试释放sds多余的内存
    if (o->encoding == OBJ_ENCODING_RAW && sdsavail(s) > len / 10) {
        //这里要将sdsRemoveFreeSpace的结果赋给ptr，因为该函数涉及malloc/realloc
        o->ptr = sdsRemoveFreeSpace(o->ptr);
    }
    /* Return the original object. */
    return o;
}

/* Get a decoded version of an encoded object (returned as a new object).
 * If the object is already raw-encoded just increment the ref count. */
//为int编码的string对象创建一个embstr/raw编码的新对象
robj *getDecodedObject(robj *o) {
    robj *dec;

    //已经是embstr/raw编码，则增加对象的引用计数
    if (sdsEncodedObject(o)) {
        incrRefCount(o);
        return o;
    }

    //否则创建新对象
    if (o->type == OBJ_STRING && o->encoding == OBJ_ENCODING_INT) {
        char buf[32];
        //将保存的整数转换为字符串
        ll2string(buf, 32, (long) o->ptr);
        dec = createStringObject(buf, strlen(buf));
        return dec;
    }
    else {
        serverPanic("Unknown encoding type");
    }
}

/* Compare two string objects via strcmp() or strcoll() depending on flags.
 * Note that the objects may be integer-encoded. In such a case we
 * use ll2string() to get a string representation of the numbers on the stack
 * and compare the strings, it's much faster than calling getDecodedObject().
 *
 * Important note: when REDIS_COMPARE_BINARY is used a binary-safe comparison
 * is used. */

#define REDIS_COMPARE_BINARY (1<<0)
#define REDIS_COMPARE_COLL (1<<1)

//比较两个string对象
int compareStringObjectsWithFlags(robj *a, robj *b, int flags) {
    serverAssertWithInfo(NULL, a, a->type == OBJ_STRING && b->type == OBJ_STRING);
    char bufa[128], bufb[128], *astr, *bstr;
    size_t alen, blen, minlen;

    //指向同一个对象
    if (a == b) {
        return 0;
    }

    //获取a对象保存的数据
    //若是int转换为字符串
    if (sdsEncodedObject(a)) {
        astr = a->ptr;
        alen = sdslen(astr);
    }
    else {
        alen = ll2string(bufa, sizeof(bufa), (long) a->ptr);
        astr = bufa;
    }

    //获取b对象保存的数据
    //若是int转换为字符串
    if (sdsEncodedObject(b)) {
        bstr = b->ptr;
        blen = sdslen(bstr);
    }
    else {
        blen = ll2string(bufb, sizeof(bufb), (long) b->ptr);
        bstr = bufb;
    }

    if (flags & REDIS_COMPARE_COLL) {
        return strcoll(astr, bstr);
    }
    else {
        int cmp;
        minlen = (alen < blen) ? alen : blen;

        //前缀相等则比较长度
        cmp = memcmp(astr, bstr, minlen);
        if (cmp == 0) {
            return alen - blen;
        }

        return cmp;
    }
}

/* Wrapper for compareStringObjectsWithFlags() using binary comparison. */
//比较两个string对象
int compareStringObjects(robj *a, robj *b) {
    return compareStringObjectsWithFlags(a, b, REDIS_COMPARE_BINARY);
}

/* Wrapper for compareStringObjectsWithFlags() using collation. */
//比较两个string对象
int collateStringObjects(robj *a, robj *b) {
    return compareStringObjectsWithFlags(a, b, REDIS_COMPARE_COLL);
}

/* Equal string objects return 1 if the two objects are the same from the
 * point of view of a string comparison, otherwise 0 is returned. Note that
 * this function is faster then checking for (compareStringObject(a,b) == 0)
 * because it can perform some more optimization. */
//两个string对象是否相等？
int equalStringObjects(robj *a, robj *b) {
    //int类型直接比较整数
    if (a->encoding == OBJ_ENCODING_INT &&
        b->encoding == OBJ_ENCODING_INT) {
        /* If both strings are integer encoded just check if the stored
         * long is the same. */
        return a->ptr == b->ptr;
    }
    else {
        return compareStringObjects(a, b) == 0;
    }
}

//获取string对象保存的数据的长度
size_t stringObjectLen(robj *o) {
    serverAssertWithInfo(NULL, o, o->type == OBJ_STRING);
    if (sdsEncodedObject(o)) {
        //embstr/raw直接获取长度
        return sdslen(o->ptr);
    }
    else {
        //int编码需要统计位数
        return sdigits10((long) o->ptr);
    }
}

//获取string对象保存的double数据，写入target
int getDoubleFromObject(robj *o, double *target) {
    double value;
    char *eptr;

    if (o == NULL) {
        value = 0;
    }
    else {
        serverAssertWithInfo(NULL, o, o->type == OBJ_STRING);
        if (sdsEncodedObject(o)) {
            errno = 0;

            //转换为double
            value = strtod(o->ptr, &eptr);

            //转换发生错误
            if (isspace(((char *) o->ptr)[0]) ||
                eptr[0] != '\0' ||
                (errno == ERANGE &&
                 (value == HUGE_VAL || value == -HUGE_VAL || value == 0)) ||
                errno == EINVAL ||
                isnan(value)) {
                return C_ERR;
            }
        }
        else if (o->encoding == OBJ_ENCODING_INT) {
            //int编码，强制类型转换即可
            value = (long) o->ptr;
        }
        else {
            serverPanic("Unknown string encoding");
        }
    }
    *target = value;
    return C_OK;
}

//获取string对象保存的double数据，写入target
//若获取失败，将msg写入回复缓冲
int getDoubleFromObjectOrReply(client *c, robj *o, double *target, const char *msg) {
    double value;

    //获取string对象保存的double数据
    if (getDoubleFromObject(o, &value) != C_OK) {
        if (msg != NULL) {
            addReplyError(c, (char *) msg);
        }
        else {
            addReplyError(c, "value is not a valid float");
        }
        return C_ERR;
    }

    *target = value;
    return C_OK;
}

//从robj对象中获取long double，写入target
int getLongDoubleFromObject(robj *o, long double *target) {
    long double value;
    char *eptr;

    if (o == NULL) {
        value = 0;
    }
    else {
        serverAssertWithInfo(NULL, o, o->type == OBJ_STRING);
        //raw/embstr
        if (sdsEncodedObject(o)) {
            errno = 0;
            //转换字符串为double
            value = strtold(o->ptr, &eptr);

            //转换失败
            if (isspace(((char *) o->ptr)[0]) || eptr[0] != '\0' ||
                errno == ERANGE || isnan(value)) {
                return C_ERR;
            }
        }
        else if (o->encoding == OBJ_ENCODING_INT) {
            //int编码直接转换类型
            value = (long) o->ptr;
        }
        else {
            serverPanic("Unknown string encoding");
        }
    }

    //写入target
    *target = value;
    return C_OK;
}

//从robj对象中获取long double，写入target
//若失败，添加msg到回复缓冲
int getLongDoubleFromObjectOrReply(client *c, robj *o, long double *target, const char *msg) {
    long double value;
    //获取string对象保存的long double数据
    if (getLongDoubleFromObject(o, &value) != C_OK) {
        //获取失败
        if (msg != NULL) {
            addReplyError(c, (char *) msg);
        }
        else {
            addReplyError(c, "value is not a valid float");
        }
        return C_ERR;
    }

    //写入target
    *target = value;
    return C_OK;
}

//将robj保存的数据转换为long long写入target
int getLongLongFromObject(robj *o, long long *target) {
    long long value;

    if (o == NULL) {
        value = 0;
    }
    else {
        serverAssertWithInfo(NULL, o, o->type == OBJ_STRING);
        //embstr/raw编码
        if (sdsEncodedObject(o)) {
            if (string2ll(o->ptr, sdslen(o->ptr), &value) == 0) {
                return C_ERR;
            }
        }
        else if (o->encoding == OBJ_ENCODING_INT) {
            //int编码直接转换类型并赋值
            value = (long) o->ptr;
        }
        else {
            serverPanic("Unknown string encoding");
        }
    }

    //写入target
    if (target) {
        *target = value;
    }

    return C_OK;
}

//将robj保存的数据转换为long long写入target中
//若失败，添加msg到回复缓冲
int getLongLongFromObjectOrReply(client *c, robj *o, long long *target, const char *msg) {
    long long value;

    //失败，添加错误信息到回复缓冲
    if (getLongLongFromObject(o, &value) != C_OK) {
        if (msg != NULL) {
            addReplyError(c, (char *) msg);
        }
        else {
            addReplyError(c, "value is not an integer or out of range");
        }
        return C_ERR;
    }

    //否则写入target
    *target = value;

    return C_OK;
}

//将robj保存的数据转换为long 写入target中
//若失败，添加msg到回复缓冲
int getLongFromObjectOrReply(client *c, robj *o, long *target, const char *msg) {
    long long value;

    //尝试获取long long
    if (getLongLongFromObjectOrReply(c, o, &value, msg) != C_OK) {
        return C_ERR;
    }

    //超过long的范围
    if (value < LONG_MIN || value > LONG_MAX) {
        if (msg != NULL) {
            addReplyError(c, (char *) msg);
        }
        else {
            addReplyError(c, "value is out of range");
        }
        return C_ERR;
    }

    *target = value;
    return C_OK;
}

//返回encoding对应的字符串表示
char *strEncoding(int encoding) {
    switch (encoding) {
        case OBJ_ENCODING_RAW:
            return "raw";
        case OBJ_ENCODING_INT:
            return "int";
        case OBJ_ENCODING_HT:
            return "hashtable";
        case OBJ_ENCODING_QUICKLIST:
            return "quicklist";
        case OBJ_ENCODING_ZIPLIST:
            return "ziplist";
        case OBJ_ENCODING_INTSET:
            return "intset";
        case OBJ_ENCODING_SKIPLIST:
            return "skiplist";
        case OBJ_ENCODING_EMBSTR:
            return "embstr";
        default:
            return "unknown";
    }
}

/* Given an object returns the min number of milliseconds the object was never
 * requested, using an approximated LRU algorithm. */
//获取对象的闲置时间
unsigned long long estimateObjectIdleTime(robj *o) {
    unsigned long long lruclock = LRU_CLOCK();
    if (lruclock >= o->lru) {
        return (lruclock - o->lru) * LRU_CLOCK_RESOLUTION;
    }
    else {
        //lru时间为24位无符号整数，到达最大值后会从头开始
        return (lruclock + (LRU_CLOCK_MAX - o->lru)) *
               LRU_CLOCK_RESOLUTION;
    }
}

/* This is a helper function for the OBJECT command. We need to lookup keys
 * without any modification of LRU or other parameters. */
//获取key对应的对象，但不更新lru时间
robj *objectCommandLookup(client *c, robj *key) {
    dictEntry *de;

    if ((de = dictFind(c->db->dict, key->ptr)) == NULL) {
        return NULL;
    }
    return (robj *) dictGetVal(de);
}

//获取key对应的对象，但不更新lru时间
//若获取失败，添加reply到回复缓冲
robj *objectCommandLookupOrReply(client *c, robj *key, robj *reply) {
    robj *o = objectCommandLookup(c, key);

    if (!o) {
        addReply(c, reply);
    }
    return o;
}

/* Object command allows to inspect the internals of an Redis Object.
 * Usage: OBJECT <refcount|encoding|idletime> <key> */
//命令实现
//OBJECT subcommand [arguments [arguments ...]]
void objectCommand(client *c) {
    robj *o;

    //获取对象的引用计数
    if (!strcasecmp(c->argv[1]->ptr, "refcount") && c->argc == 3) {
        if ((o = objectCommandLookupOrReply(c, c->argv[2], shared.nullbulk))
            == NULL) {
            return;
        }
        addReplyLongLong(c, o->refcount);
    }
        //获取对象的实现方式
    else if (!strcasecmp(c->argv[1]->ptr, "encoding") && c->argc == 3) {
        if ((o = objectCommandLookupOrReply(c, c->argv[2], shared.nullbulk)) == NULL) {
            return;
        }
        addReplyBulkCString(c, strEncoding(o->encoding));
    }
        //获取对象的闲置时间
    else if (!strcasecmp(c->argv[1]->ptr, "idletime") && c->argc == 3) {
        if ((o = objectCommandLookupOrReply(c, c->argv[2], shared.nullbulk)) == NULL) {
            return;
        }
        addReplyLongLong(c, estimateObjectIdleTime(o) / 1000);
    }
        //语法错误
    else {
        addReplyError(c, "Syntax error. Try OBJECT (refcount|encoding|idletime)");
    }
}

