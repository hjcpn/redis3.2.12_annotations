/*
 * Copyright (c) 2009-2012, Salvatore Sanfilippo <antirez at gmail dot com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Redis nor the names of its contributors may be used
 *     to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "server.h"
#include "bio.h"
#include "rio.h"

#include <signal.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <sys/param.h>

void aofUpdateCurrentSize(void);

void aofClosePipes(void);

/* ----------------------------------------------------------------------------
 * AOF rewrite buffer implementation.
 *
 * The following code implement a simple buffer used in order to accumulate
 * changes while the background process is rewriting the AOF file.
 *
 * We only need to append, but can't just use realloc with a large block
 * because 'huge' reallocs are not always handled as one could expect
 * (via remapping of pages at OS level) but may involve copying data.
 *
 * For this reason we use a list of blocks, every block is
 * AOF_RW_BUF_BLOCK_SIZE bytes.
 * ------------------------------------------------------------------------- */

#define AOF_RW_BUF_BLOCK_SIZE (1024*1024*10)    /* 10 MB per block */

//aof重写缓冲块
typedef struct aofrwblock {
    //已使用，空闲空间
    unsigned long used, free;
    char buf[AOF_RW_BUF_BLOCK_SIZE];
} aofrwblock;

/* This function free the old AOF rewrite buffer if needed, and initialize
 * a fresh new one. It tests for server.aof_rewrite_buf_blocks equal to NULL
 * so can be used for the first initialization as well. */
//重置重写缓冲(链表)
void aofRewriteBufferReset(void) {
    //释放原先的重写链表
    if (server.aof_rewrite_buf_blocks) {
        listRelease(server.aof_rewrite_buf_blocks);
    }

    //创建新的重写链表
    server.aof_rewrite_buf_blocks = listCreate();
    listSetFreeMethod(server.aof_rewrite_buf_blocks, zfree);
}

/* Return the current size of the AOF rewrite buffer. */
//当前重写链表的总数据量
unsigned long aofRewriteBufferSize(void) {
    listNode *ln;
    listIter li;
    unsigned long size = 0;

    //遍历链表统计数据量
    listRewind(server.aof_rewrite_buf_blocks, &li);
    while ((ln = listNext(&li))) {
        aofrwblock *block = listNodeValue(ln);
        size += block->used;
    }
    return size;
}

/* Event handler used to send data to the child process doing the AOF
 * rewrite. We send pieces of our AOF differences buffer so that the final
 * write when the child finishes the rewrite will be small. */
//将aof重写链表的数据发送给正在进行aof文件重写的子进程
//这样当子进程完成重写时，需要追加的新命令就不会很多
void aofChildWriteDiffData(aeEventLoop *el, int fd, void *privdata, int mask) {
    listNode *ln;
    aofrwblock *block;
    ssize_t nwritten;
    UNUSED(el);
    UNUSED(fd);
    UNUSED(privdata);
    UNUSED(mask);

    //遍历重写链表，发送数据
    while (1) {
        ln = listFirst(server.aof_rewrite_buf_blocks);
        block = ln ? ln->value : NULL;

        //节点为NULL
        //或当子进程通过管道发送"！"给父进程时，aof_stop_sending_diff被置为true，此时停止发送新命令
        if (server.aof_stop_sending_diff || !block) {
            //删除可写事件
            aeDeleteFileEvent(server.el, server.aof_pipe_write_data_to_child, AE_WRITABLE);
            return;
        }

        if (block->used > 0) {
            //向子进程管道写入新命令
            nwritten = write(server.aof_pipe_write_data_to_child, block->buf, block->used);
            if (nwritten <= 0) {
                return;
            }
            //将未发送的数据移到前面覆盖已发送的数据
            memmove(block->buf, block->buf + nwritten, block->used - nwritten);
            block->used -= nwritten;
            block->free += nwritten;
        }

        //该节点的数据已经全部发送给子进程，从链表删除
        if (block->used == 0) {
            listDelNode(server.aof_rewrite_buf_blocks, ln);
        }
    }
}

/* Append data to the AOF rewrite buffer, allocating new blocks if needed. */
//正在进行aof重写，需要将重写期间的命令，单独再写到aof重写缓冲区
void aofRewriteBufferAppend(unsigned char *s, unsigned long len) {
    listNode *ln = listLast(server.aof_rewrite_buf_blocks);
    aofrwblock *block = ln ? ln->value : NULL;

    while (len) {
        /* If we already got at least an allocated block, try appending
         * at least some piece into it. */
        //最后一块缓冲区是否还有空闲空间？
        if (block) {
            unsigned long thislen = (block->free < len) ? block->free : len;
            if (thislen) {  /* The current block is not already full. */
                memcpy(block->buf + block->used, s, thislen);
                block->used += thislen;
                block->free -= thislen;
                s += thislen;
                len -= thislen;
            }
        }

        //最后一块没有足够空间写入len的数据，需要分配新的缓存块
        if (len) { /* First block to allocate, or need another block. */
            int numblocks;

            block = zmalloc(sizeof(*block));
            block->free = AOF_RW_BUF_BLOCK_SIZE;
            block->used = 0;
            //添加新块到重写缓冲链表
            listAddNodeTail(server.aof_rewrite_buf_blocks, block);

            /* Log every time we cross more 10 or 100 blocks, respectively
             * as a notice or warning. */
            //使用超过10或100块 * n时，打印警告信息
            numblocks = listLength(server.aof_rewrite_buf_blocks);
            if (((numblocks + 1) % 10) == 0) {
                int level = ((numblocks + 1) % 100) == 0 ? LL_WARNING :
                            LL_NOTICE;
                serverLog(level, "Background AOF buffer size: %lu MB",
                          aofRewriteBufferSize() / (1024 * 1024));
            }
        }
    }

    /* Install a file event to send data to the rewrite child if there is
     * not one already. */
    //注册可写事件
    //主要是将重写链表的数据发送给正在进行aof文件重写的子进程
    //这样当子进程完成重写时，需要追加的新命令就不会很多
    if (aeGetFileEvents(server.el, server.aof_pipe_write_data_to_child) == 0) {
        aeCreateFileEvent(server.el, server.aof_pipe_write_data_to_child,
                          AE_WRITABLE, aofChildWriteDiffData, NULL);
    }
}

/* Write the buffer (possibly composed of multiple blocks) into the specified
 * fd. If a short write or any other error happens -1 is returned,
 * otherwise the number of bytes written is returned. */
//将重写链表的数据写入重写后的aof文件
ssize_t aofRewriteBufferWrite(int fd) {
    listNode *ln;
    listIter li;
    ssize_t count = 0;

    //迭代重写链表，将每个节点的数据写入aof文件
    listRewind(server.aof_rewrite_buf_blocks, &li);
    while ((ln = listNext(&li))) {
        aofrwblock *block = listNodeValue(ln);
        ssize_t nwritten;

        if (block->used) {
            nwritten = write(fd, block->buf, block->used);
            if (nwritten != (ssize_t) block->used) {
                if (nwritten == 0) errno = EIO;
                return -1;
            }
            count += nwritten;
        }
    }
    return count;
}

/* ----------------------------------------------------------------------------
 * AOF file implementation
 * ------------------------------------------------------------------------- */

/* Starts a background task that performs fsync() against the specified
 * file descriptor (the one of the AOF file) in another thread. */
//创建后台fsync任务，signal正在等待任务的线程让其执行
void aof_background_fsync(int fd) {
    bioCreateBackgroundJob(BIO_AOF_FSYNC, (void *) (long) fd, NULL, NULL);
}

/* Called when the user switches from "appendonly yes" to "appendonly no"
 * at runtime using the CONFIG command. */
//运行时关闭aof
void stopAppendOnly(void) {
    serverAssert(server.aof_state != AOF_OFF);
    //将aof缓存中的数据写到aof文件
    flushAppendOnlyFile(1);
    //刷到磁盘
    aof_fsync(server.aof_fd);
    //关闭aof文件
    close(server.aof_fd);

    //更新aof相关状态
    server.aof_fd = -1;
    server.aof_selected_db = -1;
    server.aof_state = AOF_OFF;

    /* rewrite operation in progress? kill it, wait child exit */
    //杀死aof重写子进程
    if (server.aof_child_pid != -1) {
        int statloc;
        serverLog(LL_NOTICE, "Killing running AOF rewrite child: %ld", (long) server.aof_child_pid);
        if (kill(server.aof_child_pid, SIGUSR1) != -1) {
            while (wait3(&statloc, 0, NULL) != server.aof_child_pid);
        }

        /* reset the buffer accumulating changes while the child saves */
        //重置重写缓冲(链表)
        aofRewriteBufferReset();
        aofRemoveTempFile(server.aof_child_pid);
        server.aof_child_pid = -1;
        server.aof_rewrite_time_start = -1;
        /* close pipes used for IPC between the two processes. */
        //关闭父子进程通信管道
        aofClosePipes();
    }
}

/* Called when the user switches from "appendonly no" to "appendonly yes"
 * at runtime using the CONFIG command. */
//在运行时修改配置，开启aof选项
int startAppendOnly(void) {
    char cwd[MAXPATHLEN]; /* Current working dir path for error messages. */

    server.aof_last_fsync = server.unixtime;

    //打开/创建aof文件
    server.aof_fd = open(server.aof_filename, O_WRONLY | O_APPEND | O_CREAT, 0644);
    serverAssert(server.aof_state == AOF_OFF);
    //打开/创建失败
    if (server.aof_fd == -1) {
        char *cwdp = getcwd(cwd, MAXPATHLEN);

        serverLog(LL_WARNING,
                  "Redis needs to enable the AOF but can't open the "
                  "append only file %s (in server root dir %s): %s",
                  server.aof_filename,
                  cwdp ? cwdp : "unknown",
                  strerror(errno));
        return C_ERR;
    }

    //有rdb子进程，推迟生成aof文件
    if (server.rdb_child_pid != -1) {
        server.aof_rewrite_scheduled = 1;
        serverLog(LL_WARNING,
                  "AOF was enabled but there is already a child process saving an RDB file on disk. An AOF background was scheduled to start when possible.");

        //根据当前数据库状态，创建子进程去生成aof文件
    }
    else if (rewriteAppendOnlyFileBackground() == C_ERR) {
        close(server.aof_fd);
        serverLog(LL_WARNING,
                  "Redis needs to enable the AOF but can't trigger a background AOF rewrite operation. Check the above logs for more info about the error.");
        return C_ERR;
    }

    /* We correctly switched on AOF, now wait for the rewrite to be complete
     * in order to append data on disk. */
    //等待子进程生成aof文件完成
    server.aof_state = AOF_WAIT_REWRITE;
    return C_OK;
}

/* Write the append only file buffer on disk.
 *
 * Since we are required to write the AOF before replying to the client,
 * and the only way the client socket can get a write is entering when the
 * the event loop, we accumulate all the AOF writes in a memory
 * buffer and write it on disk using this function just before entering
 * the event loop again.
 *
 * About the 'force' argument:
 *
 * When the fsync policy is set to 'everysec' we may delay the flush if there
 * is still an fsync() going on in the background thread, since for instance
 * on Linux write(2) will be blocked by the background fsync anyway.
 * When this happens we remember that there is some aof buffer to be
 * flushed ASAP, and will try to do that in the serverCron() function.
 *
 * However if force is set to 1 we'll write regardless of the background
 * fsync. */
#define AOF_WRITE_LOG_ERROR_RATE 30 /* Seconds between errors logging. */

//将aof缓存中的数据写到aof文件，并根据配置决定是否fsyhc到磁盘
//每次进入事件循环之前会调用该函数
//当服务器准备关闭时也会调用该函数
void flushAppendOnlyFile(int force) {
    ssize_t nwritten;
    int sync_in_progress = 0;
    mstime_t latency;

    //aof缓冲区没有数据
    if (sdslen(server.aof_buf) == 0) {
        return;
    }

    if (server.aof_fsync == AOF_FSYNC_EVERYSEC) {
        //是否有fsync正在执行？
        sync_in_progress = bioPendingJobsOfType(BIO_AOF_FSYNC) != 0;
    }

    if (server.aof_fsync == AOF_FSYNC_EVERYSEC && !force) {
        /* With this append fsync policy we do background fsyncing.
         * If the fsync is still in progress we can try to delay
         * the write for a couple of seconds. */
        //已经有fsync正在执行
        if (sync_in_progress) {
            //这是第一次推迟
            if (server.aof_flush_postponed_start == 0) {
                /* No previous write postponing, remember that we are
                 * postponing the flush and return. */
                //记录本次推迟
                server.aof_flush_postponed_start = server.unixtime;
                return;

                //不是第一次推迟，但是距离上一次小于2秒，那么再次推迟
            }
            else if (server.unixtime - server.aof_flush_postponed_start < 2) {
                /* We were already waiting for fsync to finish, but for less
                 * than two seconds this is still ok. Postpone again. */
                return;
            }
            /* Otherwise fall trough, and go write since we can't wait
             * over two seconds. */
            //否则不再推迟执行
            server.aof_delayed_fsync++;
            serverLog(LL_NOTICE,
                      "Asynchronous AOF fsync is taking too long (disk is busy?). Writing the AOF buffer without waiting for fsync to complete, this may slow down Redis.");
        }
    }
    /* We want to perform a single write. This should be guaranteed atomic
     * at least if the filesystem we are writing is a real physical one.
     * While this will save us against the server being killed I don't think
     * there is much to do about the whole server stopping for power problems
     * or alike */

    latencyStartMonitor(latency);
    //写aof缓冲区数据到aof文件
    nwritten = write(server.aof_fd, server.aof_buf, sdslen(server.aof_buf));
    latencyEndMonitor(latency);

    /* We want to capture different events for delayed writes:
     * when the delay happens with a pending fsync, or with a saving child
     * active, and when the above two conditions are missing.
     * We also use an additional event name to save all samples which is
     * useful for graphing / monitoring purposes. */
    if (sync_in_progress) {
        latencyAddSampleIfNeeded("aof-write-pending-fsync", latency);
    }
    else if (server.aof_child_pid != -1 || server.rdb_child_pid != -1) {
        latencyAddSampleIfNeeded("aof-write-active-child", latency);
    }
    else {
        latencyAddSampleIfNeeded("aof-write-alone", latency);
    }
    latencyAddSampleIfNeeded("aof-write", latency);

    /* We performed the write so reset the postponed flush sentinel to zero. */
    //已经开始执行flush，重置推迟标记
    server.aof_flush_postponed_start = 0;

    //写入失败或部分失败，没有将aof缓冲区的全部数据写入aof文件
    if (nwritten != (signed) sdslen(server.aof_buf)) {
        static time_t last_write_error_log = 0;
        int can_log = 0;

        /* Limit logging rate to 1 line per AOF_WRITE_LOG_ERROR_RATE seconds. */
        //限制记录日志的频率
        if ((server.unixtime - last_write_error_log) > AOF_WRITE_LOG_ERROR_RATE) {
            can_log = 1;
            last_write_error_log = server.unixtime;
        }

        /* Log the AOF write error and record the error code. */
        //写入发生错误
        if (nwritten == -1) {
            if (can_log) {
                serverLog(LL_WARNING, "Error writing to the AOF file: %s",
                          strerror(errno));
                server.aof_last_write_errno = errno;
            }
        }
        else {
            //部分写入
            if (can_log) {
                serverLog(LL_WARNING, "Short write while writing to "
                                      "the AOF file: (nwritten=%lld, "
                                      "expected=%lld)",
                          (long long) nwritten,
                          (long long) sdslen(server.aof_buf));
            }

            //尝试移除部分写入的数据
            if (ftruncate(server.aof_fd, server.aof_current_size) == -1) {
                if (can_log) {
                    serverLog(LL_WARNING, "Could not remove short write "
                                          "from the append-only file.  Redis may refuse "
                                          "to load the AOF the next time it starts.  "
                                          "ftruncate: %s", strerror(errno));
                }
            }
            else {
                /* If the ftruncate() succeeded we can set nwritten to
                 * -1 since there is no longer partial data into the AOF. */
                //移除部分写入的数据成功
                nwritten = -1;
            }
            server.aof_last_write_errno = ENOSPC;
        }

        /* Handle the AOF write error. */
        if (server.aof_fsync == AOF_FSYNC_ALWAYS) {
            /* We can't recover when the fsync policy is ALWAYS since the
             * reply for the client is already in the output buffers, and we
             * have the contract with the user that on acknowledged write data
             * is synced on disk. */
            //fsync设定为AOF_FSYNC_ALWAYS，则回复用户之前必须确保aof数据已经被写入磁盘
            //则此时的write错误无法再补救，终止redis
            serverLog(LL_WARNING,
                      "Can't recover from AOF write error when the AOF fsync policy is 'always'. Exiting...");
            exit(1);
        }
        else {
            /* Recover from failed write leaving data into the buffer. However
             * set an error to stop accepting writes as long as the error
             * condition is not cleared. */
            //记录最后一次flush操作失败
            //这种状态下服务器拒绝执行写命令
            //serverCron会尝试再次flush
            //或者下次调用beforeSleep时再次尝试flush
            server.aof_last_write_status = C_ERR;

            /* Trim the sds buffer if there was a partial write, and there
             * was no way to undo it with ftruncate(2). */
            //从aof文件移除部分写入的数据失败，则将这部分数据从aof缓冲区移除
            if (nwritten > 0) {
                //更新当前aof文件大小
                server.aof_current_size += nwritten;
                sdsrange(server.aof_buf, nwritten, -1);
            }
            //下次再试
            return; /* We'll try again on the next call... */
        }
    }
    else {
        /* Successful write(2). If AOF was in error state, restore the
         * OK state and log the event. */
        //成功！aof缓冲区数据全部写入aof文件(严格讲只是写入内核缓冲区)
        //aof_last_write_status == C_ERR，则表示此时成功从上一次的flush失败恢复了
        if (server.aof_last_write_status == C_ERR) {
            serverLog(LL_WARNING,
                      "AOF write error looks solved, Redis can write again.");
            server.aof_last_write_status = C_OK;
        }
    }
    //更新aof文件大小
    server.aof_current_size += nwritten;

    /* Re-use AOF buffer when it is small enough. The maximum comes from the
     * arena size of 4k minus some overhead (but is otherwise arbitrary). */
    //aof缓冲区小于4k，则保留缓冲区等待下次使用
    if ((sdslen(server.aof_buf) + sdsavail(server.aof_buf)) < 4000) {
        sdsclear(server.aof_buf);
    }
    else {
        //否则释放缓冲区
        sdsfree(server.aof_buf);
        server.aof_buf = sdsempty();
    }

    /* Don't fsync if no-appendfsync-on-rewrite is set to yes and there are
     * children doing I/O in the background. */
    //是否在有aof/rdb子进程正在执行的情况下先不fsync？
    if (server.aof_no_fsync_on_rewrite &&
        (server.aof_child_pid != -1 || server.rdb_child_pid != -1)) {
        return;
    }

    /* Perform the fsync if needed. */
    //每次进入事件循环前都fsync？
    //保证了回复用户之前，用户本次命令的aof记录已经刷到磁盘
    if (server.aof_fsync == AOF_FSYNC_ALWAYS) {
        /* aof_fsync is defined as fdatasync() for Linux in order to avoid
         * flushing metadata. */
        latencyStartMonitor(latency);
        aof_fsync(server.aof_fd); /* Let's try to get this data on the disk */
        latencyEndMonitor(latency);
        latencyAddSampleIfNeeded("aof-fsync-always", latency);
        server.aof_last_fsync = server.unixtime;
    }
    else if ((server.aof_fsync == AOF_FSYNC_EVERYSEC && server.unixtime > server.aof_last_fsync)) {
        //或者每秒执行一次fsync？
        //若是这种情况，则放到后台线程去fsync
        if (!sync_in_progress) {
            //当前没有正在执行的fsync
            aof_background_fsync(server.aof_fd);
        }
        server.aof_last_fsync = server.unixtime;
    }
}

//添加命令到aof缓冲区
sds catAppendOnlyGenericCommand(sds dst, int argc, robj **argv) {
    char buf[32];
    int len, j;
    robj *o;

    //写入命令数组参数个数
    buf[0] = '*';
    len = 1 + ll2string(buf + 1, sizeof(buf) - 1, argc);
    buf[len++] = '\r';
    buf[len++] = '\n';
    dst = sdscatlen(dst, buf, len);

    //遍历argv写入bulk参数
    for (j = 0; j < argc; j++) {
        //为int编码的string对象创建一个embstr/raw编码的新对象
        o = getDecodedObject(argv[j]);

        //写入当前参数长度
        buf[0] = '$';
        len = 1 + ll2string(buf + 1, sizeof(buf) - 1, sdslen(o->ptr));
        buf[len++] = '\r';
        buf[len++] = '\n';
        dst = sdscatlen(dst, buf, len);

        //写入参数内容
        dst = sdscatlen(dst, o->ptr, sdslen(o->ptr));
        dst = sdscatlen(dst, "\r\n", 2);

        decrRefCount(o);
    }

    return dst;
}

/* Create the sds representation of an PEXPIREAT command, using
 * 'seconds' as time to live and 'cmd' to understand what command
 * we are translating into a PEXPIREAT.
 *
 * This command is used in order to translate EXPIRE and PEXPIRE commands
 * into PEXPIREAT command so that we retain precision in the append only
 * file, and the time is always absolute and not relative. */
//将不同的过期命令转换成统一的命令格式
sds catAppendOnlyExpireAtCommand(sds buf, struct redisCommand *cmd, robj *key, robj *seconds) {
    long long when;
    robj *argv[3];

    /* Make sure we can use strtoll */
    //获取过期时间
    seconds = getDecodedObject(seconds);
    when = strtoll(seconds->ptr, NULL, 10);

    /* Convert argument into milliseconds for EXPIRE, SETEX, EXPIREAT */
    //处理过期时间单位
    if (cmd->proc == expireCommand || cmd->proc == setexCommand ||
        cmd->proc == expireatCommand) {
        when *= 1000;
    }

    /* Convert into absolute time for EXPIRE, PEXPIRE, SETEX, PSETEX */
    //转换相对时间成为绝对时间
    if (cmd->proc == expireCommand || cmd->proc == pexpireCommand ||
        cmd->proc == setexCommand || cmd->proc == psetexCommand) {
        when += mstime();
    }

    decrRefCount(seconds);

    //添加命令到aof缓冲区
    argv[0] = createStringObject("PEXPIREAT", 9);
    argv[1] = key;
    argv[2] = createStringObjectFromLongLong(when);
    buf = catAppendOnlyGenericCommand(buf, 3, argv);

    decrRefCount(argv[0]);
    decrRefCount(argv[2]);

    return buf;
}

//添加命令到aof缓冲区，call之后调用
void feedAppendOnlyFile(struct redisCommand *cmd, int dictid, robj **argv, int argc) {
    sds buf = sdsempty();
    robj *tmpargv[3];

    /* The DB this command was targeting is not the same as the last command
     * we appended. To issue a SELECT command is needed. */
    //选择数据库
    if (dictid != server.aof_selected_db) {
        char seldb[64];
        snprintf(seldb, sizeof(seldb), "%d", dictid);
        buf = sdscatprintf(buf, "*2\r\n$6\r\nSELECT\r\n$%lu\r\n%s\r\n",
                           (unsigned long) strlen(seldb), seldb);
        server.aof_selected_db = dictid;
    }

    if (cmd->proc == expireCommand || cmd->proc == pexpireCommand || cmd->proc == expireatCommand) {
        /* Translate EXPIRE/PEXPIRE/EXPIREAT into PEXPIREAT */
        //将不同的过期命令转换成统一的命令格式
        buf = catAppendOnlyExpireAtCommand(buf, cmd, argv[1], argv[2]);
    }
    else if (cmd->proc == setexCommand || cmd->proc == psetexCommand) {
        /* Translate SETEX/PSETEX to SET and PEXPIREAT */
        //SETEX key seconds value
        //将setex/psetex转换成set和pexpireat命令
        tmpargv[0] = createStringObject("SET", 3);
        //key
        tmpargv[1] = argv[1];
        //value
        tmpargv[2] = argv[3];
        //set命令
        buf = catAppendOnlyGenericCommand(buf, 3, tmpargv);
        decrRefCount(tmpargv[0]);
        //设置过期时间
        buf = catAppendOnlyExpireAtCommand(buf, cmd, argv[1], argv[2]);
    }
    else if (cmd->proc == setCommand && argc > 3) {
        //将set [...]转换成set和pexpireat命令
        int i;
        robj *exarg = NULL, *pxarg = NULL;
        /* Translate SET [EX seconds][PX milliseconds] to SET and PEXPIREAT */
        buf = catAppendOnlyGenericCommand(buf, 3, argv);
        for (i = 3; i < argc; i++) {
            if (!strcasecmp(argv[i]->ptr, "ex")) { exarg = argv[i + 1]; }
            if (!strcasecmp(argv[i]->ptr, "px")) { pxarg = argv[i + 1]; }
        }
        serverAssert(!(exarg && pxarg));
        if (exarg) {
            //有ex参数
            buf = catAppendOnlyExpireAtCommand(buf, server.expireCommand, argv[1],
                                               exarg);
        }
        if (pxarg) {
            //有px参数
            buf = catAppendOnlyExpireAtCommand(buf, server.pexpireCommand, argv[1],
                                               pxarg);
        }
    }
    else {
        /* All the other commands don't need translation or need the
         * same translation already operated in the command vector
         * for the replication itself. */
        //其他命令不需要转换，直接添加到缓冲区
        buf = catAppendOnlyGenericCommand(buf, argc, argv);
    }

    /* Append to the AOF buffer. This will be flushed on disk just before
     * of re-entering the event loop, so before the client will get a
     * positive reply about the operation performed. */
    //将buf的内容添加到aof缓冲区
    if (server.aof_state == AOF_ON) {
        server.aof_buf = sdscatlen(server.aof_buf, buf, sdslen(buf));
    }

    /* If a background append only file rewriting is in progress we want to
     * accumulate the differences between the child DB and the current one
     * in a buffer, so that when the child process will do its work we
     * can append the differences to the new append only file. */
    //正在进行aof重写，需要将重写期间的命令，单独再写到aof重写缓冲区
    if (server.aof_child_pid != -1) {
        aofRewriteBufferAppend((unsigned char *) buf, sdslen(buf));
    }

    //释放临时缓存
    sdsfree(buf);
}

/* ----------------------------------------------------------------------------
 * AOF loading
 * ------------------------------------------------------------------------- */

/* In Redis commands are always executed in the context of a client, so in
 * order to load the append only file we need to create a fake client. */
//创建伪客户端用于执行从aof文件读取的命令
struct client *createFakeClient(void) {
    struct client *c = zmalloc(sizeof(*c));

    selectDb(c, 0);
    c->fd = -1;
    c->name = NULL;
    c->querybuf = sdsempty();
    c->querybuf_peak = 0;
    c->argc = 0;
    c->argv = NULL;
    c->bufpos = 0;
    c->flags = 0;
    c->btype = BLOCKED_NONE;
    /* We set the fake client as a slave waiting for the synchronization
     * so that Redis will not try to send replies to this client. */
    c->replstate = SLAVE_STATE_WAIT_BGSAVE_START;
    c->reply = listCreate();
    c->reply_bytes = 0;
    c->obuf_soft_limit_reached_time = 0;
    c->watched_keys = listCreate();
    c->peerid = NULL;
    listSetFreeMethod(c->reply, decrRefCountVoid);
    listSetDupMethod(c->reply, dupClientReplyValue);
    initClientMultiState(c);
    return c;
}

//释放伪客户端参数对象数组
void freeFakeClientArgv(struct client *c) {
    int j;

    for (j = 0; j < c->argc; j++)
        decrRefCount(c->argv[j]);
    zfree(c->argv);
}

//释放伪客户端
void freeFakeClient(struct client *c) {
    //释放读缓冲区
    sdsfree(c->querybuf);
    //释放回复链表
    listRelease(c->reply);
    listRelease(c->watched_keys);
    freeClientMultiState(c);
    zfree(c);
}

/* Replay the append log file. On success C_OK is returned. On non fatal
 * error (the append only file is zero-length) C_ERR is returned. On
 * fatal error an error message is logged and the program exists. */
//读取aof文件
int loadAppendOnlyFile(char *filename) {
    struct client *fakeClient;

    FILE *fp = fopen(filename, "r");

    struct redis_stat sb;
    int old_aof_state = server.aof_state;
    long loops = 0;
    off_t valid_up_to = 0; /* Offset of the latest well-formed command loaded. */

    //文件size为0
    if (fp && redis_fstat(fileno(fp), &sb) != -1 && sb.st_size == 0) {
        server.aof_current_size = 0;
        fclose(fp);
        return C_ERR;
    }

    //无法打开文件
    if (fp == NULL) {
        serverLog(LL_WARNING, "Fatal error: can't open the append log file for reading: %s", strerror(errno));
        exit(1);
    }

    /* Temporarily disable AOF, to prevent EXEC from feeding a MULTI
     * to the same file we're about to read. */
    server.aof_state = AOF_OFF;

    //创建伪客户端
    fakeClient = createFakeClient();

    //开始读取aof之前更新相关状态
    startLoading(fp);

    //读取aof文件命令并执行，直到文件结束
    while (1) {
        int argc, j;
        unsigned long len;
        robj **argv;
        char buf[128];
        sds argsds;
        struct redisCommand *cmd;

        /* Serve the clients from time to time */
        //每读取并执行完1000条命令，处理一下文件事件
        if (!(loops++ % 1000)) {
            loadingProgress(ftello(fp));
            processEventsWhileBlocked();
        }

        //读取文件内容到缓冲区
        if (fgets(buf, sizeof(buf), fp) == NULL) {
            //文件结束
            if (feof(fp)) {
                break;
            }
            else {
                goto readerr;
            }
        }

        //命令总是以数组长度开始，表示数组长度的第一个字符是*
        if (buf[0] != '*') {
            goto fmterr;
        }

        //意外结束
        if (buf[1] == '\0') {
            goto readerr;
        }

        //读取数组长度，即当前命令参数个数
        argc = atoi(buf + 1);
        if (argc < 1) {
            goto fmterr;
        }

        //创建参数对象数组
        argv = zmalloc(sizeof(robj *) * argc);
        fakeClient->argc = argc;
        fakeClient->argv = argv;

        //读取argc个参数
        for (j = 0; j < argc; j++) {
            //读取参数失败，释放已经读取的参数对象
            if (fgets(buf, sizeof(buf), fp) == NULL) {
                fakeClient->argc = j; /* Free up to j-1. */
                freeFakeClientArgv(fakeClient);
                goto readerr;
            }

            //bulk参数以参数长度开始，表示参数长度的第一个字符是$
            if (buf[0] != '$') {
                goto fmterr;
            }

            //读取参数长度
            len = strtol(buf + 1, NULL, 10);
            //用于存放参数字符串
            argsds = sdsnewlen(NULL, len);
            //读取参数到argsds
            if (len && fread(argsds, len, 1, fp) == 0) {
                //失败则释放argssds和参数数组
                sdsfree(argsds);
                fakeClient->argc = j; /* Free up to j-1. */
                freeFakeClientArgv(fakeClient);
                goto readerr;
            }

            //创建参数对象
            argv[j] = createObject(OBJ_STRING, argsds);
            //读取\r\n
            if (fread(buf, 2, 1, fp) == 0) {
                fakeClient->argc = j + 1; /* Free up to j. */
                freeFakeClientArgv(fakeClient);
                goto readerr; /* discard CRLF */
            }
        } //for循环读取参数并创建参数数组，在这里结束

        /* Command lookup */
        //寻找命令
        cmd = lookupCommand(argv[0]->ptr);
        //无该命令
        if (!cmd) {
            serverLog(LL_WARNING, "Unknown command '%s' reading the append only file", (char *) argv[0]->ptr);
            exit(1);
        }

        /* Run the command in the context of a fake client */
        //执行命令，传入的是伪客户端
        cmd->proc(fakeClient);

        /* The fake client should not have a reply */
        //因为是伪客户端，所以不必回复
        serverAssert(fakeClient->bufpos == 0 && listLength(fakeClient->reply) == 0);
        /* The fake client should never get blocked */
        //伪客户端不会被阻塞
        serverAssert((fakeClient->flags & CLIENT_BLOCKED) == 0);

        /* Clean up. Command code may have changed argv/argc so we use the
         * argv/argc of the client instead of the local variables. */
        //释放伪客户端参数对象数组
        freeFakeClientArgv(fakeClient);

        if (server.aof_load_truncated) {
            valid_up_to = ftello(fp);
        }
    } //while循环结束

    /* This point can only be reached when EOF is reached without errors.
     * If the client is in the middle of a MULTI/EXEC, log error and quit. */
    if (fakeClient->flags & CLIENT_MULTI) {
        goto uxeof;
    }

    //成功读取aof并执行了命令
    loaded_ok: /* DB loaded, cleanup and return C_OK to the caller. */
    //关闭文件
    fclose(fp);
    //释放伪客户端
    freeFakeClient(fakeClient);

    server.aof_state = old_aof_state;

    //更新loading状态
    stopLoading();

    aofUpdateCurrentSize();
    server.aof_rewrite_base_size = server.aof_current_size;

    return C_OK;

    //读取aof文件发生非eof错误
    readerr: /* Read error. If feof(fp) is true, fall through to unexpected EOF. */
    if (!feof(fp)) {
        if (fakeClient) {
            //释放伪客户端
            freeFakeClient(fakeClient);
        } /* avoid valgrind warning */
        serverLog(LL_WARNING, "Unrecoverable error reading the append only file: %s", strerror(errno));
        exit(1);
    }

    //未预期的eof
    uxeof: /* Unexpected AOF end of file. */
    if (server.aof_load_truncated) {
        serverLog(LL_WARNING, "!!! Warning: short read while loading the AOF file !!!");
        serverLog(LL_WARNING, "!!! Truncating the AOF at offset %llu !!!",
                  (unsigned long long) valid_up_to);
        if (valid_up_to == -1 || truncate(filename, valid_up_to) == -1) {
            if (valid_up_to == -1) {
                serverLog(LL_WARNING, "Last valid command offset is invalid");
            }
            else {
                serverLog(LL_WARNING, "Error truncating the AOF file: %s",
                          strerror(errno));
            }
        }
        else {
            /* Make sure the AOF file descriptor points to the end of the
             * file after the truncate call. */
            if (server.aof_fd != -1 && lseek(server.aof_fd, 0, SEEK_END) == -1) {
                serverLog(LL_WARNING, "Can't seek the end of the AOF file: %s",
                          strerror(errno));
            }
            else {
                serverLog(LL_WARNING,
                          "AOF loaded anyway because aof-load-truncated is enabled");
                goto loaded_ok;
            }
        }
    }

    //释放伪客户端
    if (fakeClient) {
        freeFakeClient(fakeClient);
    } /* avoid valgrind warning */

    serverLog(LL_WARNING,
              "Unexpected end of file reading the append only file. You can: 1) Make a backup of your AOF file, then use ./redis-check-aof --fix <filename>. 2) Alternatively you can set the 'aof-load-truncated' configuration option to yes and restart the server.");
    exit(1);

    //aof文件语法错误
    fmterr: /* Format error. */
    if (fakeClient) {
        freeFakeClient(fakeClient);
    } /* avoid valgrind warning */
    serverLog(LL_WARNING,
              "Bad file format reading the append only file: make a backup of your AOF file, then use ./redis-check-aof --fix <filename>");
    exit(1);
}

/* ----------------------------------------------------------------------------
 * AOF rewrite
 * ------------------------------------------------------------------------- */

/* Delegate writing an object to writing a bulk string or bulk long long.
 * This is not placed in rio.c since that adds the server.h dependency. */
//写入string对象，以bulk的形式
int rioWriteBulkObject(rio *r, robj *obj) {
    /* Avoid using getDecodedObject to help copy-on-write (we are often
     * in a child process when this function is called). */
    if (obj->encoding == OBJ_ENCODING_INT) {
        return rioWriteBulkLongLong(r, (long) obj->ptr);
    }
    else if (sdsEncodedObject(obj)) {
        return rioWriteBulkString(r, obj->ptr, sdslen(obj->ptr));
    }
    else {
        serverPanic("Unknown string encoding");
    }
}

/* Emit the commands needed to rebuild a list object.
 * The function returns 0 on error, 1 on success. */
//重写list类型
int rewriteListObject(rio *r, robj *key, robj *o) {
    long long count = 0, items = listTypeLength(o);

    if (o->encoding == OBJ_ENCODING_QUICKLIST) {
        quicklist *list = o->ptr;
        quicklistIter *li = quicklistGetIterator(list, AL_START_HEAD);
        quicklistEntry entry;

        while (quicklistNext(li, &entry)) {
            if (count == 0) {
                int cmd_items = (items > AOF_REWRITE_ITEMS_PER_CMD) ?
                                AOF_REWRITE_ITEMS_PER_CMD : items;
                if (rioWriteBulkCount(r, '*', 2 + cmd_items) == 0) { return 0; }
                if (rioWriteBulkString(r, "RPUSH", 5) == 0) { return 0; }
                if (rioWriteBulkObject(r, key) == 0) { return 0; }
            }

            if (entry.value) {
                if (rioWriteBulkString(r, (char *) entry.value, entry.sz) == 0) { return 0; }
            }
            else {
                if (rioWriteBulkLongLong(r, entry.longval) == 0) { return 0; }
            }
            if (++count == AOF_REWRITE_ITEMS_PER_CMD) { count = 0; }
            items--;
        }
        quicklistReleaseIterator(li);
    }
    else {
        serverPanic("Unknown list encoding");
    }
    return 1;
}

/* Emit the commands needed to rebuild a set object.
 * The function returns 0 on error, 1 on success. */
//重写set类型
int rewriteSetObject(rio *r, robj *key, robj *o) {
    long long count = 0;

    //集合大小
    long long items = setTypeSize(o);

    if (o->encoding == OBJ_ENCODING_INTSET) {
        int ii = 0;
        int64_t llval;

        //遍历intset
        while (intsetGet(o->ptr, ii++, &llval)) {
            //count=0表示一条新的sadd命令
            if (count == 0) {
                //每条sadd命令的value不能超过AOF_REWRITE_ITEMS_PER_CMD，以免命令太长
                int cmd_items = (items > AOF_REWRITE_ITEMS_PER_CMD) ?
                                AOF_REWRITE_ITEMS_PER_CMD : items;
                //写入参数个数
                if (rioWriteBulkCount(r, '*', 2 + cmd_items) == 0) {
                    return 0;
                }
                if (rioWriteBulkString(r, "SADD", 4) == 0) {
                    return 0;
                }
                //写入集合key
                if (rioWriteBulkObject(r, key) == 0) {
                    return 0;
                }
            }

            //写入value
            if (rioWriteBulkLongLong(r, llval) == 0) {
                return 0;
            }

            //当前sadd的value个数太多了
            if (++count == AOF_REWRITE_ITEMS_PER_CMD) { count = 0; }
            items--;
        }
    }
    else if (o->encoding == OBJ_ENCODING_HT) {
        dictIterator *di = dictGetIterator(o->ptr);
        dictEntry *de;

        while ((de = dictNext(di)) != NULL) {
            robj *eleobj = dictGetKey(de);
            if (count == 0) {
                //每条sadd命令的value不能超过AOF_REWRITE_ITEMS_PER_CMD，以免命令太长
                int cmd_items = (items > AOF_REWRITE_ITEMS_PER_CMD) ?
                                AOF_REWRITE_ITEMS_PER_CMD : items;
                //写入参数个数
                if (rioWriteBulkCount(r, '*', 2 + cmd_items) == 0) {
                    return 0;
                }
                if (rioWriteBulkString(r, "SADD", 4) == 0) {
                    return 0;
                }
                //写入集合key
                if (rioWriteBulkObject(r, key) == 0) {
                    return 0;
                }
            }

            //写入value
            if (rioWriteBulkObject(r, eleobj) == 0) {
                return 0;
            }

            //当前sadd的value个数太多了
            if (++count == AOF_REWRITE_ITEMS_PER_CMD) {
                count = 0;
            }
            items--;
        }
        dictReleaseIterator(di);
    }
    else {
        serverPanic("Unknown set encoding");
    }
    return 1;
}

/* Emit the commands needed to rebuild a sorted set object.
 * The function returns 0 on error, 1 on success. */
//略，大同小异
int rewriteSortedSetObject(rio *r, robj *key, robj *o) {
    long long count = 0, items = zsetLength(o);

    if (o->encoding == OBJ_ENCODING_ZIPLIST) {
        unsigned char *zl = o->ptr;
        unsigned char *eptr, *sptr;
        unsigned char *vstr;
        unsigned int vlen;
        long long vll;
        double score;

        eptr = ziplistIndex(zl, 0);
        serverAssert(eptr != NULL);
        sptr = ziplistNext(zl, eptr);
        serverAssert(sptr != NULL);

        while (eptr != NULL) {
            serverAssert(ziplistGet(eptr, &vstr, &vlen, &vll));
            score = zzlGetScore(sptr);

            if (count == 0) {
                int cmd_items = (items > AOF_REWRITE_ITEMS_PER_CMD) ?
                                AOF_REWRITE_ITEMS_PER_CMD : items;

                if (rioWriteBulkCount(r, '*', 2 + cmd_items * 2) == 0) { return 0; }
                if (rioWriteBulkString(r, "ZADD", 4) == 0) { return 0; }
                if (rioWriteBulkObject(r, key) == 0) { return 0; }
            }
            if (rioWriteBulkDouble(r, score) == 0) { return 0; }
            if (vstr != NULL) {
                if (rioWriteBulkString(r, (char *) vstr, vlen) == 0) { return 0; }
            }
            else {
                if (rioWriteBulkLongLong(r, vll) == 0) { return 0; }
            }
            zzlNext(zl, &eptr, &sptr);
            if (++count == AOF_REWRITE_ITEMS_PER_CMD) { count = 0; }
            items--;
        }
    }
    else if (o->encoding == OBJ_ENCODING_SKIPLIST) {
        zset *zs = o->ptr;
        dictIterator *di = dictGetIterator(zs->dict);
        dictEntry *de;

        while ((de = dictNext(di)) != NULL) {
            robj *eleobj = dictGetKey(de);
            double *score = dictGetVal(de);

            if (count == 0) {
                int cmd_items = (items > AOF_REWRITE_ITEMS_PER_CMD) ?
                                AOF_REWRITE_ITEMS_PER_CMD : items;

                if (rioWriteBulkCount(r, '*', 2 + cmd_items * 2) == 0) { return 0; }
                if (rioWriteBulkString(r, "ZADD", 4) == 0) { return 0; }
                if (rioWriteBulkObject(r, key) == 0) { return 0; }
            }
            if (rioWriteBulkDouble(r, *score) == 0) { return 0; }
            if (rioWriteBulkObject(r, eleobj) == 0) { return 0; }
            if (++count == AOF_REWRITE_ITEMS_PER_CMD) { count = 0; }
            items--;
        }
        dictReleaseIterator(di);
    }
    else {
        serverPanic("Unknown sorted zset encoding");
    }
    return 1;
}

/* Write either the key or the value of the currently selected item of a hash.
 * The 'hi' argument passes a valid Redis hash iterator.
 * The 'what' filed specifies if to write a key or a value and can be
 * either OBJ_HASH_KEY or OBJ_HASH_VALUE.
 *
 * The function returns 0 on error, non-zero on success. */
//写入hash迭代器当前的键值对
static int rioWriteHashIteratorCursor(rio *r, hashTypeIterator *hi, int what) {
    if (hi->encoding == OBJ_ENCODING_ZIPLIST) {
        unsigned char *vstr = NULL;
        unsigned int vlen = UINT_MAX;
        long long vll = LLONG_MAX;

        hashTypeCurrentFromZiplist(hi, what, &vstr, &vlen, &vll);
        if (vstr) {
            return rioWriteBulkString(r, (char *) vstr, vlen);
        }
        else {
            return rioWriteBulkLongLong(r, vll);
        }

    }
    else if (hi->encoding == OBJ_ENCODING_HT) {
        robj *value;

        hashTypeCurrentFromHashTable(hi, what, &value);
        return rioWriteBulkObject(r, value);
    }

    serverPanic("Unknown hash encoding");
    return 0;
}

/* Emit the commands needed to rebuild a hash object.
 * The function returns 0 on error, 1 on success. */
//重写hash类型
int rewriteHashObject(rio *r, robj *key, robj *o) {
    hashTypeIterator *hi;

    long long count = 0;

    //hash大小
    long long items = hashTypeLength(o);

    hi = hashTypeInitIterator(o);
    while (hashTypeNext(hi) != C_ERR) {
        if (count == 0) {
            //每条hmset的键值对不能太多，不然命令就太长了
            int cmd_items = (items > AOF_REWRITE_ITEMS_PER_CMD) ?
                            AOF_REWRITE_ITEMS_PER_CMD : items;

            //写入参数个数
            //cmd_items是键值对数量，*2才是键值对参数的个数
            if (rioWriteBulkCount(r, '*', 2 + cmd_items * 2) == 0) {
                return 0;
            }

            if (rioWriteBulkString(r, "HMSET", 5) == 0) {
                return 0;
            }

            //写入hash key
            if (rioWriteBulkObject(r, key) == 0) {
                return 0;
            }
        }

        //写入键值对
        if (rioWriteHashIteratorCursor(r, hi, OBJ_HASH_KEY) == 0) {
            return 0;
        }
        if (rioWriteHashIteratorCursor(r, hi, OBJ_HASH_VALUE) == 0) {
            return 0;
        }

        if (++count == AOF_REWRITE_ITEMS_PER_CMD) {
            count = 0;
        }
        items--;
    }

    hashTypeReleaseIterator(hi);

    return 1;
}

/* This function is called by the child rewriting the AOF file to read
 * the difference accumulated from the parent into a buffer, that is
 * concatenated at the end of the rewrite. */
//从管道读取父进程发送的新命令
ssize_t aofReadDiffFromParent(void) {
    char buf[65536]; /* Default pipe buffer size on most Linux systems. */
    ssize_t nread, total = 0;

    //读取新命令并添加到aof_child_diff
    while ((nread = read(server.aof_pipe_read_data_from_parent, buf, sizeof(buf))) > 0) {
        server.aof_child_diff = sdscatlen(server.aof_child_diff, buf, nread);
        total += nread;
    }
    return total;
}

/* Write a sequence of commands able to fully rebuild the dataset into
 * "filename". Used both by REWRITEAOF and BGREWRITEAOF.
 *
 * In order to minimize the number of commands needed in the rewritten
 * log Redis uses variadic commands when possible, such as RPUSH, SADD
 * and ZADD. However at max AOF_REWRITE_ITEMS_PER_CMD items per time
 * are inserted using a single command. */
//重写aof文件
int rewriteAppendOnlyFile(char *filename) {
    dictIterator *di = NULL;
    dictEntry *de;
    rio aof;
    FILE *fp;
    char tmpfile[256];
    int j;
    long long now = mstime();
    char byte;
    size_t processed = 0;

    /* Note that we have to use a different temp name here compared to the
     * one used by rewriteAppendOnlyFileBackground() function. */
    //打开临时文件
    snprintf(tmpfile, 256, "temp-rewriteaof-%d.aof", (int) getpid());
    fp = fopen(tmpfile, "w");
    if (!fp) {
        serverLog(LL_WARNING, "Opening the temp file for AOF rewrite in rewriteAppendOnlyFile(): %s", strerror(errno));
        return C_ERR;
    }

    server.aof_child_diff = sdsempty();

    //创建底层为文件的rio
    rioInitWithFile(&aof, fp);

    //是否在重写过程中同时fsync？
    if (server.aof_rewrite_incremental_fsync) {
        rioSetAutoSync(&aof, AOF_AUTOSYNC_BYTES);
    }

    //遍历所有数据库进行重写
    for (j = 0; j < server.dbnum; j++) {
        //选择当前数据库命令
        char selectcmd[] = "*2\r\n$6\r\nSELECT\r\n";
        redisDb *db = server.db + j;
        dict *d = db->dict;

        //跳过空数据库
        if (dictSize(d) == 0) {
            continue;
        }

        //当前数据库迭代器
        di = dictGetSafeIterator(d);
        if (!di) {
            fclose(fp);
            return C_ERR;
        }

        /* SELECT the new DB */
        //写入aof文件：选择数据库命令select
        if (rioWrite(&aof, selectcmd, sizeof(selectcmd) - 1) == 0) {
            goto werr;
        }
        //写入选择的数据库号
        if (rioWriteBulkLongLong(&aof, j) == 0) {
            goto werr;
        }

        /* Iterate this DB writing every entry */
        //遍历数据库键值对
        while ((de = dictNext(di)) != NULL) {
            sds keystr;
            robj key, *o;
            long long expiretime;

            //获取键值对
            keystr = dictGetKey(de);
            o = dictGetVal(de);
            initStaticStringObject(key, keystr);

            //获取过期时间
            expiretime = getExpire(db, &key);
            /* If this key is already expired skip it */
            //跳过过期的key
            if (expiretime != -1 && expiretime < now) {
                continue;
            }

            /* Save the key and associated value */
            //重写set类型
            if (o->type == OBJ_STRING) {
                /* Emit a SET command */
                //写入set命令
                char cmd[] = "*3\r\n$3\r\nSET\r\n";
                if (rioWrite(&aof, cmd, sizeof(cmd) - 1) == 0) {
                    goto werr;
                }
                /* Key and value */
                //写入键值对
                if (rioWriteBulkObject(&aof, &key) == 0) {
                    goto werr;
                }
                if (rioWriteBulkObject(&aof, o) == 0) {
                    goto werr;
                }
            }
            else if (o->type == OBJ_LIST) {
                //重写list类型
                if (rewriteListObject(&aof, &key, o) == 0) { goto werr; }
            }
            else if (o->type == OBJ_SET) {
                //重写set类型
                if (rewriteSetObject(&aof, &key, o) == 0) {
                    goto werr;
                }
            }
            else if (o->type == OBJ_ZSET) {
                //重写sortedset类型
                if (rewriteSortedSetObject(&aof, &key, o) == 0) { goto werr; }
            }
            else if (o->type == OBJ_HASH) {
                //重写hash类型
                if (rewriteHashObject(&aof, &key, o) == 0) { goto werr; }
            }
            else {
                serverPanic("Unknown object type");
            }

            /* Save the expire time */
            //写入key的过期时间
            if (expiretime != -1) {
                char cmd[] = "*3\r\n$9\r\nPEXPIREAT\r\n";
                if (rioWrite(&aof, cmd, sizeof(cmd) - 1) == 0) { goto werr; }
                if (rioWriteBulkObject(&aof, &key) == 0) { goto werr; }
                if (rioWriteBulkLongLong(&aof, expiretime) == 0) { goto werr; }
            }

            /* Read some diff from the parent process from time to time. */
            //读取父进程发送的新命令，保存在aof_child_diff
            if (aof.processed_bytes > processed + 1024 * 10) {
                processed = aof.processed_bytes;
                aofReadDiffFromParent();
            }
        }//while循环结束，结束当前数据库遍历

        //释放迭代器
        dictReleaseIterator(di);
        di = NULL;

    } //遍历所有数据库的for循环在此结束

    /* Do an initial slow fsync here while the parent is still sending
     * data, in order to make the next final fsync faster. */
    //将数据从标准io缓冲刷到内核缓冲
    if (fflush(fp) == EOF) {
        goto werr;
    }
    //将数据从内核缓冲刷到磁盘
    if (fsync(fileno(fp)) == -1) {
        goto werr;
    }

    /* Read again a few times to get more data from the parent.
     * We can't read forever (the server may receive data from clients
     * faster than it is able to send data to the child), so we try to read
     * some more data in a loop as soon as there is a good chance more data
     * will come. If it looks like we are wasting time, we abort (this
     * happens after 20 ms without new data). */
    //再次从管道读取父进程发送的新命令
    //如果管道一直没有可读事件发生，放弃读取新命令
    int nodata = 0;
    mstime_t start = mstime();
    while (mstime() - start < 1000 && nodata < 20) {
        //阻塞等待管道可读，超时时间为1
        if (aeWait(server.aof_pipe_read_data_from_parent, AE_READABLE, 1) <= 0) {
            nodata++;
            continue;
        }
        nodata = 0; /* Start counting from zero, we stop on N *contiguous*
                       timeouts. */
        aofReadDiffFromParent();
    }

    /* Ask the master to stop sending diffs. */
    //告诉主进程停止向管道发送新命令
    if (write(server.aof_pipe_write_ack_to_parent, "!", 1) != 1) {
        goto werr;
    }

    //设置管道非阻塞
    if (anetNonBlock(NULL, server.aof_pipe_read_ack_from_parent) != ANET_OK) {
        goto werr;
    }

    /* We read the ACK from the server using a 10 seconds timeout. Normally
     * it should reply ASAP, but just in case we lose its reply, we are sure
     * the child will eventually get terminated. */
    //非阻塞地读取父进程的ack
    if (syncRead(server.aof_pipe_read_ack_from_parent, &byte, 1, 5000) != 1 ||
        byte != '!') {
        goto werr;
    }
    serverLog(LL_NOTICE, "Parent agreed to stop sending diffs. Finalizing AOF...");

    /* Read the final diff if any. */
    //告诉主进程停止向管道发送新命令之前可能父进程又发送了一些新命令
    aofReadDiffFromParent();

    /* Write the received diff to the file. */
    serverLog(LL_NOTICE, "Concatenating %.2f MB of AOF diff received from parent.",
              (double) sdslen(server.aof_child_diff) / (1024 * 1024));

    //将子进程重写期间父进程发送的新命令写入重写文件
    if (rioWrite(&aof, server.aof_child_diff, sdslen(server.aof_child_diff)) == 0) {
        goto werr;
    }

    /* Make sure data will not remain on the OS's output buffers */
    //数据刷到磁盘
    if (fflush(fp) == EOF) { goto werr; }
    if (fsync(fileno(fp)) == -1) { goto werr; }

    //关闭文件
    if (fclose(fp) == EOF) { goto werr; }

    /* Use RENAME to make sure the DB file is changed atomically only
     * if the generate DB file is ok. */
    //重命名重写文件
    if (rename(tmpfile, filename) == -1) {
        serverLog(LL_WARNING, "Error moving temp append only file on the final destination: %s", strerror(errno));
        unlink(tmpfile);
        return C_ERR;
    }
    serverLog(LL_NOTICE, "SYNC append only file rewrite performed");
    return C_OK;

    werr:
    serverLog(LL_WARNING, "Write error writing append only file on disk: %s", strerror(errno));
    fclose(fp);
    unlink(tmpfile);
    if (di) { dictReleaseIterator(di); }
    return C_ERR;
}

/* ----------------------------------------------------------------------------
 * AOF rewrite pipes for IPC
 * -------------------------------------------------------------------------- */

/* This event handler is called when the AOF rewriting child sends us a
 * single '!' char to signal we should stop sending buffer diffs. The
 * parent sends a '!' as well to acknowledge. */
//子进程向父进程发送"已完成重写"通知
void aofChildPipeReadable(aeEventLoop *el, int fd, void *privdata, int mask) {
    char byte;
    UNUSED(el);
    UNUSED(privdata);
    UNUSED(mask);

    if (read(fd, &byte, 1) == 1 && byte == '!') {
        serverLog(LL_NOTICE, "AOF rewrite child asks to stop sending diffs.");

        //停止向子进程发送新命令
        server.aof_stop_sending_diff = 1;

        //向子进程发送ack
        if (write(server.aof_pipe_write_ack_to_child, "!", 1) != 1) {
            /* If we can't send the ack, inform the user, but don't try again
             * since in the other side the children will use a timeout if the
             * kernel can't buffer our write, or, the children was
             * terminated. */
            serverLog(LL_WARNING, "Can't send ACK to AOF child: %s",
                      strerror(errno));
        }
    }
    /* Remove the handler since this can be called only one time during a
     * rewrite. */
    //从事件循环删除该管道的可读事件，因为子进程仅会发送一次通知
    aeDeleteFileEvent(server.el, server.aof_pipe_read_ack_from_child, AE_READABLE);
}

/* Create the pipes used for parent - child process IPC during rewrite.
 * We have a data pipe used to send AOF incremental diffs to the child,
 * and two other pipes used by the children to signal it finished with
 * the rewrite so no more data should be written, and another for the
 * parent to acknowledge it understood this new condition. */
//创建aof重写时父子进程通信的管道
int aofCreatePipes(void) {
    int fds[6] = {-1, -1, -1, -1, -1, -1};
    int j;

    //管道0：父进程向子进程发送新的命令
    if (pipe(fds) == -1) {
        goto error;
    } /* parent -> children data. */

    //管道1：子进程向父进程通告已完成重写
    if (pipe(fds + 2) == -1) {
        goto error;
    } /* children -> parent ack. */

    //管道2：父进程发送对子进程"已完成重写"的ack
    if (pipe(fds + 4) == -1) {
        goto error;
    } /* children -> parent ack. */

    /* Parent -> children data is non blocking. */
    //管道0是非阻塞的
    if (anetNonBlock(NULL, fds[0]) != ANET_OK) {
        goto error;
    }
    if (anetNonBlock(NULL, fds[1]) != ANET_OK) {
        goto error;
    }

    //为管道1读端注册可读事件
    //当发生可读事件时，表示子进程向父进程发送"已完成重写"通知
    if (aeCreateFileEvent(server.el, fds[2], AE_READABLE, aofChildPipeReadable, NULL) == AE_ERR) {
        goto error;
    }

    //更新服务器全局变量
    server.aof_pipe_write_data_to_child = fds[1];
    server.aof_pipe_read_data_from_parent = fds[0];

    server.aof_pipe_write_ack_to_parent = fds[3];
    server.aof_pipe_read_ack_from_child = fds[2];

    server.aof_pipe_write_ack_to_child = fds[5];
    server.aof_pipe_read_ack_from_parent = fds[4];

    server.aof_stop_sending_diff = 0;

    return C_OK;

    error:
    serverLog(LL_WARNING, "Error opening /setting AOF rewrite IPC pipes: %s",
              strerror(errno));
    for (j = 0; j < 6; j++)
        if (fds[j] != -1) {
            close(fds[j]);
        }
    return C_ERR;
}

//关闭管道
void aofClosePipes(void) {
    //从事件循环删除管道可读/可写事件
    aeDeleteFileEvent(server.el, server.aof_pipe_read_ack_from_child, AE_READABLE);
    aeDeleteFileEvent(server.el, server.aof_pipe_write_data_to_child, AE_WRITABLE);

    //关闭管道
    close(server.aof_pipe_write_data_to_child);
    close(server.aof_pipe_read_data_from_parent);
    close(server.aof_pipe_write_ack_to_parent);
    close(server.aof_pipe_read_ack_from_child);
    close(server.aof_pipe_write_ack_to_child);
    close(server.aof_pipe_read_ack_from_parent);
}

/* ----------------------------------------------------------------------------
 * AOF background rewrite
 * ------------------------------------------------------------------------- */

/* This is how rewriting of the append only file in background works:
 *
 * 1) The user calls BGREWRITEAOF
 * 2) Redis calls this function, that forks():
 *    2a) the child rewrite the append only file in a temp file.
 *    2b) the parent accumulates differences in server.aof_rewrite_buf.
 * 3) When the child finished '2a' exists.
 * 4) The parent will trap the exit code, if it's OK, will append the
 *    data accumulated into server.aof_rewrite_buf into the temp file, and
 *    finally will rename(2) the temp file in the actual file name.
 *    The the new file is reopened as the new append only file. Profit!
 */
//在子进程中重写aof文件
//重写期间，服务器将新的命令同时写到aof缓冲和aof重写缓冲中
int rewriteAppendOnlyFileBackground(void) {
    pid_t childpid;
    long long start;

    //有rdb/aof子进程已经在执行
    if (server.aof_child_pid != -1 || server.rdb_child_pid != -1) {
        return C_ERR;
    }

    //创建aof重写时父子进程通信的管道
    if (aofCreatePipes() != C_OK) {
        return C_ERR;
    }

    start = ustime();

    //子进程
    if ((childpid = fork()) == 0) {
        char tmpfile[256];

        /* Child */
        //关闭子进程listen套接字
        closeListeningSockets(0);

        //命名子进程
        redisSetProcTitle("redis-aof-rewrite");

        //重写的文件名
        snprintf(tmpfile, 256, "temp-rewriteaof-bg-%d.aof", (int) getpid());

        //重写aof文件
        if (rewriteAppendOnlyFile(tmpfile) == C_OK) {
            size_t private_dirty = zmalloc_get_private_dirty();

            if (private_dirty) {
                serverLog(LL_NOTICE,
                          "AOF rewrite: %zu MB of memory used by copy-on-write",
                          private_dirty / (1024 * 1024));
            }
            //成功重写，正常退出子进程
            exitFromChild(0);
        }
        else {
            exitFromChild(1);
        }
    }
    else {
        /* Parent */
        //父进程
        server.stat_fork_time = ustime() - start;
        server.stat_fork_rate = (double) zmalloc_used_memory() * 1000000 / server.stat_fork_time /
                                (1024 * 1024 * 1024); /* GB per second. */
        latencyAddSampleIfNeeded("fork", server.stat_fork_time / 1000);

        //fork子进程失败
        if (childpid == -1) {
            serverLog(LL_WARNING, "Can't rewrite append only file in background: fork: %s", strerror(errno));
            //关闭管道
            aofClosePipes();
            return C_ERR;
        }

        serverLog(LL_NOTICE, "Background append only file rewriting started by pid %d", childpid);

        //更新全局aof状态
        server.aof_rewrite_scheduled = 0;
        server.aof_rewrite_time_start = time(NULL);
        server.aof_child_pid = childpid;

        //尽量避免重哈希以免触发cow
        updateDictResizePolicy();

        /* We set appendseldb to -1 in order to force the next call to the
         * feedAppendOnlyFile() to issue a SELECT command, so the differences
         * accumulated by the parent into server.aof_rewrite_buf will start
         * with a SELECT statement and it will be safe to merge. */
        server.aof_selected_db = -1;
        replicationScriptCacheFlush();
        return C_OK;
    }

    //父子进程都不会执行到这里
    return C_OK; /* unreached */
}

//命令实现
//BGREWRITEAOF
void bgrewriteaofCommand(client *c) {
    if (server.aof_child_pid != -1) {
        //已经有aof重写子进程在执行了
        addReplyError(c, "Background append only file rewriting already in progress");
    }
    else if (server.rdb_child_pid != -1) {
        //有rdb子进程在执行，等待其执行完再执行aof
        //在serverCron中检查
        server.aof_rewrite_scheduled = 1;
        addReplyStatus(c, "Background append only file rewriting scheduled");
    }
    else if (rewriteAppendOnlyFileBackground() == C_OK) {
        //执行后台aof重写
        addReplyStatus(c, "Background append only file rewriting started");
    }
    else {
        addReply(c, shared.err);
    }
}

void aofRemoveTempFile(pid_t childpid) {
    char tmpfile[256];

    snprintf(tmpfile, 256, "temp-rewriteaof-bg-%d.aof", (int) childpid);
    unlink(tmpfile);
}

/* Update the server.aof_current_size field explicitly using stat(2)
 * to check the size of the file. This is useful after a rewrite or after
 * a restart, normally the size is updated just adding the write length
 * to the current length, that is much faster. */
//更新aof文件当前大小
void aofUpdateCurrentSize(void) {
    struct redis_stat sb;
    mstime_t latency;

    latencyStartMonitor(latency);
    if (redis_fstat(server.aof_fd, &sb) == -1) {
        serverLog(LL_WARNING, "Unable to obtain the AOF file length. stat: %s",
                  strerror(errno));
    }
    else {
        server.aof_current_size = sb.st_size;
    }
    latencyEndMonitor(latency);
    latencyAddSampleIfNeeded("aof-fstat", latency);
}

/* A background append only file rewriting (BGREWRITEAOF) terminated its work.
 * Handle this. */
//aof重写子进程终止后父进程调用该函数
void backgroundRewriteDoneHandler(int exitcode, int bysignal) {
    //正常终止
    if (!bysignal && exitcode == 0) {
        int newfd, oldfd;
        char tmpfile[256];
        long long now = ustime();
        mstime_t latency;

        serverLog(LL_NOTICE, "Background AOF rewrite terminated with success");

        /* Flush the differences accumulated by the parent to the
         * rewritten AOF. */
        //将重写期间的新命令写入重写后的aof文件
        //在重写期间父进程已经通过管道发送一部分新命令给子进程
        //但是仍可能还有一部分新命令
        latencyStartMonitor(latency);
        snprintf(tmpfile, 256, "temp-rewriteaof-bg-%d.aof",
                 (int) server.aof_child_pid);
        //打开重写后的aof文件
        newfd = open(tmpfile, O_WRONLY | O_APPEND);
        if (newfd == -1) {
            serverLog(LL_WARNING,
                      "Unable to open the temporary AOF produced by the child: %s", strerror(errno));
            goto cleanup;
        }

        //将重写链表的数据写入重写后的aof文件
        if (aofRewriteBufferWrite(newfd) == -1) {
            serverLog(LL_WARNING,
                      "Error trying to flush the parent diff to the rewritten AOF: %s", strerror(errno));
            close(newfd);
            goto cleanup;
        }
        latencyEndMonitor(latency);
        latencyAddSampleIfNeeded("aof-rewrite-diff-write", latency);

        serverLog(LL_NOTICE,
                  "Residual parent diff successfully flushed to the rewritten AOF (%.2f MB)",
                  (double) aofRewriteBufferSize() / (1024 * 1024));

        /* The only remaining thing to do is to rename the temporary file to
         * the configured file and switch the file descriptor used to do AOF
         * writes. We don't want close(2) or rename(2) calls to block the
         * server on old file deletion.
         *
         * There are two possible scenarios:
         *
         * 1) AOF is DISABLED and this was a one time rewrite. The temporary
         * file will be renamed to the configured file. When this file already
         * exists, it will be unlinked, which may block the server.
         *
         * 2) AOF is ENABLED and the rewritten AOF will immediately start
         * receiving writes. After the temporary file is renamed to the
         * configured file, the original AOF file descriptor will be closed.
         * Since this will be the last reference to that file, closing it
         * causes the underlying file to be unlinked, which may block the
         * server.
         *
         * To mitigate the blocking effect of the unlink operation (either
         * caused by rename(2) in scenario 1, or by close(2) in scenario 2), we
         * use a background thread to take care of this. First, we
         * make scenario 1 identical to scenario 2 by opening the target file
         * when it exists. The unlink operation after the rename(2) will then
         * be executed upon calling close(2) for its descriptor. Everything to
         * guarantee atomicity for this switch has already happened by then, so
         * we don't care what the outcome or duration of that close operation
         * is, as long as the file descriptor is released again. */
        //todo 重命名重写后的aof文件
        //todo 好像是unlink会阻塞服务器？
        if (server.aof_fd == -1) {
            /* AOF disabled */

            /* Don't care if this fails: oldfd will be -1 and we handle that.
             * One notable case of -1 return is if the old file does
             * not exist. */
            oldfd = open(server.aof_filename, O_RDONLY | O_NONBLOCK);
        }
        else {
            /* AOF enabled */
            oldfd = -1; /* We'll set this to the current AOF filedes later. */
        }

        /* Rename the temporary file. This will not unlink the target file if
         * it exists, because we reference it with "oldfd". */
        latencyStartMonitor(latency);
        if (rename(tmpfile, server.aof_filename) == -1) {
            serverLog(LL_WARNING,
                      "Error trying to rename the temporary AOF file %s into %s: %s",
                      tmpfile,
                      server.aof_filename,
                      strerror(errno));
            close(newfd);
            if (oldfd != -1) { close(oldfd); }
            goto cleanup;
        }
        latencyEndMonitor(latency);
        latencyAddSampleIfNeeded("aof-rename", latency);

        if (server.aof_fd == -1) {
            /* AOF disabled, we don't need to set the AOF file descriptor
             * to this new file, so we can close it. */
            close(newfd);
        }
        else {
            /* AOF enabled, replace the old fd with the new one. */
            oldfd = server.aof_fd;
            server.aof_fd = newfd;
            if (server.aof_fsync == AOF_FSYNC_ALWAYS)
                aof_fsync(newfd);
            else if (server.aof_fsync == AOF_FSYNC_EVERYSEC) {
                aof_background_fsync(newfd);
            }
            server.aof_selected_db = -1; /* Make sure SELECT is re-issued */
            aofUpdateCurrentSize();
            server.aof_rewrite_base_size = server.aof_current_size;

            /* Clear regular AOF buffer since its contents was just written to
             * the new AOF from the background rewrite buffer. */
            sdsfree(server.aof_buf);
            server.aof_buf = sdsempty();
        }

        server.aof_lastbgrewrite_status = C_OK;

        serverLog(LL_NOTICE, "Background AOF rewrite finished successfully");
        /* Change state from WAIT_REWRITE to ON if needed */
        if (server.aof_state == AOF_WAIT_REWRITE) {
            server.aof_state = AOF_ON;
        }

        /* Asynchronously close the overwritten AOF. */
        if (oldfd != -1) { bioCreateBackgroundJob(BIO_CLOSE_FILE, (void *) (long) oldfd, NULL, NULL); }

        serverLog(LL_VERBOSE,
                  "Background AOF rewrite signal handler took %lldus", ustime() - now);
    }
    else if (!bysignal && exitcode != 0) {
        /* SIGUSR1 is whitelisted, so we have a way to kill a child without
         * tirggering an error conditon. */
        if (bysignal != SIGUSR1) {
            server.aof_lastbgrewrite_status = C_ERR;
        }
        serverLog(LL_WARNING,
                  "Background AOF rewrite terminated with error");
    }
    else {
        server.aof_lastbgrewrite_status = C_ERR;

        serverLog(LL_WARNING,
                  "Background AOF rewrite terminated by signal %d", bysignal);
    }

    cleanup:
    //关闭父子进程通信管道
    aofClosePipes();
    //重置重写缓冲(链表)
    aofRewriteBufferReset();
    aofRemoveTempFile(server.aof_child_pid);

    //更新aof相关状态信息
    server.aof_child_pid = -1;
    server.aof_rewrite_time_last = time(NULL) - server.aof_rewrite_time_start;
    server.aof_rewrite_time_start = -1;

    /* Schedule a new rewrite if we are waiting for it to switch the AOF ON. */
    if (server.aof_state == AOF_WAIT_REWRITE) {
        server.aof_rewrite_scheduled = 1;
    }
}
