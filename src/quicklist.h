/* quicklist.h - A generic doubly linked quicklist implementation
 *
 * Copyright (c) 2014, Matt Stancliff <matt@genges.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this quicklist of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this quicklist of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Redis nor the names of its contributors may be used
 *     to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

//双向链表优点
//O(1)插入/删除节点

//双向链表缺点
//每个节点都要prev/next/value指针的额外内存消耗
//对缓存不友好

//ziplist优点
//节省内存
//对缓存友好

//ziplist缺点
//每次插入/删除都要realloc
//若不是表尾插入/删除，要移动其他元素
//插入/删除可能引发prevlensize的级联更新

//quicklist
//结合了ziplist和双向链表的一种数据结构
//每个链表节点都是一个容量较小的ziplist
//这样既节省了内存，又不会因为ziplist过大而在插入/删除元素时开销过大
//redis list类型底层实现只有quicklist一种

#ifndef __QUICKLIST_H__
#define __QUICKLIST_H__

/* Node, quicklist, and Iterator are the only data structures used currently. */

/* quicklistNode is a 32 byte struct describing a ziplist for a quicklist.
 * We use bit fields keep the quicklistNode at 32 bytes.
 * count: 16 bits, max 65536 (max zl bytes is 65k, so max count actually < 32k).
 * encoding: 2 bits, RAW=1, LZF=2.
 * container: 2 bits, NONE=1, ZIPLIST=2.
 * recompress: 1 bit, bool, true if node is temporarry decompressed for usage.
 * attempted_compress: 1 bit, boolean, used for verifying during testing.
 * extra: 12 bits, free for future use; pads out the remainder of 32 bits */
//quicklist链表节点
typedef struct quicklistNode {
    //上一个节点
    struct quicklistNode *prev;

    //下一个节点
    struct quicklistNode *next;

    //节点内部的ziplist或quicklistLZF
    unsigned char *zl;

    //ziplist大小，单位字节
    unsigned int sz;             /* ziplist size in bytes */

    //ziplist元素数量
    unsigned int count : 16;     /* count of items in ziplist */

    //是否有压缩？
    unsigned int encoding : 2;   /* RAW==1 or LZF==2 */

    unsigned int container : 2;  /* NONE==1 or ZIPLIST==2 */

    //解压缩之前置该标记为1，方便之后再次压缩
    unsigned int recompress : 1; /* was this node previous compressed? */

    unsigned int attempted_compress : 1; /* node can't compress; too small */

    unsigned int extra : 10; /* more bits to steal for future usage */
} quicklistNode;

/* quicklistLZF is a 4+N byte struct holding 'sz' followed by 'compressed'.
 * 'sz' is byte length of 'compressed' field.
 * 'compressed' is LZF data with total (compressed) length 'sz'
 * NOTE: uncompressed length is stored in quicklistNode->sz.
 * When quicklistNode->zl is compressed, node->zl points to a quicklistLZF */
//压缩后的ziplist
typedef struct quicklistLZF {
    //压缩后的大小，即compressed数组大小
    unsigned int sz; /* LZF size in bytes*/

    //压缩后的数据
    char compressed[];
} quicklistLZF;

/* quicklist is a 32 byte struct (on 64-bit systems) describing a quicklist.
 * 'count' is the number of total entries.
 * 'len' is the number of quicklist nodes.
 * 'compress' is: -1 if compression disabled, otherwise it's the number
 *                of quicklistNodes to leave uncompressed at ends of quicklist.
 * 'fill' is the user-requested (or default) fill factor. */
typedef struct quicklist {
    //头节点
    quicklistNode *head;

    //尾节点
    quicklistNode *tail;

    //链表元素数量，即所有ziplist的元素数量总和
    unsigned long count;        /* total count of all entries in all ziplists */

    //链表节点个数
    unsigned int len;           /* number of quicklistNodes */

    //每个节点ziplist允许的大小
    //若是正数，表示每个ziplist允许的元素个数
    //若是负数
    //-5: ziplist大小不能超过64 Kb
    //-4: ziplist大小不能超过32 Kb
    //-3: ziplist大小不能超过16 Kb
    //-2: ziplist大小不能超过8 Kb(默认值)
    //-1: ziplist大小不能超过4 Kb
    int fill : 16;              /* fill factor for individual nodes */

    //首尾两端不进行压缩的节点数量
    //0表示关闭压缩
    unsigned int compress : 16; /* depth of end nodes not to compress;0=off */
} quicklist;

//迭代器
typedef struct quicklistIter {
    //迭代的quicklist
    const quicklist *quicklist;

    //当前迭代到的节点
    quicklistNode *current;

    //当前迭代到的元素
    //zi并不是指向ziplist起始，而是ziplist第offset个元素
    unsigned char *zi;

    //当前元素在当前ziplist的偏移量
    long offset; /* offset in current ziplist */

    //迭代方向
    int direction;
} quicklistIter;

//quicklist元素
typedef struct quicklistEntry {
    //元素所属quicklist
    const quicklist *quicklist;

    //元素所属节点
    quicklistNode *node;

    //元素当前迭代到的元素
    //zi并不是指向所属ziplist起始，而是所属ziplist第offset个元素
    unsigned char *zi;

    //保存元素的值
    unsigned char *value;
    long long longval;

    unsigned int sz;

    //元素在ziplist的偏移量
    //小于0表示从尾部开始
    int offset;
} quicklistEntry;

#define QUICKLIST_HEAD 0
#define QUICKLIST_TAIL -1

/* quicklist node encodings */
#define QUICKLIST_NODE_ENCODING_RAW 1
#define QUICKLIST_NODE_ENCODING_LZF 2

/* quicklist compression disable */
#define QUICKLIST_NOCOMPRESS 0

/* quicklist container formats */
#define QUICKLIST_NODE_CONTAINER_NONE 1
#define QUICKLIST_NODE_CONTAINER_ZIPLIST 2

#define quicklistNodeIsCompressed(node)                                        \
    ((node)->encoding == QUICKLIST_NODE_ENCODING_LZF)

/* Prototypes */
//创建新的quicklist
quicklist *quicklistCreate(void);

//创建新的quicklist，指定了ziplist允许的大小和压缩深度
quicklist *quicklistNew(int fill, int compress);

//设定压缩深度
void quicklistSetCompressDepth(quicklist *quicklist, int depth);

//设定ziplist允许的大小
void quicklistSetFill(quicklist *quicklist, int fill);

//设定ziplist允许的大小和压缩深度
void quicklistSetOptions(quicklist *quicklist, int fill, int depth);

//释放quicklist
void quicklistRelease(quicklist *quicklist);

//在quicklist表头插入新元素
int quicklistPushHead(quicklist *quicklist, void *value, const size_t sz);

//在quicklist表尾插入新元素
int quicklistPushTail(quicklist *quicklist, void *value, const size_t sz);

//在quicklist头部或尾部插入新元素
void quicklistPush(quicklist *quicklist, void *value, const size_t sz,
                   int where);

//创建一个包含给定ziplist的链表节点，并插入quicklist表尾
void quicklistAppendZiplist(quicklist *quicklist, unsigned char *zl);

//将ziplist的元素添加到quicklist表尾，并释放ziplist
quicklist *quicklistAppendValuesFromZiplist(quicklist *quicklist,
                                            unsigned char *zl);

//以给定的ziplist创建新的quicklist，同时释放ziplsit
quicklist *quicklistCreateFromZiplist(int fill, int compress,
                                      unsigned char *zl);

//在entry之后插入新元素
void quicklistInsertAfter(quicklist *quicklist, quicklistEntry *node,
                          void *value, const size_t sz);

//在entry之前插入新元素
void quicklistInsertBefore(quicklist *quicklist, quicklistEntry *node,
                           void *value, const size_t sz);

//从quicklist删除entry所代表的元素
void quicklistDelEntry(quicklistIter *iter, quicklistEntry *entry);

//更新第index个元素的值
int quicklistReplaceAtIndex(quicklist *quicklist, long index, void *data,
                            int sz);

//删除自start起的连续count个元素
int quicklistDelRange(quicklist *quicklist, const long start, const long stop);

//创建quicklist迭代器
quicklistIter *quicklistGetIterator(const quicklist *quicklist, int direction);

//创建一个开始于idx的迭代器
quicklistIter *quicklistGetIteratorAtIdx(const quicklist *quicklist,
                                         int direction, const long long idx);

//获取迭代器的下一个元素，写入entry
int quicklistNext(quicklistIter *iter, quicklistEntry *node);

//释放迭代器
void quicklistReleaseIterator(quicklistIter *iter);

//复制quicklist
quicklist *quicklistDup(quicklist *orig);

//获取第idx个元素，写入entry
int quicklistIndex(const quicklist *quicklist, const long long index,
                   quicklistEntry *entry);

void quicklistRewind(quicklist *quicklist, quicklistIter *li);

void quicklistRewindTail(quicklist *quicklist, quicklistIter *li);

//将quicklist的表尾元素移动到表头
void quicklistRotate(quicklist *quicklist);

//从表头/表尾弹出元素，元素的值写入data/sval
int quicklistPopCustom(quicklist *quicklist, int where, unsigned char **data,
                       unsigned int *sz, long long *sval,
                       void *(*saver)(unsigned char *data, unsigned int sz));

//从两端弹出元素
int quicklistPop(quicklist *quicklist, int where, unsigned char **data,
                 unsigned int *sz, long long *slong);

//返回quicklist元素数量
unsigned int quicklistCount(quicklist *ql);

//就是ziplistCompare
int quicklistCompare(unsigned char *p1, unsigned char *p2, int p2_len);

//获取压缩后的数据，写入data，返回压缩后的数据大小
size_t quicklistGetLzf(const quicklistNode *node, void **data);

#ifdef REDIS_TEST
int quicklistTest(int argc, char *argv[]);
#endif

/* Directions for iterators */
#define AL_START_HEAD 0
#define AL_START_TAIL 1

#endif /* __QUICKLIST_H__ */
