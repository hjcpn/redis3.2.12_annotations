/*
 * Copyright (c) 2009-2012, Salvatore Sanfilippo <antirez at gmail dot com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Redis nor the names of its contributors may be used
 *     to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

//用于集合的字典类型
//dictType setDictType = {
//        dictEncObjHash,            /* hash function */
//        NULL,                      /* key dup */
//        NULL,                      /* val dup */
//        dictEncObjKeyCompare,      /* key compare */
//        dictObjectDestructor, /* key destructor */
//        NULL                       /* val destructor */
//}
//集合只需要设置key，val为NULL
//key为共享robj对象

#include "server.h"

/*-----------------------------------------------------------------------------
 * Set Commands
 *----------------------------------------------------------------------------*/

void sunionDiffGenericCommand(client *c, robj **setkeys, int setnum, robj *dstkey, int op);

/* Factory method to return a set that *can* hold "value". When the object has
 * an integer-encodable value, an intset will be returned. Otherwise a regular
 * hash table. */
//根据value的类型创建集合对象
robj *setTypeCreate(robj *value) {

    if (isObjectRepresentableAsLongLong(value, NULL) == C_OK) {
        //value可以表示为整数，则使用intset实现集合
        return createIntsetObject();
    }
    //否则创建字典实现的集合
    return createSetObject();
}

/* Add the specified value into a set. The function takes care of incrementing
 * the reference count of the object if needed in order to retain a copy.
 *
 * If the value was already member of the set, nothing is done and 0 is
 * returned, otherwise the new element is added and 1 is returned. */
//往集合中添加新元素
int setTypeAdd(robj *subject, robj *value) {
    long long llval;
    //字典实现的集合
    if (subject->encoding == OBJ_ENCODING_HT) {
        //添加的时候value作为字典的key，字典的val不用设置
        if (dictAdd(subject->ptr, value, NULL) == DICT_OK) {
            //共享robj，故增加引用计数
            incrRefCount(value);
            return 1;
        }
    }
    else if (subject->encoding == OBJ_ENCODING_INTSET) {
        //intset实现的集合，需要检查value是否能转换为整数
        if (isObjectRepresentableAsLongLong(value, &llval) == C_OK) {
            uint8_t success = 0;
            subject->ptr = intsetAdd(subject->ptr, llval, &success);
            if (success) {
                /* Convert to regular set when the intset contains
                 * too many entries. */
                //检查是否需要将intset集合转换为dict集合
                if (intsetLen(subject->ptr) > server.set_max_intset_entries) {
                    setTypeConvert(subject, OBJ_ENCODING_HT);
                }
                return 1;
            }
        }
        else {
            /* Failed to get integer from object, convert to regular set. */
            //value无法转换为整数，则需要转换集合底层实现
            setTypeConvert(subject, OBJ_ENCODING_HT);

            /* The set *was* an intset and this value is not integer
             * encodable, so dictAdd should always work. */
            //添加value到字典
            serverAssertWithInfo(NULL, value, dictAdd(subject->ptr, value, NULL) == DICT_OK);
            //增加value引用计数
            incrRefCount(value);
            return 1;
        }
    }
    else {
        serverPanic("Unknown set encoding");
    }

    return 0;
}

//从集合中删除元素
int setTypeRemove(robj *setobj, robj *value) {
    long long llval;
    if (setobj->encoding == OBJ_ENCODING_HT) {
        //比较函数为对string对象进行比较
        if (dictDelete(setobj->ptr, value) == DICT_OK) {
            //检查是否需要缩小字典
            if (htNeedsResize(setobj->ptr)) {
                dictResize(setobj->ptr);
            }
            return 1;
        }
    }
    else if (setobj->encoding == OBJ_ENCODING_INTSET) {
        //从intset中删除元素，先检查value能否表示为整数
        if (isObjectRepresentableAsLongLong(value, &llval) == C_OK) {
            int success;
            setobj->ptr = intsetRemove(setobj->ptr, llval, &success);
            if (success) {
                return 1;
            }
        }
    }
    else {
        serverPanic("Unknown set encoding");
    }

    return 0;
}

//检查value是否在集合中
int setTypeIsMember(robj *subject, robj *value) {
    long long llval;
    if (subject->encoding == OBJ_ENCODING_HT) {
        //直接比较对象
        return dictFind((dict *) subject->ptr, value) != NULL;
    }
    else if (subject->encoding == OBJ_ENCODING_INTSET) {
        //或者比较对象存储的整数值
        if (isObjectRepresentableAsLongLong(value, &llval) == C_OK) {
            return intsetFind((intset *) subject->ptr, llval);
        }
    }
    else {
        serverPanic("Unknown set encoding");
    }
    return 0;
}

//集合迭代器
setTypeIterator *setTypeInitIterator(robj *subject) {
    setTypeIterator *si = zmalloc(sizeof(setTypeIterator));

    si->subject = subject;
    si->encoding = subject->encoding;

    //字典迭代器或intset迭代器？
    if (si->encoding == OBJ_ENCODING_HT) {
        si->di = dictGetIterator(subject->ptr);
    }
    else if (si->encoding == OBJ_ENCODING_INTSET) {
        si->ii = 0;
    }
    else {
        serverPanic("Unknown set encoding");
    }

    return si;
}

//释放集合迭代器
void setTypeReleaseIterator(setTypeIterator *si) {
    if (si->encoding == OBJ_ENCODING_HT) {
        //需要单独释放字典迭代器
        dictReleaseIterator(si->di);
    }
    zfree(si);
}

/* Move to the next entry in the set. Returns the object at the current
 * position.
 *
 * Since set elements can be internally be stored as redis objects or
 * simple arrays of integers, setTypeNext returns the encoding of the
 * set object you are iterating, and will populate the appropriate pointer
 * (objele) or (llele) accordingly.
 *
 * Note that both the objele and llele pointers should be passed and cannot
 * be NULL since the function will try to defensively populate the non
 * used field with values which are easy to trap if misused.
 *
 * When there are no longer elements -1 is returned.
 * Returned objects ref count is not incremented, so this function is
 * copy on write friendly. */
//获取集合迭代器的下一个元素，写入objele或者llele中，同时返回value类型
int setTypeNext(setTypeIterator *si, robj **objele, int64_t *llele) {
    if (si->encoding == OBJ_ENCODING_HT) {
        dictEntry *de = dictNext(si->di);
        if (de == NULL) {
            return -1;
        }
        //获取dictEntry的key，即集合的value
        *objele = dictGetKey(de);
        *llele = -123456789; /* Not needed. Defensive. */
    }
    else if (si->encoding == OBJ_ENCODING_INTSET) {
        //获取当前索引的值，并自增索引
        if (!intsetGet(si->subject->ptr, si->ii++, llele)) {
            return -1;
        }
        *objele = NULL; /* Not needed. Defensive. */
    }
    else {
        serverPanic("Wrong set encoding in setTypeNext");
    }

    //同时返回value类型
    return si->encoding;
}

/* The not copy on write friendly version but easy to use version
 * of setTypeNext() is setTypeNextObject(), returning new objects
 * or incrementing the ref count of returned objects. So if you don't
 * retain a pointer to this object you should call decrRefCount() against it.
 *
 * This function is the way to go for write operations where COW is not
 * an issue as the result will be anyway of incrementing the ref count. */
//获取集合迭代器的下一个元素，总是封装为对象形式
robj *setTypeNextObject(setTypeIterator *si) {
    int64_t intele;
    robj *objele;
    int encoding;

    encoding = setTypeNext(si, &objele, &intele);

    //根据value类型创建robj对象
    switch (encoding) {
        case -1:
            return NULL;
        case OBJ_ENCODING_INTSET:
            //为整数创建对象
            return createStringObjectFromLongLong(intele);
        case OBJ_ENCODING_HT:
            //只是简单地增加对象的引用计数
            incrRefCount(objele);
            return objele;
        default:
            serverPanic("Unsupported encoding");
    }

    return NULL; /* just to suppress warnings */
}

/* Return random element from a non empty set.
 * The returned element can be a int64_t value if the set is encoded
 * as an "intset" blob of integers, or a redis object if the set
 * is a regular set.
 *
 * The caller provides both pointers to be populated with the right
 * object. The return value of the function is the object->encoding
 * field of the object and is used by the caller to check if the
 * int64_t pointer or the redis object pointer was populated.
 *
 * Note that both the objele and llele pointers should be passed and cannot
 * be NULL since the function will try to defensively populate the non
 * used field with values which are easy to trap if misused.
 *
 * When an object is returned (the set was a real set) the ref count
 * of the object is not incremented so this function can be considered
 * copy on write friendly. */
//随机返回集合的一个元素，写入objele/llele
int setTypeRandomElement(robj *setobj, robj **objele, int64_t *llele) {
    if (setobj->encoding == OBJ_ENCODING_HT) {
        dictEntry *de = dictGetRandomKey(setobj->ptr);
        *objele = dictGetKey(de);
        *llele = -123456789; /* Not needed. Defensive. */
    }
    else if (setobj->encoding == OBJ_ENCODING_INTSET) {
        *llele = intsetRandom(setobj->ptr);
        *objele = NULL; /* Not needed. Defensive. */
    }
    else {
        serverPanic("Unknown set encoding");
    }
    return setobj->encoding;
}

//返回集合大小
unsigned long setTypeSize(robj *subject) {
    if (subject->encoding == OBJ_ENCODING_HT) {
        return dictSize((dict *) subject->ptr);
    }
    else if (subject->encoding == OBJ_ENCODING_INTSET) {
        return intsetLen((intset *) subject->ptr);
    }
    else {
        serverPanic("Unknown set encoding");
    }
}

/* Convert the set to specified encoding. The resulting dict (when converting
 * to a hash table) is presized to hold the number of elements in the original
 * set. */
//转换集合的底层实现，intset -> dict
void setTypeConvert(robj *setobj, int enc) {
    setTypeIterator *si;
    serverAssertWithInfo(NULL, setobj, setobj->type == OBJ_SET && setobj->encoding == OBJ_ENCODING_INTSET);

    if (enc == OBJ_ENCODING_HT) {
        int64_t intele;
        dict *d = dictCreate(&setDictType, NULL);
        robj *element;

        /* Presize the dict to avoid rehashing */
        //创建字典，该字典一开始就有足够的bucket来容纳集合元素，避免了重哈希
        dictExpand(d, intsetLen(setobj->ptr));

        /* To add the elements we extract integers and create redis objects */
        //集合迭代器
        si = setTypeInitIterator(setobj);
        //遍历集合，添加元素到字典
        while (setTypeNext(si, &element, &intele) != -1) {
            element = createStringObjectFromLongLong(intele);
            serverAssertWithInfo(NULL, element, dictAdd(d, element, NULL) == DICT_OK);
        }
        //释放迭代器
        setTypeReleaseIterator(si);

        //设置底层实现方式
        setobj->encoding = OBJ_ENCODING_HT;

        //释放原来的intset数据结构
        zfree(setobj->ptr);

        //指向新的实现
        setobj->ptr = d;
    }
    else {
        serverPanic("Unsupported set conversion");
    }
}

//命令实现
//SADD key member [member ...]
void saddCommand(client *c) {
    robj *set;
    int j, added = 0;

    set = lookupKeyWrite(c->db, c->argv[1]);
    if (set == NULL) {
        //key若不存在，先创建
        set = setTypeCreate(c->argv[2]);
        dbAdd(c->db, c->argv[1], set);
    }
    else {
        //类型检查
        if (set->type != OBJ_SET) {
            addReply(c, shared.wrongtypeerr);
            return;
        }
    }

    //遍历value并添加，部分添加失败不会影响其他添加成功的
    for (j = 2; j < c->argc; j++) {
        //尝试将string对象编码类型由raw转换为embstr或int，节省内存
        c->argv[j] = tryObjectEncoding(c->argv[j]);
        if (setTypeAdd(set, c->argv[j])) {
            added++;
        }
    }

    if (added) {
        signalModifiedKey(c->db, c->argv[1]);
        notifyKeyspaceEvent(NOTIFY_SET, "sadd", c->argv[1], c->db->id);
    }

    server.dirty += added;

    //回复成功添加的元素个数
    addReplyLongLong(c, added);
}

//命令实现
//SREM key member [member ...]
void sremCommand(client *c) {
    robj *set;
    int j, deleted = 0, keyremoved = 0;

    //key不存在或类型错误
    if ((set = lookupKeyWriteOrReply(c, c->argv[1], shared.czero)) == NULL || checkType(c, set, OBJ_SET)) {
        return;
    }

    //遍历参数删除元素
    for (j = 2; j < c->argc; j++) {
        if (setTypeRemove(set, c->argv[j])) {
            deleted++;
            //如果集合为空了，从db删除集合
            if (setTypeSize(set) == 0) {
                dbDelete(c->db, c->argv[1]);
                keyremoved = 1;
                break;
            }
        }
    }

    if (deleted) {
        signalModifiedKey(c->db, c->argv[1]);
        notifyKeyspaceEvent(NOTIFY_SET, "srem", c->argv[1], c->db->id);
        if (keyremoved) {
            notifyKeyspaceEvent(NOTIFY_GENERIC, "del", c->argv[1],
                                c->db->id);
        }
        server.dirty += deleted;
    }
    addReplyLongLong(c, deleted);
}

//命令实现
//SMOVE source destination member
void smoveCommand(client *c) {
    robj *srcset, *dstset, *ele;

    //获取src/dst集合
    srcset = lookupKeyWrite(c->db, c->argv[1]);
    dstset = lookupKeyWrite(c->db, c->argv[2]);

    //获取待移动的元素member
    ele = c->argv[3] = tryObjectEncoding(c->argv[3]);

    /* If the source key does not exist return 0 */
    //src集合不存在
    if (srcset == NULL) {
        addReply(c, shared.czero);
        return;
    }

    /* If the source key has the wrong type, or the destination key
     * is set and has the wrong type, return with an error. */
    //src/dst不是集合类型
    if (checkType(c, srcset, OBJ_SET) ||
        (dstset && checkType(c, dstset, OBJ_SET))) {
        return;
    }

    /* If srcset and dstset are equal, SMOVE is a no-op */
    //src/dst是同一个集合
    if (srcset == dstset) {
        addReply(c, setTypeIsMember(srcset, ele) ? shared.cone : shared.czero);
        return;
    }

    /* If the element cannot be removed from the src set, return 0. */
    //从src集合删除元素
    if (!setTypeRemove(srcset, ele)) {
        addReply(c, shared.czero);
        return;
    }
    notifyKeyspaceEvent(NOTIFY_SET, "srem", c->argv[1], c->db->id);

    /* Remove the src set from the database when empty */
    //若src集合为空，从db删除
    if (setTypeSize(srcset) == 0) {
        dbDelete(c->db, c->argv[1]);
        notifyKeyspaceEvent(NOTIFY_GENERIC, "del", c->argv[1], c->db->id);
    }

    /* Create the destination set when it doesn't exist */
    //若dst集合不存在，先创建
    if (!dstset) {
        dstset = setTypeCreate(ele);
        dbAdd(c->db, c->argv[2], dstset);
    }

    signalModifiedKey(c->db, c->argv[1]);
    signalModifiedKey(c->db, c->argv[2]);
    server.dirty++;

    /* An extra key has changed when ele was successfully added to dstset */
    //向dst集合添加元素
    if (setTypeAdd(dstset, ele)) {
        server.dirty++;
        notifyKeyspaceEvent(NOTIFY_SET, "sadd", c->argv[2], c->db->id);
    }
    addReply(c, shared.cone);
}

//命令实现
//SISMEMBER key member
void sismemberCommand(client *c) {
    robj *set;

    //key不存在或类型错误
    if ((set = lookupKeyReadOrReply(c, c->argv[1], shared.czero)) == NULL || checkType(c, set, OBJ_SET)) {
        return;
    }

    //尝试将string对象编码类型由raw转换为embstr或int，节省内存
    c->argv[2] = tryObjectEncoding(c->argv[2]);

    //member是否存在？
    if (setTypeIsMember(set, c->argv[2])) {
        addReply(c, shared.cone);
    }
    else {
        addReply(c, shared.czero);
    }
}

//命令实现
//SCARD key
void scardCommand(client *c) {
    robj *o;

    //key不存在或类型错误
    if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.czero)) == NULL ||
        checkType(c, o, OBJ_SET)) {
        return;
    }

    //返回集合元素数量
    addReplyLongLong(c, setTypeSize(o));
}

/* Handle the "SPOP key <count>" variant. The normal version of the
 * command is handled by the spopCommand() function itself. */

/* How many times bigger should be the set compared to the remaining size
 * for us to use the "create new set" strategy? Read later in the
 * implementation for more info. */
#define SPOP_MOVE_STRATEGY_MUL 5

//命令实现
//SPOP key [count]
void spopWithCountCommand(client *c) {
    long l;
    unsigned long count, size;
    robj *set;

    /* Get the count argument */
    //获取count
    if (getLongFromObjectOrReply(c, c->argv[2], &l, NULL) != C_OK) {
        return;
    }

    //count是否有效？
    if (l >= 0) {
        count = (unsigned) l;
    }
    else {
        addReply(c, shared.outofrangeerr);
        return;
    }

    /* Make sure a key with the name inputted exists, and that it's type is
     * indeed a set. Otherwise, return nil */
    //key不存在或类型错误
    if ((set = lookupKeyReadOrReply(c, c->argv[1], shared.emptymultibulk)) == NULL || checkType(c, set, OBJ_SET)) {
        return;
    }

    /* If count is zero, serve an empty multibulk ASAP to avoid special
     * cases later. */
    if (count == 0) {
        addReply(c, shared.emptymultibulk);
        return;
    }

    //集合元素个数
    size = setTypeSize(set);

    /* Generate an SPOP keyspace notification */
    notifyKeyspaceEvent(NOTIFY_SET, "spop", c->argv[1], c->db->id);
    server.dirty += count;

    /* CASE 1:
     * The number of requested elements is greater than or equal to
     * the number of elements inside the set: simply return the whole set. */
    //情况1：count大于等于集合元素数量，直接返回整个集合
    if (count >= size) {
        /* We just return the entire set */
        //只有一个集合的并集操作
        sunionDiffGenericCommand(c, c->argv + 1, 1, NULL, SET_OP_UNION);

        /* Delete the set as it is now empty */
        //从db删除集合
        dbDelete(c->db, c->argv[1]);
        notifyKeyspaceEvent(NOTIFY_GENERIC, "del", c->argv[1], c->db->id);

        /* Propagate this command as an DEL operation */
        rewriteClientCommandVector(c, 2, shared.del, c->argv[1]);
        signalModifiedKey(c->db, c->argv[1]);
        server.dirty++;
        return;
    }

    /* Case 2 and 3 require to replicate SPOP as a set of SERM commands.
     * Prepare our replication argument vector. Also send the array length
     * which is common to both the code paths. */
    robj *propargv[3];
    propargv[0] = createStringObject("SREM", 4);
    propargv[1] = c->argv[1];
    addReplyMultiBulkLen(c, count);

    /* Common iteration vars. */
    robj *objele;
    int encoding;
    int64_t llele;

    //pop之后还剩下的元素个数
    unsigned long remaining = size - count; /* Elements left after SPOP. */

    /* If we are here, the number of requested elements is less than the
     * number of elements inside the set. Also we are sure that count < size.
     * Use two different strategies.
     *
     * CASE 2: The number of elements to return is small compared to the
     * set size. We can just extract random elements and return them to
     * the set. */
    //情况2：count远远小于集合大小
    //直接pop count个随机元素
    if (remaining * SPOP_MOVE_STRATEGY_MUL > count) {
        while (count--) {
            encoding = setTypeRandomElement(set, &objele, &llele);
            if (encoding == OBJ_ENCODING_INTSET) {
                objele = createStringObjectFromLongLong(llele);
            }
            else {
                incrRefCount(objele);
            }

            /* Return the element to the client and remove from the set. */
            //添加pop元素到回复缓冲
            addReplyBulk(c, objele);

            //将元素从集合删除
            setTypeRemove(set, objele);

            /* Replicate/AOF this command as an SREM operation */
            propargv[2] = objele;
            alsoPropagate(server.sremCommand, c->db->id, propargv, 3,
                          PROPAGATE_AOF | PROPAGATE_REPL);
            decrRefCount(objele);
        }
    }
    else {
        /* CASE 3: The number of elements to return is very big, approaching
         * the size of the set itself. After some time extracting random elements
         * from such a set becomes computationally expensive, so we use
         * a different strategy, we extract random elements that we don't
         * want to return (the elements that will remain part of the set),
         * creating a new set as we do this (that will be stored as the original
         * set). Then we return the elements left in the original set and
         * release it. */
        //情况3：count比较大
        // 此时不要弹出count个元素，而是弹出remaining个元素，然后添加到netset集合
        //netset集合写入数据库代替原来的集合
        //原来的集合作为被弹出元素，添加到回复缓冲
        robj *newset = NULL;

        /* Create a new set with just the remaining elements. */
        //弹出remaining个元素，添加到netset集合
        while (remaining--) {
            encoding = setTypeRandomElement(set, &objele, &llele);
            if (encoding == OBJ_ENCODING_INTSET) {
                objele = createStringObjectFromLongLong(llele);
            }
            else {
                incrRefCount(objele);
            }

            if (!newset) {
                newset = setTypeCreate(objele);
            }

            //添加元素到newset
            setTypeAdd(newset, objele);
            //从原来的集合删除元素
            setTypeRemove(set, objele);

            decrRefCount(objele);
        }

        /* Assign the new set as the key value. */
        //将newset写到数据库，覆盖原来的集合
        //需要先增加原来集合的引用计数，因为dbOverwrite会减少其引用计数
        incrRefCount(set); /* Protect the old set value. */
        dbOverwrite(c->db, c->argv[1], newset);

        /* Tranfer the old set to the client and release it. */
        //遍历原来的集合，将其元素添加到回复缓冲
        setTypeIterator *si;
        si = setTypeInitIterator(set);
        while ((encoding = setTypeNext(si, &objele, &llele)) != -1) {
            if (encoding == OBJ_ENCODING_INTSET) {
                objele = createStringObjectFromLongLong(llele);
            }
            else {
                incrRefCount(objele);
            }
            addReplyBulk(c, objele);

            /* Replicate/AOF this command as an SREM operation */
            propargv[2] = objele;
            alsoPropagate(server.sremCommand, c->db->id, propargv, 3,
                          PROPAGATE_AOF | PROPAGATE_REPL);

            decrRefCount(objele);
        }
        setTypeReleaseIterator(si);
        decrRefCount(set);
    }

    /* Don't propagate the command itself even if we incremented the
     * dirty counter. We don't want to propagate an SPOP command since
     * we propagated the command as a set of SREMs operations using
     * the alsoPropagate() API. */
    decrRefCount(propargv[0]);
    preventCommandPropagation(c);
    signalModifiedKey(c->db, c->argv[1]);
    server.dirty++;
}

//命令实现
//SPOP key [count]
void spopCommand(client *c) {
    robj *set, *ele, *aux;
    int64_t llele;
    int encoding;

    if (c->argc == 3) {
        //存在count参数
        spopWithCountCommand(c);
        return;
    }
    else if (c->argc > 3) {
        //大于3个参数，命令语法错误
        addReply(c, shared.syntaxerr);
        return;
    }

    /* Make sure a key with the name inputted exists, and that it's type is
     * indeed a set */
    //key不存在或类型错误
    if ((set = lookupKeyWriteOrReply(c, c->argv[1], shared.nullbulk)) == NULL || checkType(c, set, OBJ_SET)) {
        return;
    }

    /* Get a random element from the set */
    //获取随机元素
    encoding = setTypeRandomElement(set, &ele, &llele);

    /* Remove the element from the set */
    //从集合中删除该随机元素
    if (encoding == OBJ_ENCODING_INTSET) {
        //ele用于添加到回复缓冲
        ele = createStringObjectFromLongLong(llele);
        set->ptr = intsetRemove(set->ptr, llele, NULL);
    }
    else {
        //因为该元素还要添加到回复缓冲，所以在从集合删除之前先增加引用计数
        incrRefCount(ele);
        setTypeRemove(set, ele);
    }

    notifyKeyspaceEvent(NOTIFY_SET, "spop", c->argv[1], c->db->id);

    /* Replicate/AOF this command as an SREM operation */
    aux = createStringObject("SREM", 4);
    rewriteClientCommandVector(c, 3, aux, c->argv[1], ele);
    decrRefCount(ele);
    decrRefCount(aux);

    /* Add the element to the reply */
    //添加获取的随机元素到回复缓冲
    addReplyBulk(c, ele);

    /* Delete the set if it's empty */
    //如果集合为空，从db删除
    if (setTypeSize(set) == 0) {
        dbDelete(c->db, c->argv[1]);
        notifyKeyspaceEvent(NOTIFY_GENERIC, "del", c->argv[1], c->db->id);
    }

    /* Set has been modified */
    signalModifiedKey(c->db, c->argv[1]);
    server.dirty++;
}

/* handle the "SRANDMEMBER key <count>" variant. The normal version of the
 * command is handled by the srandmemberCommand() function itself. */

/* How many times bigger should be the set compared to the requested size
 * for us to don't use the "remove elements" strategy? Read later in the
 * implementation for more info. */
#define SRANDMEMBER_SUB_STRATEGY_MUL 3

//SRANDMEMBER命令的底层实现
//SRANDMEMBER key [count]
void srandmemberWithCountCommand(client *c) {
    long l;
    unsigned long count, size;
    int uniq = 1;
    robj *set, *ele;
    int64_t llele;
    int encoding;

    dict *d;

    //获取count
    if (getLongFromObjectOrReply(c, c->argv[2], &l, NULL) != C_OK) {
        return;
    }

    if (l >= 0) {
        count = (unsigned) l;
    }
    else {
        //count为负数时，即使count大于集合个数，也返回count个元素，其中会有重复
        /* A negative count means: return the same elements multiple times
         * (i.e. don't remove the extracted element after every extraction). */
        count = -l;
        uniq = 0;
    }

    //key不存在或类型错误
    if ((set = lookupKeyReadOrReply(c, c->argv[1], shared.emptymultibulk))
        == NULL || checkType(c, set, OBJ_SET)) {
        return;
    }

    //集合元素个数
    size = setTypeSize(set);

    /* If count is zero, serve it ASAP to avoid special cases later. */
    if (count == 0) {
        addReply(c, shared.emptymultibulk);
        return;
    }

    /* CASE 1: The count was negative, so the extraction method is just:
     * "return N random elements" sampling the whole set every time.
     * This case is trivial and can be served without auxiliary data
     * structures. */
    //count是负数，获取count个随机元素，不管是不是重复
    if (!uniq) {
        //添加数组长度到回复缓冲
        addReplyMultiBulkLen(c, count);
        //获取count个随机元素
        while (count--) {
            encoding = setTypeRandomElement(set, &ele, &llele);
            if (encoding == OBJ_ENCODING_INTSET) {
                addReplyBulkLongLong(c, llele);
            }
            else {
                addReplyBulk(c, ele);
            }
        }
        return;
    }

    /* CASE 2:
     * The number of requested elements is greater than the number of
     * elements inside the set: simply return the whole set. */
    //count非负数且大于集合元素个数，返回整个集合
    if (count >= size) {
        sunionDiffGenericCommand(c, c->argv + 1, 1, NULL, SET_OP_UNION);
        return;
    }

    /* For CASE 3 and CASE 4 we need an auxiliary dictionary. */
    //count小于集合元素个数，需要辅助字典
    d = dictCreate(&setDictType, NULL);

    /* CASE 3:
     * The number of elements inside the set is not greater than
     * SRANDMEMBER_SUB_STRATEGY_MUL times the number of requested elements.
     * In this case we create a set from scratch with all the elements, and
     * subtract random elements to reach the requested number of elements.
     *
     * This is done because if the number of requsted elements is just
     * a bit less than the number of elements in the set, the natural approach
     * used into CASE 3 is highly inefficient. */
    //count相对于集合元素个数来说很大，获取count个随机元素效率太低
    //将集合的元素全部添加到辅助字典，从辅助字典弹出size - count个元素
    //剩下的就是count个随机元素了
    if (count * SRANDMEMBER_SUB_STRATEGY_MUL > size) {
        setTypeIterator *si;

        /* Add all the elements into the temporary dictionary. */
        //将集合的元素全部添加到辅助字典
        si = setTypeInitIterator(set);
        while ((encoding = setTypeNext(si, &ele, &llele)) != -1) {
            int retval = DICT_ERR;

            if (encoding == OBJ_ENCODING_INTSET) {
                retval = dictAdd(d, createStringObjectFromLongLong(llele), NULL);
            }
            else {
                retval = dictAdd(d, dupStringObject(ele), NULL);
            }
            serverAssert(retval == DICT_OK);
        }
        //释放迭代器
        setTypeReleaseIterator(si);
        serverAssert(dictSize(d) == size);

        /* Remove random elements to reach the right count. */
        //从辅助字典弹出size - count个元素
        while (size > count) {
            dictEntry *de;
            de = dictGetRandomKey(d);
            dictDelete(d, dictGetKey(de));
            size--;
        }
    }

        /* CASE 4: We have a big set compared to the requested number of elements.
         * In this case we can simply get random elements from the set and add
         * to the temporary set, trying to eventually get enough unique elements
         * to reach the specified count. */
        //count的个数相对于集合大小来说很小，直接获取count个随机元素添加到辅助字典
    else {
        unsigned long added = 0;

        //添加count个随机元素添加到辅助字典
        while (added < count) {
            encoding = setTypeRandomElement(set, &ele, &llele);
            if (encoding == OBJ_ENCODING_INTSET) {
                ele = createStringObjectFromLongLong(llele);
            }
            else {
                ele = dupStringObject(ele);
            }
            /* Try to add the object to the dictionary. If it already exists
             * free it, otherwise increment the number of objects we have
             * in the result dictionary. */
            if (dictAdd(d, ele, NULL) == DICT_OK) {
                added++;
            }
            else {
                decrRefCount(ele);
            }
        }
    }

    /* CASE 3 & 4: send the result to the user. */
    //将辅助字典的元素添加到回复缓冲
    {
        dictIterator *di;
        dictEntry *de;

        addReplyMultiBulkLen(c, count);
        di = dictGetIterator(d);
        while ((de = dictNext(di)) != NULL)
            addReplyBulk(c, dictGetKey(de));
        dictReleaseIterator(di);
        dictRelease(d);
    }
}

//命令实现
//SRANDMEMBER key [count]
void srandmemberCommand(client *c) {
    robj *set, *ele;
    int64_t llele;
    int encoding;

    if (c->argc == 3) {
        //存在count参数
        srandmemberWithCountCommand(c);
        return;
    }
    else if (c->argc > 3) {
        //命令语法错误
        addReply(c, shared.syntaxerr);
        return;
    }

    //key不存在或类型错误
    if ((set = lookupKeyReadOrReply(c, c->argv[1], shared.nullbulk)) == NULL || checkType(c, set, OBJ_SET)) {
        return;
    }

    //获取随机元素
    encoding = setTypeRandomElement(set, &ele, &llele);
    if (encoding == OBJ_ENCODING_INTSET) {
        addReplyBulkLongLong(c, llele);
    }
    else {
        addReplyBulk(c, ele);
    }
}

//按集合大小排序用的比较函数
int qsortCompareSetsByCardinality(const void *s1, const void *s2) {
    return setTypeSize(*(robj **) s1) - setTypeSize(*(robj **) s2);
}

/* This is used by SDIFF and in this case we can receive NULL that should
 * be handled as empty sets. */
//按照集合大小从大到小排序
int qsortCompareSetsByRevCardinality(const void *s1, const void *s2) {
    robj *o1 = *(robj **) s1, *o2 = *(robj **) s2;
    return (o2 ? setTypeSize(o2) : 0) - (o1 ? setTypeSize(o1) : 0);
}

//inter命令底层实现
//SINTER key [key ...]
//SINTERSTORE destination key [key ...]
void sinterGenericCommand(client *c, robj **setkeys, unsigned long setnum, robj *dstkey) {
    //集合对象数组
    robj **sets = zmalloc(sizeof(robj *) * setnum);

    setTypeIterator *si;
    robj *eleobj, *dstset = NULL;
    int64_t intobj;
    void *replylen = NULL;
    unsigned long j, cardinality = 0;
    int encoding;

    //遍历检查setkeys是否存在，类型是否正确
    for (j = 0; j < setnum; j++) {
        robj *setobj = dstkey ? lookupKeyWrite(c->db, setkeys[j]) : lookupKeyRead(c->db, setkeys[j]);

        //key不存在
        if (!setobj) {
            zfree(sets);
            if (dstkey) {
                if (dbDelete(c->db, dstkey)) {
                    signalModifiedKey(c->db, dstkey);
                    server.dirty++;
                }
                addReply(c, shared.czero);
            }
            else {
                addReply(c, shared.emptymultibulk);
            }
            return;
        }

        //检查类型
        if (checkType(c, setobj, OBJ_SET)) {
            zfree(sets);
            return;
        }

        //检查通过，保存集合对象到数组
        sets[j] = setobj;
    }

    /* Sort sets from the smallest to largest, this will improve our
     * algorithm's performance */
    //对集合按照元素数量从小到大排序
    qsort(sets, setnum, sizeof(robj *), qsortCompareSetsByCardinality);

    /* The first thing we should output is the total number of elements...
     * since this is a multi-bulk write, but at this stage we don't know
     * the intersection set size, so we use a trick, append an empty object
     * to the output list and save the pointer to later modify it with the
     * right length */
    if (!dstkey) {
        //因为需要返回一个数组给用户，数组内容是bulk
        //但是目前不知道数组长度，故先在回复链表尾部添加一个占位节点
        //之后再将数组长度写入该节点
        replylen = addDeferredMultiBulkLength(c);
    }
    else {
        /* If we have a target key where to store the resulting set
         * create this key with an empty set inside */
        //创建dst集合对象
        dstset = createIntsetObject();
    }

    /* Iterate all the elements of the first (smallest) set, and test
     * the element against all the other sets, if at least one set does
     * not include the element it is discarded */
    //第一个集合，即最小集合的迭代器
    si = setTypeInitIterator(sets[0]);
    //遍历第一个集合，对其中的每个元素，检查该元素是否存在于其他所有集合
    while ((encoding = setTypeNext(si, &eleobj, &intobj)) != -1) {
        for (j = 1; j < setnum; j++) {
            //当前集合和最小集合是同一个集合，跳过
            if (sets[j] == sets[0]) {
                continue;
            }

            //最小集合是intset
            if (encoding == OBJ_ENCODING_INTSET) {
                /* intset with intset is simple... and fast */
                //且当前集合也是intset，直接在intset中寻找当前元素
                if (sets[j]->encoding == OBJ_ENCODING_INTSET && !intsetFind((intset *) sets[j]->ptr, intobj)) {
                    break;

                    /* in order to compare an integer with an object we
                     * have to use the generic function, creating an object
                     * for this */
                    //如果当前集合是字典，则需要将整数元素先封装成对象再寻找
                }
                else if (sets[j]->encoding == OBJ_ENCODING_HT) {
                    eleobj = createStringObjectFromLongLong(intobj);
                    if (!setTypeIsMember(sets[j], eleobj)) {
                        decrRefCount(eleobj);
                        break;
                    }
                    decrRefCount(eleobj);
                }

                //若最小集合是字典
            }
            else if (encoding == OBJ_ENCODING_HT) {
                /* Optimization... if the source object is integer
                 * encoded AND the target set is an intset, we can get
                 * a much faster path. */
                //若当前元素是int编码
                //且当前集合是intset
                //直接在intset中寻找当前元素
                if (eleobj->encoding == OBJ_ENCODING_INT &&
                    sets[j]->encoding == OBJ_ENCODING_INTSET &&
                    !intsetFind((intset *) sets[j]->ptr, (long) eleobj->ptr)) {
                    break;
                    /* else... object to object check is easy as we use the
                     * type agnostic API here. */
                    //否则寻找对象
                }
                else if (!setTypeIsMember(sets[j], eleobj)) {
                    break;
                }
            }
        } //for循环结束

        /* Only take action when all sets contain the member */
        //if成立说明该元素在所有集合中都出现了
        if (j == setnum) {
            //添加元素到回复缓冲
            if (!dstkey) {
                if (encoding == OBJ_ENCODING_HT) {
                    addReplyBulk(c, eleobj);
                }
                else {
                    addReplyBulkLongLong(c, intobj);
                }
                cardinality++;
            }
            else {
                //或添加元素到dst集合
                if (encoding == OBJ_ENCODING_INTSET) {
                    eleobj = createStringObjectFromLongLong(intobj);
                    setTypeAdd(dstset, eleobj);
                    decrRefCount(eleobj);
                }
                else {
                    setTypeAdd(dstset, eleobj);
                }
            }
        }
    } //while循环结束

    //释放迭代器
    setTypeReleaseIterator(si);

    if (dstkey) {
        /* Store the resulting set into the target, if the intersection
         * is not an empty set. */
        //如果dst集合已经存在，需要先将已经存在的dst集合从db删除
        int deleted = dbDelete(c->db, dstkey);

        //新的dst集合不为空，则添加到db
        if (setTypeSize(dstset) > 0) {
            dbAdd(c->db, dstkey, dstset);
            addReplyLongLong(c, setTypeSize(dstset));
            notifyKeyspaceEvent(NOTIFY_SET, "sinterstore", dstkey, c->db->id);
        }
        else {
            //否则删除集合对象
            decrRefCount(dstset);
            addReply(c, shared.czero);
            if (deleted) {
                notifyKeyspaceEvent(NOTIFY_GENERIC, "del",
                                    dstkey, c->db->id);
            }
        }
        signalModifiedKey(c->db, dstkey);
        server.dirty++;
    }
    else {
        //向占位节点写入数组长度
        setDeferredMultiBulkLength(c, replylen, cardinality);
    }

    //释放集合对象数组
    zfree(sets);
}

//命令实现
//SINTER key [key ...]
void sinterCommand(client *c) {
    sinterGenericCommand(c, c->argv + 1, c->argc - 1, NULL);
}

//命令实现
//SINTERSTORE destination key [key ...]
void sinterstoreCommand(client *c) {
    sinterGenericCommand(c, c->argv + 2, c->argc - 2, c->argv[1]);
}

#define SET_OP_UNION 0
#define SET_OP_DIFF 1
#define SET_OP_INTER 2

//union/diff命令的底层实现
//SUNIONSTORE destination key [key ...]
//SDIFFSTORE destination key [key ...]
void sunionDiffGenericCommand(client *c, robj **setkeys, int setnum, robj *dstkey, int op) {
    //集合对象数组
    robj **sets = zmalloc(sizeof(robj *) * setnum);

    setTypeIterator *si;
    robj *ele, *dstset = NULL;
    int j, cardinality = 0;
    int diff_algo = 1;

    //遍历检查key是否存在，类型是否正确
    for (j = 0; j < setnum; j++) {
        robj *setobj = dstkey ? lookupKeyWrite(c->db, setkeys[j]) : lookupKeyRead(c->db, setkeys[j]);

        //某个集合不存在不会影响其他集合
        if (!setobj) {
            sets[j] = NULL;
            continue;
        }

        //类型检查
        if (checkType(c, setobj, OBJ_SET)) {
            zfree(sets);
            return;
        }

        sets[j] = setobj;
    }

    /* Select what DIFF algorithm to use.
     *
     * Algorithm 1 is O(N*M) where N is the size of the element first set
     * and M the total number of sets.
     *
     * Algorithm 2 is O(N) where N is the total number of elements in all
     * the sets.
     *
     * We compute what is the best bet with the current input here. */
    // diff可使用两种算法
    // 算法1遍历sets[0], 移除存在于其他任意集合的元素
    // 时间复杂度O(NM)，N为sets[0]元素个数，M为其他集合个数
    // 算法2遍历其他集合的所有元素，如果元素在sets[0]中，则从sets[0]移除
    // 时间复杂度O(K)，K为其他集合的元素个数
    // 查找时间复杂度近似认为是O(1)，虽然intset二分查找是logn

    //计算两种算法的工作量
    if (op == SET_OP_DIFF && sets[0]) {
        long long algo_one_work = 0, algo_two_work = 0;

        for (j = 0; j < setnum; j++) {
            //跳过不存在集合
            if (sets[j] == NULL) {
                continue;
            }
            algo_one_work += setTypeSize(sets[0]);
            algo_two_work += setTypeSize(sets[j]);
        }

        /* Algorithm 1 has better constant times and performs less operations
         * if there are elements in common. Give it some advantage. */
        //算法一对于每个sets[0]元素，平均遍历一半的其他集合即可
        algo_one_work /= 2;
        //使用哪个算法？
        diff_algo = (algo_one_work <= algo_two_work) ? 1 : 2;

        //如果使用算法1，对其他集合，按照集合大小从大到小排序
        //大集合优先查找，可能更快的找到存在的元素
        if (diff_algo == 1 && setnum > 1) {
            /* With algorithm 1 it is better to order the sets to subtract
             * by decreasing size, so that we are more likely to find
             * duplicated elements ASAP. */
            qsort(sets + 1, setnum - 1, sizeof(robj *),
                  qsortCompareSetsByRevCardinality);
        }
    }

    /* We need a temp set object to store our union. If the dstkey
     * is not NULL (that is, we are inside an SUNIONSTORE operation) then
     * this set object will be the resulting object to set into the target key*/
    //用作临时集合/dst集合
    dstset = createIntsetObject();

    //并集操作
    if (op == SET_OP_UNION) {
        /* Union is trivial, just add every element of every set to the
         * temporary set. */
        //遍历集合，将元素添加到dst集合即可
        for (j = 0; j < setnum; j++) {
            //跳过不存在的集合
            if (!sets[j]) {
                continue;
            } /* non existing keys are like empty sets */

            si = setTypeInitIterator(sets[j]);
            while ((ele = setTypeNextObject(si)) != NULL) {
                if (setTypeAdd(dstset, ele)) {
                    cardinality++;
                }
                decrRefCount(ele);
            }
            setTypeReleaseIterator(si);
        }
        //使用算法1的diff操作
    }
    else if (op == SET_OP_DIFF && sets[0] && diff_algo == 1) {
        /* DIFF Algorithm 1:
         *
         * We perform the diff by iterating all the elements of the first set,
         * and only adding it to the target set if the element does not exist
         * into all the other sets.
         *
         * This way we perform at max N*M operations, where N is the size of
         * the first set, and M the number of sets. */
        si = setTypeInitIterator(sets[0]);
        while ((ele = setTypeNextObject(si)) != NULL) {
            for (j = 1; j < setnum; j++) {
                //跳过不存在的集合
                if (!sets[j]) {
                    continue;
                } /* no key is an empty set. */

                //跳过同一个集合
                if (sets[j] == sets[0]) {
                    break;
                } /* same set! */

                //当前元素是否存在与该集合？
                if (setTypeIsMember(sets[j], ele)) {
                    break;
                }
            }

            //当前元素不存在于其他任意集合
            //添加到dst集合
            if (j == setnum) {
                /* There is no other set with this element. Add it. */
                setTypeAdd(dstset, ele);
                cardinality++;
            }
            decrRefCount(ele);
        }
        //释放迭代器
        setTypeReleaseIterator(si);

        //采用算法2的diff操作
    }
    else if (op == SET_OP_DIFF && sets[0] && diff_algo == 2) {
        /* DIFF Algorithm 2:
         *
         * Add all the elements of the first set to the auxiliary set.
         * Then remove all the elements of all the next sets from it.
         *
         * This is O(N) where N is the sum of all the elements in every
         * set. */
        for (j = 0; j < setnum; j++) {
            //跳过不存在的集合
            if (!sets[j]) {
                continue;
            } /* non existing keys are like empty sets */

            //当前集合迭代器
            si = setTypeInitIterator(sets[j]);
            while ((ele = setTypeNextObject(si)) != NULL) {
                if (j == 0) {
                    //首先将sets[0]的所有元素添加到dst集合
                    //同时记下其元素个数
                    if (setTypeAdd(dstset, ele)) {
                        cardinality++;
                    }
                }
                else {
                    //如果元素存在于sets[0]，删除该元素
                    if (setTypeRemove(dstset, ele)) {
                        cardinality--;
                    }
                }
                decrRefCount(ele);
            }
            //释放迭代器
            setTypeReleaseIterator(si);

            /* Exit if result set is empty as any additional removal
             * of elements will have no effect. */
            //cardinality等于0说明sets[0]的元素已经全部删除了,没必要再遍历其他集合了
            if (cardinality == 0) {
                break;
            }
        }
    }

    /* Output the content of the resulting set, if not in STORE mode */
    if (!dstkey) {
        //添加差集到回复缓冲，以bulk数组的形式
        addReplyMultiBulkLen(c, cardinality);
        //迭代dst集合，添加其中的元素到回复缓冲
        //todo：为什么不采用addDeferredMultiBulkLength？
        si = setTypeInitIterator(dstset);
        while ((ele = setTypeNextObject(si)) != NULL) {
            addReplyBulk(c, ele);
            decrRefCount(ele);
        }
        setTypeReleaseIterator(si);
        decrRefCount(dstset);
    }
    else {
        /* If we have a target key where to store the resulting set
         * create this key with the result set inside */
        //否则将dst集合添加到db
        //如果dst集合已经存在，需要先从db删除
        int deleted = dbDelete(c->db, dstkey);
        if (setTypeSize(dstset) > 0) {
            dbAdd(c->db, dstkey, dstset);
            addReplyLongLong(c, setTypeSize(dstset));
            notifyKeyspaceEvent(NOTIFY_SET,
                                op == SET_OP_UNION ? "sunionstore" : "sdiffstore",
                                dstkey, c->db->id);
        }
        else {
            //dst集合为空，即交集为空
            decrRefCount(dstset);
            addReply(c, shared.czero);
            if (deleted) {
                notifyKeyspaceEvent(NOTIFY_GENERIC, "del",
                                    dstkey, c->db->id);
            }
        }
        signalModifiedKey(c->db, dstkey);
        server.dirty++;
    }

    //释放集合对象数组
    zfree(sets);
}

//命令实现
//SUNION key [key ...]
void sunionCommand(client *c) {
    sunionDiffGenericCommand(c, c->argv + 1, c->argc - 1, NULL, SET_OP_UNION);
}

//命令实现
//SUNIONSTORE destination key [key ...]
void sunionstoreCommand(client *c) {
    sunionDiffGenericCommand(c, c->argv + 2, c->argc - 2, c->argv[1], SET_OP_UNION);
}

//命令实现
//SDIFF key [key ...]
void sdiffCommand(client *c) {
    sunionDiffGenericCommand(c, c->argv + 1, c->argc - 1, NULL, SET_OP_DIFF);
}

//命令实现
//SDIFFSTORE destination key [key ...]
void sdiffstoreCommand(client *c) {
    sunionDiffGenericCommand(c, c->argv + 2, c->argc - 2, c->argv[1], SET_OP_DIFF);
}

//todo
void sscanCommand(client *c) {
    robj *set;
    unsigned long cursor;

    if (parseScanCursorOrReply(c, c->argv[2], &cursor) == C_ERR) { return; }
    if ((set = lookupKeyReadOrReply(c, c->argv[1], shared.emptyscan)) == NULL ||
        checkType(c, set, OBJ_SET)) {
        return;
    }
    scanGenericCommand(c, set, cursor);
}
