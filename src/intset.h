/*
 * Copyright (c) 2009-2012, Pieter Noordhuis <pcnoordhuis at gmail dot com>
 * Copyright (c) 2009-2012, Salvatore Sanfilippo <antirez at gmail dot com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Redis nor the names of its contributors may be used
 *     to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __INTSET_H
#define __INTSET_H

#include <stdint.h>

//整数集合
typedef struct intset {
    //集合编码类型
    uint32_t encoding;
    //元素个数
    uint32_t length;
    //元素数组，是一个升序数组
    int8_t contents[];
} intset;

//创建新的整数集合
intset *intsetNew(void);

//往集合中添加新元素
//每次添加都需要realloc cur_size + 1的空间，不会预分配更大的空间
intset *intsetAdd(intset *is, int64_t value, uint8_t *success);

//从集合中删除value
//每次删除都要realloc，不会保留多余的空闲空间
intset *intsetRemove(intset *is, int64_t value, int *success);

//检查元素是否存在于集合中
uint8_t intsetFind(intset *is, int64_t value);

//随机返回一个元素
int64_t intsetRandom(intset *is);

//获取索引pos处的值，写入value
uint8_t intsetGet(intset *is, uint32_t pos, int64_t *value);

//返回集合元素个数
uint32_t intsetLen(intset *is);

//返回集合使用内存大小
//等于集合结构体大小+所有元素大小
size_t intsetBlobLen(intset *is);

#ifdef REDIS_TEST
int intsetTest(int argc, char *argv[]);
#endif

#endif // __INTSET_H
