/*
 * Copyright (c) 2009-2012, Salvatore Sanfilippo <antirez at gmail dot com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Redis nor the names of its contributors may be used
 *     to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "server.h"

/*-----------------------------------------------------------------------------
 * Pubsub low level API
 *----------------------------------------------------------------------------*/

//释放模式
void freePubsubPattern(void *p) {
    pubsubPattern *pat = p;

    decrRefCount(pat->pattern);
    zfree(pat);
}

//用于模式链表的比较函数
int listMatchPubsubPattern(void *a, void *b) {
    pubsubPattern *pa = a, *pb = b;

    return (pa->client == pb->client) &&
           (equalStringObjects(pa->pattern, pb->pattern));
}

/* Return the number of channels + patterns a client is subscribed to. */
//客户端订阅的频道数量和模式数量
int clientSubscriptionsCount(client *c) {
    return dictSize(c->pubsub_channels) +
           listLength(c->pubsub_patterns);
}

/* Subscribe a client to a channel. Returns 1 if the operation succeeded, or
 * 0 if the client was already subscribed to that channel. */
//订阅某个频道
int pubsubSubscribeChannel(client *c, robj *channel) {
    dictEntry *de;
    list *clients = NULL;
    int retval = 0;

    /* Add the channel to the client -> channels hash table */
    //将频道加入当前客户端已订阅频道集合
    if (dictAdd(c->pubsub_channels, channel, NULL) == DICT_OK) {
        retval = 1;
        //pubsub_channels字典没有key复制函数，故需要增加channel引用计数
        incrRefCount(channel);

        /* Add the client to the channel -> list of clients hash table */
        //将客户端添加到全局订阅字典pubsub_channels对应频道的链表
        de = dictFind(server.pubsub_channels, channel);
        //还没有其他客户端订阅该频道，则要先将该频道添加到全局订阅字典pubsub_channels
        if (de == NULL) {
            clients = listCreate();
            //将该频道添加到pubsub_channels
            dictAdd(server.pubsub_channels, channel, clients);
            incrRefCount(channel);
        }
        else {
            //否则获取该链表
            clients = dictGetVal(de);
        }

        //添加客户端到该频道链表
        listAddNodeTail(clients, c);
    }

    /* Notify the client */
    addReply(c, shared.mbulkhdr[3]);
    addReply(c, shared.subscribebulk);
    addReplyBulk(c, channel);
    addReplyLongLong(c, clientSubscriptionsCount(c));
    return retval;
}

/* Unsubscribe a client from a channel. Returns 1 if the operation succeeded, or
 * 0 if the client was not subscribed to the specified channel. */
//退订频道
int pubsubUnsubscribeChannel(client *c, robj *channel, int notify) {
    dictEntry *de;
    list *clients;
    listNode *ln;
    int retval = 0;

    /* Remove the channel from the client -> channels hash table */
    //防止client/server.pubsub_channels的删除操作过早释放了channel
    incrRefCount(channel); /* channel may be just a pointer to the same object
                            we have in the hash tables. Protect it... */

    //将频道从客户端已订阅集合删除
    if (dictDelete(c->pubsub_channels, channel) == DICT_OK) {
        retval = 1;
        /* Remove the client from the channel -> clients list hash table */

        //从全局订阅字典pubsub_channels获取订阅该频道的客户端链表
        de = dictFind(server.pubsub_channels, channel);
        serverAssertWithInfo(c, NULL, de != NULL);
        clients = dictGetVal(de);

        //搜索该链表并将当前客户端从链表中删除
        ln = listSearchKey(clients, c);
        serverAssertWithInfo(c, NULL, ln != NULL);
        listDelNode(clients, ln);

        //当前客户端是该频道唯一订阅者，从pubsub_channels删除该频道
        if (listLength(clients) == 0) {
            /* Free the list and associated hash entry at all if this was
             * the latest client, so that it will be possible to abuse
             * Redis PUBSUB creating millions of channels. */
            dictDelete(server.pubsub_channels, channel);
        }
    }

    /* Notify the client */
    if (notify) {
        addReply(c, shared.mbulkhdr[3]);
        addReply(c, shared.unsubscribebulk);
        addReplyBulk(c, channel);
        addReplyLongLong(c, dictSize(c->pubsub_channels) +
                            listLength(c->pubsub_patterns));

    }

    //到这里再释放channel
    decrRefCount(channel); /* it is finally safe to release it */

    return retval;
}

/* Subscribe a client to a pattern. Returns 1 if the operation succeeded,
 * or 0 if the client was already subscribed to that pattern. */
//订阅模式
int pubsubSubscribePattern(client *c, robj *pattern) {
    int retval = 0;

    //检查是否已经订阅了该模式
    if (listSearchKey(c->pubsub_patterns, pattern) == NULL) {
        retval = 1;
        pubsubPattern *pat;

        //将模式加入当前客户端已订阅模式链表
        listAddNodeTail(c->pubsub_patterns, pattern);
        incrRefCount(pattern);

        //将(客户端，模式)添加到全局模式链表
        pat = zmalloc(sizeof(*pat));
        pat->pattern = getDecodedObject(pattern);
        pat->client = c;
        listAddNodeTail(server.pubsub_patterns, pat);
    }

    /* Notify the client */
    addReply(c, shared.mbulkhdr[3]);
    addReply(c, shared.psubscribebulk);
    addReplyBulk(c, pattern);
    addReplyLongLong(c, clientSubscriptionsCount(c));
    return retval;
}

/* Unsubscribe a client from a channel. Returns 1 if the operation succeeded, or
 * 0 if the client was not subscribed to the specified channel. */
//退订模式
int pubsubUnsubscribePattern(client *c, robj *pattern, int notify) {
    listNode *ln;
    pubsubPattern pat;
    int retval = 0;

    //防止pattern被过早释放
    incrRefCount(pattern); /* Protect the object. May be the same we remove */

    if ((ln = listSearchKey(c->pubsub_patterns, pattern)) != NULL) {
        retval = 1;
        //将模式从客户端已订阅模式链表删除
        listDelNode(c->pubsub_patterns, ln);

        //将客户端从全局模式链表删除
        pat.client = c;
        pat.pattern = pattern;
        ln = listSearchKey(server.pubsub_patterns, &pat);
        listDelNode(server.pubsub_patterns, ln);
    }

    /* Notify the client */
    if (notify) {
        addReply(c, shared.mbulkhdr[3]);
        addReply(c, shared.punsubscribebulk);
        addReplyBulk(c, pattern);
        addReplyLongLong(c, dictSize(c->pubsub_channels) +
                            listLength(c->pubsub_patterns));
    }

    //释放pattern
    decrRefCount(pattern);
    return retval;
}

/* Unsubscribe from all the channels. Return the number of channels the
 * client was subscribed to. */
//退订客户端订阅的所有频道
int pubsubUnsubscribeAllChannels(client *c, int notify) {
    dictIterator *di = dictGetSafeIterator(c->pubsub_channels);
    dictEntry *de;
    int count = 0;

    //遍历客户端已订阅频道集合，退订频道
    while ((de = dictNext(di)) != NULL) {
        robj *channel = dictGetKey(de);
        count += pubsubUnsubscribeChannel(c, channel, notify);
    }

    /* We were subscribed to nothing? Still reply to the client. */
    if (notify && count == 0) {
        addReply(c, shared.mbulkhdr[3]);
        addReply(c, shared.unsubscribebulk);
        addReply(c, shared.nullbulk);
        addReplyLongLong(c, dictSize(c->pubsub_channels) +
                            listLength(c->pubsub_patterns));
    }
    dictReleaseIterator(di);
    return count;
}

/* Unsubscribe from all the patterns. Return the number of patterns the
 * client was subscribed from. */
//退订客户端订阅的所有模式
int pubsubUnsubscribeAllPatterns(client *c, int notify) {
    listNode *ln;
    listIter li;
    int count = 0;

    //遍历客户端已订阅模式链表，退订模式
    listRewind(c->pubsub_patterns, &li);
    while ((ln = listNext(&li)) != NULL) {
        robj *pattern = ln->value;
        count += pubsubUnsubscribePattern(c, pattern, notify);
    }

    if (notify && count == 0) {
        /* We were subscribed to nothing? Still reply to the client. */
        addReply(c, shared.mbulkhdr[3]);
        addReply(c, shared.punsubscribebulk);
        addReply(c, shared.nullbulk);
        addReplyLongLong(c, dictSize(c->pubsub_channels) +
                            listLength(c->pubsub_patterns));
    }

    return count;
}

/* Publish a message */
//发布消息到频道
int pubsubPublishMessage(robj *channel, robj *message) {
    int receivers = 0;
    dictEntry *de;
    listNode *ln;
    listIter li;

    /* Send to clients listening for that channel */
    //发送消息给订阅该频道的客户端
    de = dictFind(server.pubsub_channels, channel);
    if (de) {
        //获取订阅该频道的客户端链表
        list *list = dictGetVal(de);
        listNode *ln;
        listIter li;

        //迭代客户端链表，添加message到这些客户端的回复缓冲
        listRewind(list, &li);
        while ((ln = listNext(&li)) != NULL) {
            client *c = ln->value;
            addReply(c, shared.mbulkhdr[3]);
            addReply(c, shared.messagebulk);
            addReplyBulk(c, channel);
            addReplyBulk(c, message);
            receivers++;
        }
    }

    /* Send to clients listening to matching channels */
    //发送消息给订阅了匹配该频道的模式的客户端
    if (listLength(server.pubsub_patterns)) {
        listRewind(server.pubsub_patterns, &li);
        channel = getDecodedObject(channel);

        //遍历全局模式订阅链表
        while ((ln = listNext(&li)) != NULL) {
            pubsubPattern *pat = ln->value;

            //频道和模式是否匹配?
            if (stringmatchlen((char *) pat->pattern->ptr, sdslen(pat->pattern->ptr), (char *) channel->ptr,
                               sdslen(channel->ptr), 0)) {
                addReply(pat->client, shared.mbulkhdr[4]);
                addReply(pat->client, shared.pmessagebulk);
                addReplyBulk(pat->client, pat->pattern);
                addReplyBulk(pat->client, channel);
                addReplyBulk(pat->client, message);
                receivers++;
            }
        }
        decrRefCount(channel);
    }
    return receivers;
}

/*-----------------------------------------------------------------------------
 * Pubsub commands implementation
 *----------------------------------------------------------------------------*/

//命令实现
//SUBSCRIBE channel [channel ...]
void subscribeCommand(client *c) {
    int j;

    //遍历参数，订阅频道
    for (j = 1; j < c->argc; j++)
        pubsubSubscribeChannel(c, c->argv[j]);

    //更新客户端状态为订阅状态
    c->flags |= CLIENT_PUBSUB;
}

//命令实现
//UNSUBSCRIBE [channel [channel ...]]
void unsubscribeCommand(client *c) {
    //没有channel参数则退订所有频道
    if (c->argc == 1) {
        pubsubUnsubscribeAllChannels(c, 1);
    }
    else {
        int j;
        //遍历channel，退订频道
        for (j = 1; j < c->argc; j++)
            pubsubUnsubscribeChannel(c, c->argv[j], 1);
    }

    //当前客户端没有订阅任何频道/模式了，更新客户端状态
    if (clientSubscriptionsCount(c) == 0) {
        c->flags &= ~CLIENT_PUBSUB;
    }
}

//命令实现
//PSUBSCRIBE pattern [pattern ...]
//h?llo subscribes to hello, hallo and hxllo
//h*llo subscribes to hllo and heeeello
//h[ae]llo subscribes to hello and hallo, but not hillo
void psubscribeCommand(client *c) {
    int j;
    for (j = 1; j < c->argc; j++)
        pubsubSubscribePattern(c, c->argv[j]);
    //更新客户端专题
    c->flags |= CLIENT_PUBSUB;
}

//命令实现
//PUNSUBSCRIBE [pattern [pattern ...]]
void punsubscribeCommand(client *c) {
    //没有pattern参数则退订所有模式
    if (c->argc == 1) {
        pubsubUnsubscribeAllPatterns(c, 1);
    }
    else {
        int j;

        for (j = 1; j < c->argc; j++)
            pubsubUnsubscribePattern(c, c->argv[j], 1);
    }

    //客户端没有订阅任何模式/频道了，更新客户端状态
    if (clientSubscriptionsCount(c) == 0) {
        c->flags &= ~CLIENT_PUBSUB;
    }
}

//命令实现
//PUBLISH channel message
void publishCommand(client *c) {
    int receivers = pubsubPublishMessage(c->argv[1], c->argv[2]);
    if (server.cluster_enabled) {
        clusterPropagatePublish(c->argv[1], c->argv[2]);
    }
    else {
        forceCommandPropagation(c, PROPAGATE_REPL);
    }
    addReplyLongLong(c, receivers);
}

/* PUBSUB command for Pub/Sub introspection. */
//命令实现
//PUBSUB subcommand [argument [argument ...]]
void pubsubCommand(client *c) {
    //返回所有被订阅的符合模式的频道
    if (!strcasecmp(c->argv[1]->ptr, "channels") && (c->argc == 2 || c->argc == 3)) {
        /* PUBSUB CHANNELS [<pattern>] */
        //是否有pattern参数？
        sds pat = (c->argc == 2) ? NULL : c->argv[2]->ptr;

        //当前已订阅频道的迭代器
        dictIterator *di = dictGetIterator(server.pubsub_channels);
        dictEntry *de;
        long mblen = 0;
        void *replylen;

        //添加占位节点，因为不知道有多少符合要求的频道要返回
        replylen = addDeferredMultiBulkLength(c);

        //遍历所有频道
        while ((de = dictNext(di)) != NULL) {
            //获取key，即频道
            robj *cobj = dictGetKey(de);
            sds channel = cobj->ptr;

            //是否匹配模式？
            if (!pat || stringmatchlen(pat, sdslen(pat), channel, sdslen(channel), 0)) {
                addReplyBulk(c, cobj);
                mblen++;
            }
        }

        //是否迭代器
        dictReleaseIterator(di);
        //将返回的频道个数写入占位节点
        setDeferredMultiBulkLength(c, replylen, mblen);
    }

        //返回订阅某个频道的客户端数量
    else if (!strcasecmp(c->argv[1]->ptr, "numsub") && c->argc >= 2) {
        /* PUBSUB NUMSUB [Channel_1 ... Channel_N] */
        int j;

        //添加返回的bulk数组长度到回复缓冲
        //*2因为对于每个频道，返回(频道名字，订阅数量)的二元组
        addReplyMultiBulkLen(c, (c->argc - 2) * 2);

        for (j = 2; j < c->argc; j++) {
            //订阅当前频道的客户端链表
            list *l = dictFetchValue(server.pubsub_channels, c->argv[j]);
            //添加当前频道到回复缓冲
            addReplyBulk(c, c->argv[j]);
            //添加链表长度到回复缓冲
            addReplyLongLong(c, l ? listLength(l) : 0);
        }
    }

        //返回订阅任何模式的总客户端数量
    else if (!strcasecmp(c->argv[1]->ptr, "numpat") && c->argc == 2) {
        /* PUBSUB NUMPAT */
        //返回全局模式订阅链表的长度
        addReplyLongLong(c, listLength(server.pubsub_patterns));
    }
    else {
        //无法识别的命令
        addReplyErrorFormat(c,
                            "Unknown PUBSUB subcommand or wrong number of arguments for '%s'",
                            (char *) c->argv[1]->ptr);
    }
}
