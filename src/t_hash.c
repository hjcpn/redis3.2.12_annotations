/*
 * Copyright (c) 2009-2012, Salvatore Sanfilippo <antirez at gmail dot com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Redis nor the names of its contributors may be used
 *     to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

//用于hash的字典类型
//dictType hashDictType = {
//        dictEncObjHash,             /* hash function */
//        NULL,                       /* key dup */
//        NULL,                       /* val dup */
//        dictEncObjKeyCompare,       /* key compare */
//        dictObjectDestructor,  /* key destructor */
//        dictObjectDestructor   /* val destructor */
//};
//键值对都是共享的robj对象


#include "server.h"
#include <math.h>

/*-----------------------------------------------------------------------------
 * Hash type API
 *----------------------------------------------------------------------------*/

/* Check the length of a number of objects to see if we need to convert a
 * ziplist to a real hash. Note that we only check string encoded objects
 * as their string length can be queried in constant time. */
//遍历参数，检查每个sds长度是否超过hash_max_ziplist_value
//超过的话，将ziplist实现的hash转换为dict实现
void hashTypeTryConversion(robj *o, robj **argv, int start, int end) {
    int i;

    if (o->encoding != OBJ_ENCODING_ZIPLIST) { return; }

    for (i = start; i <= end; i++) {
        if (sdsEncodedObject(argv[i]) && sdslen(argv[i]->ptr) > server.hash_max_ziplist_value) {
            hashTypeConvert(o, OBJ_ENCODING_HT);
            break;
        }
    }
}

/* Encode given objects in-place when the hash uses a dict. */
//转换键值对的string编码类型，节省内存
void hashTypeTryObjectEncoding(robj *subject, robj **o1, robj **o2) {
    if (subject->encoding == OBJ_ENCODING_HT) {
        if (o1) {
            *o1 = tryObjectEncoding(*o1);
        }
        if (o2) {
            *o2 = tryObjectEncoding(*o2);
        }
    }
}

/* Get the value from a ziplist encoded hash, identified by field.
 * Returns -1 when the field cannot be found. */
//在ziplist中寻找field，将其数据写入vstr/vll
int hashTypeGetFromZiplist(robj *o, robj *field, unsigned char **vstr, unsigned int *vlen, long long *vll) {
    unsigned char *zl, *fptr = NULL, *vptr = NULL;
    int ret;

    serverAssert(o->encoding == OBJ_ENCODING_ZIPLIST);

    //为int编码的string对象创建一个embstr/raw编码的新对象
    field = getDecodedObject(field);

    zl = o->ptr;
    fptr = ziplistIndex(zl, ZIPLIST_HEAD);
    //在ziplist中寻找field
    if (fptr != NULL) {
        fptr = ziplistFind(fptr, field->ptr, sdslen(field->ptr), 1);
        if (fptr != NULL) {
            /* Grab pointer to the value (fptr points to the field) */
            //value在key的下一个节点
            vptr = ziplistNext(zl, fptr);
            serverAssert(vptr != NULL);
        }
    }

    decrRefCount(field);

    //获取value节点保存的数据，写入vstr/vll
    if (vptr != NULL) {
        ret = ziplistGet(vptr, vstr, vlen, vll);
        serverAssert(ret);
        return 0;
    }

    return -1;
}

/* Get the value from a hash table encoded hash, identified by field.
 * Returns -1 when the field cannot be found. */
//在dict中寻找field，将其value写入value
int hashTypeGetFromHashTable(robj *o, robj *field, robj **value) {
    dictEntry *de;

    serverAssert(o->encoding == OBJ_ENCODING_HT);

    de = dictFind(o->ptr, field);
    if (de == NULL) {
        return -1;
    }
    //获取entry的value，写入value
    *value = dictGetVal(de);
    return 0;
}

/* Higher level function of hashTypeGet*() that always returns a Redis
 * object (either new or with refcount incremented), so that the caller
 * can retain a reference or call decrRefCount after the usage.
 *
 * The lower level function can prevent copy on write so it is
 * the preferred way of doing read operations. */
//从hash中获取field对应的value，以对象形式返回
robj *hashTypeGetObject(robj *o, robj *field) {
    robj *value = NULL;

    //从ziplist获取field
    if (o->encoding == OBJ_ENCODING_ZIPLIST) {
        unsigned char *vstr = NULL;
        unsigned int vlen = UINT_MAX;
        long long vll = LLONG_MAX;

        //为vstr/vll创建robj对象
        if (hashTypeGetFromZiplist(o, field, &vstr, &vlen, &vll) == 0) {
            if (vstr) {
                value = createStringObject((char *) vstr, vlen);
            }
            else {
                value = createStringObjectFromLongLong(vll);
            }
        }

        //从dict获取field
    }
    else if (o->encoding == OBJ_ENCODING_HT) {
        robj *aux;

        if (hashTypeGetFromHashTable(o, field, &aux) == 0) {
            incrRefCount(aux);
            value = aux;
        }
    }
    else {
        serverPanic("Unknown hash encoding");
    }

    return value;
}

/* Higher level function using hashTypeGet*() to return the length of the
 * object associated with the requested field, or 0 if the field does not
 * exist. */
//获取field的value的长度
size_t hashTypeGetValueLength(robj *o, robj *field) {
    size_t len = 0;
    if (o->encoding == OBJ_ENCODING_ZIPLIST) {
        unsigned char *vstr = NULL;
        unsigned int vlen = UINT_MAX;
        long long vll = LLONG_MAX;

        if (hashTypeGetFromZiplist(o, field, &vstr, &vlen, &vll) == 0) {
            //整数的话计算有多少位来表示长度.
            len = vstr ? vlen : sdigits10(vll);
        }
    }
    else if (o->encoding == OBJ_ENCODING_HT) {
        robj *aux;

        if (hashTypeGetFromHashTable(o, field, &aux) == 0) {
            //string对象长度
            len = stringObjectLen(aux);
        }
    }
    else {
        serverPanic("Unknown hash encoding");
    }
    return len;
}

/* Test if the specified field exists in the given hash. Returns 1 if the field
 * exists, and 0 when it doesn't. */
//key是否已经存在？
int hashTypeExists(robj *o, robj *field) {
    //在ziplist中寻找field
    if (o->encoding == OBJ_ENCODING_ZIPLIST) {
        unsigned char *vstr = NULL;
        unsigned int vlen = UINT_MAX;
        long long vll = LLONG_MAX;

        if (hashTypeGetFromZiplist(o, field, &vstr, &vlen, &vll) == 0) { return 1; }

        //dictz中寻找field
    }
    else if (o->encoding == OBJ_ENCODING_HT) {
        robj *aux;

        if (hashTypeGetFromHashTable(o, field, &aux) == 0) { return 1; }
    }
    else {
        serverPanic("Unknown hash encoding");
    }

    return 0;
}

/* Add an element, discard the old if the key already exists.
 * Return 0 on insert and 1 on update.
 * This function will take care of incrementing the reference count of the
 * retained fields and value objects. */
//添加或更新键值对
int hashTypeSet(robj *o, robj *field, robj *value) {
    int update = 0;

    //添加到ziplist
    if (o->encoding == OBJ_ENCODING_ZIPLIST) {
        unsigned char *zl, *fptr, *vptr;

        //为int编码的string对象创建一个embstr/raw编码的新对象
        field = getDecodedObject(field);
        value = getDecodedObject(value);

        zl = o->ptr;
        fptr = ziplistIndex(zl, ZIPLIST_HEAD);
        if (fptr != NULL) {
            //key是否存在？
            fptr = ziplistFind(fptr, field->ptr, sdslen(field->ptr), 1);
            if (fptr != NULL) {
                /* Grab pointer to the value (fptr points to the field) */
                //value是key的下一个节点
                vptr = ziplistNext(zl, fptr);
                serverAssert(vptr != NULL);
                update = 1;

                /* Delete value */
                //删除原来的value
                //ziplistDelete会更新vptr指针指向被删除节点的下一个节点
                zl = ziplistDelete(zl, &vptr);

                /* Insert new value */
                //插入新的value
                zl = ziplistInsert(zl, vptr, value->ptr, sdslen(value->ptr));
            }
        }

        //key不存在
        if (!update) {
            /* Push new field/value pair onto the tail of the ziplist */
            //插入新的键值对到ziplist尾部
            zl = ziplistPush(zl, field->ptr, sdslen(field->ptr), ZIPLIST_TAIL);
            zl = ziplistPush(zl, value->ptr, sdslen(value->ptr), ZIPLIST_TAIL);
        }

        o->ptr = zl;

        //ziplist插入的是sds，故减少对象引用计数
        decrRefCount(field);
        decrRefCount(value);

        /* Check if the ziplist needs to be converted to a hash table */
        //元素数量是否超过hash_max_ziplist_entries，需要转换成dict实现？
        if (hashTypeLength(o) > server.hash_max_ziplist_entries) {
            hashTypeConvert(o, OBJ_ENCODING_HT);
        }

        //添加到dict
    }
    else if (o->encoding == OBJ_ENCODING_HT) {
        ////添加或更新键值对，增加filed/value的引用计数
        if (dictReplace(o->ptr, field, value)) { /* Insert */
            incrRefCount(field);
        }
        else { /* Update */
            update = 1;
        }
        incrRefCount(value);
    }
    else {
        serverPanic("Unknown hash encoding");
    }

    //更新还是添加？
    return update;
}

/* Delete an element from a hash.
 * Return 1 on deleted and 0 on not found. */
//从hash删除field
int hashTypeDelete(robj *o, robj *field) {
    int deleted = 0;

    if (o->encoding == OBJ_ENCODING_ZIPLIST) {
        unsigned char *zl, *fptr;

        //为int编码的string对象创建一个embstr/raw编码的新对象
        field = getDecodedObject(field);

        zl = o->ptr;
        fptr = ziplistIndex(zl, ZIPLIST_HEAD);
        if (fptr != NULL) {
            //寻找field
            fptr = ziplistFind(fptr, field->ptr, sdslen(field->ptr), 1);
            if (fptr != NULL) {
                //删除键值对
                //删除field后ziplistDelete函数会更新fptr指向下一个节点，即value
                zl = ziplistDelete(zl, &fptr);
                zl = ziplistDelete(zl, &fptr);
                o->ptr = zl;
                deleted = 1;
            }
        }

        decrRefCount(field);

    }
    else if (o->encoding == OBJ_ENCODING_HT) {
        if (dictDelete((dict *) o->ptr, field) == C_OK) {
            deleted = 1;

            /* Always check if the dictionary needs a resize after a delete. */
            //检查是否需要缩小字典
            if (htNeedsResize(o->ptr)) {
                dictResize(o->ptr);
            }
        }

    }
    else {
        serverPanic("Unknown hash encoding");
    }

    return deleted;
}

/* Return the number of elements in a hash. */
//hash键值对的个数
unsigned long hashTypeLength(robj *o) {
    unsigned long length = ULONG_MAX;

    if (o->encoding == OBJ_ENCODING_ZIPLIST) {
        //布局：field value field value
        //故需要/2
        length = ziplistLen(o->ptr) / 2;
    }
    else if (o->encoding == OBJ_ENCODING_HT) {
        length = dictSize((dict *) o->ptr);
    }
    else {
        serverPanic("Unknown hash encoding");
    }

    return length;
}

//创建hash对象的迭代器
hashTypeIterator *hashTypeInitIterator(robj *subject) {
    hashTypeIterator *hi = zmalloc(sizeof(hashTypeIterator));
    hi->subject = subject;
    hi->encoding = subject->encoding;

    if (hi->encoding == OBJ_ENCODING_ZIPLIST) {
        hi->fptr = NULL;
        hi->vptr = NULL;
    }
    else if (hi->encoding == OBJ_ENCODING_HT) {
        hi->di = dictGetIterator(subject->ptr);
    }
    else {
        serverPanic("Unknown hash encoding");
    }

    return hi;
}

//释放迭代器
void hashTypeReleaseIterator(hashTypeIterator *hi) {
    //dict迭代器需要单独释放
    if (hi->encoding == OBJ_ENCODING_HT) {
        dictReleaseIterator(hi->di);
    }

    zfree(hi);
}

/* Move to the next entry in the hash. Return C_OK when the next entry
 * could be found and C_ERR when the iterator reaches the end. */
//返回迭代器的下一个元素
int hashTypeNext(hashTypeIterator *hi) {
    if (hi->encoding == OBJ_ENCODING_ZIPLIST) {
        unsigned char *zl;
        unsigned char *fptr, *vptr;

        zl = hi->subject->ptr;
        fptr = hi->fptr;
        vptr = hi->vptr;

        //第一次迭代
        if (fptr == NULL) {
            /* Initialize cursor */
            serverAssert(vptr == NULL);
            //指向第一个entry
            fptr = ziplistIndex(zl, 0);
        }
        else {
            /* Advance cursor */
            //不是第一次迭代，指向vptr的下一个节点，即下一对键值对
            serverAssert(vptr != NULL);
            fptr = ziplistNext(zl, vptr);
        }

        //没有下一个节点了
        if (fptr == NULL) {
            return C_ERR;
        }

        /* Grab pointer to the value (fptr points to the field) */
        //指向fptr的下一个节点，即field对应的value
        vptr = ziplistNext(zl, fptr);
        serverAssert(vptr != NULL);

        /* fptr, vptr now point to the first or next pair */
        hi->fptr = fptr;
        hi->vptr = vptr;
    }
    else if (hi->encoding == OBJ_ENCODING_HT) {
        //dict迭代器的下一个元素
        if ((hi->de = dictNext(hi->di)) == NULL) {
            return C_ERR;
        }
    }
    else {
        serverPanic("Unknown hash encoding");
    }
    return C_OK;
}

/* Get the field or value at iterator cursor, for an iterator on a hash value
 * encoded as a ziplist. Prototype is similar to `hashTypeGetFromZiplist`. */
//将迭代器当前迭代的键值对写入vstr/vll
//what表示写入键/值
void hashTypeCurrentFromZiplist(hashTypeIterator *hi, int what,
                                unsigned char **vstr,
                                unsigned int *vlen,
                                long long *vll) {
    int ret;

    serverAssert(hi->encoding == OBJ_ENCODING_ZIPLIST);

    if (what & OBJ_HASH_KEY) {
        ret = ziplistGet(hi->fptr, vstr, vlen, vll);
        serverAssert(ret);
    }
    else {
        ret = ziplistGet(hi->vptr, vstr, vlen, vll);
        serverAssert(ret);
    }
}

/* Get the field or value at iterator cursor, for an iterator on a hash value
 * encoded as a ziplist. Prototype is similar to `hashTypeGetFromHashTable`. */
//将迭代器当前迭代到的键值对写入dst
void hashTypeCurrentFromHashTable(hashTypeIterator *hi, int what, robj **dst) {
    serverAssert(hi->encoding == OBJ_ENCODING_HT);

    if (what & OBJ_HASH_KEY) {
        *dst = dictGetKey(hi->de);
    }
    else {
        *dst = dictGetVal(hi->de);
    }
}

/* A non copy-on-write friendly but higher level version of hashTypeCurrent*()
 * that returns an object with incremented refcount (or a new object). It is up
 * to the caller to decrRefCount() the object if no reference is retained. */
//返回迭代器当前迭代到的键/值，以对象的形式
robj *hashTypeCurrentObject(hashTypeIterator *hi, int what) {
    robj *dst;

    if (hi->encoding == OBJ_ENCODING_ZIPLIST) {
        unsigned char *vstr = NULL;
        unsigned int vlen = UINT_MAX;
        long long vll = LLONG_MAX;

        //为vstr/vll创建对象
        hashTypeCurrentFromZiplist(hi, what, &vstr, &vlen, &vll);
        if (vstr) {
            dst = createStringObject((char *) vstr, vlen);
        }
        else {
            dst = createStringObjectFromLongLong(vll);
        }
    }
    else if (hi->encoding == OBJ_ENCODING_HT) {
        hashTypeCurrentFromHashTable(hi, what, &dst);
        //已经是对象了，只需要增加引用计数
        incrRefCount(dst);
    }
    else {
        serverPanic("Unknown hash encoding");
    }

    return dst;
}

//寻找key，若不存在创建并添加到db
robj *hashTypeLookupWriteOrCreate(client *c, robj *key) {
    robj *o = lookupKeyWrite(c->db, key);
    if (o == NULL) {
        o = createHashObject();
        dbAdd(c->db, key, o);
    }
    else {
        //类型检查
        if (o->type != OBJ_HASH) {
            addReply(c, shared.wrongtypeerr);
            return NULL;
        }
    }
    return o;
}

//将ziplist实现的hash转换成dict实现的hash
void hashTypeConvertZiplist(robj *o, int enc) {
    serverAssert(o->encoding == OBJ_ENCODING_ZIPLIST);

    if (enc == OBJ_ENCODING_ZIPLIST) {
        /* Nothing to do... */

    }
    else if (enc == OBJ_ENCODING_HT) {
        hashTypeIterator *hi;
        dict *dict;
        int ret;

        //创建hash对象化迭代器
        hi = hashTypeInitIterator(o);

        //创建hash对象ptr指向的dict
        dict = dictCreate(&hashDictType, NULL);

        //迭代hash对象，添加键值对
        while (hashTypeNext(hi) != C_ERR) {
            robj *field, *value;

            //获取field对象
            field = hashTypeCurrentObject(hi, OBJ_HASH_KEY);
            //尝试将string对象编码类型由raw转换为embstr或int，节省内存
            field = tryObjectEncoding(field);

            //获取value对象
            value = hashTypeCurrentObject(hi, OBJ_HASH_VALUE);
            //尝试将string对象编码类型由raw转换为embstr或int，节省内存
            value = tryObjectEncoding(value);

            //添加键值对到dict
            ret = dictAdd(dict, field, value);

            if (ret != DICT_OK) {
                serverLogHexDump(LL_WARNING, "ziplist with dup elements dump",
                                 o->ptr, ziplistBlobLen(o->ptr));
                serverAssert(ret == DICT_OK);
            }
        }

        //释放迭代器
        hashTypeReleaseIterator(hi);
        //释放原来的ziplist
        zfree(o->ptr);
        //修改实现方式
        o->encoding = OBJ_ENCODING_HT;
        //指向dict
        o->ptr = dict;

    }
    else {
        serverPanic("Unknown hash encoding");
    }
}

//将ziplist实现的hash转换成dict实现的hash
void hashTypeConvert(robj *o, int enc) {
    if (o->encoding == OBJ_ENCODING_ZIPLIST) {
        hashTypeConvertZiplist(o, enc);
    }
    else if (o->encoding == OBJ_ENCODING_HT) {
        serverPanic("Not implemented");
    }
    else {
        serverPanic("Unknown hash encoding");
    }
}

/*-----------------------------------------------------------------------------
 * Hash type commands
 *----------------------------------------------------------------------------*/

//命令实现
//HSET key field value
void hsetCommand(client *c) {
    int update;
    robj *o;

    //寻找key并检查类型，若不存在创建并添加到db
    if ((o = hashTypeLookupWriteOrCreate(c, c->argv[1])) == NULL) {
        return;
    }

    //是否需要转换hash底层实现？
    hashTypeTryConversion(o, c->argv, 2, 3);
    //转换键值对的string编码类型，节省内存
    hashTypeTryObjectEncoding(o, &c->argv[2], &c->argv[3]);

    //添加或更新键值对
    update = hashTypeSet(o, c->argv[2], c->argv[3]);

    addReply(c, update ? shared.czero : shared.cone);
    signalModifiedKey(c->db, c->argv[1]);
    notifyKeyspaceEvent(NOTIFY_HASH, "hset", c->argv[1], c->db->id);
    server.dirty++;
}

//命令实现
//HSETNX key field value
void hsetnxCommand(client *c) {
    robj *o;
    //寻找key，若不存在创建并添加到db
    if ((o = hashTypeLookupWriteOrCreate(c, c->argv[1])) == NULL) {
        return;
    }

    //是否需要转换hash底层实现？
    hashTypeTryConversion(o, c->argv, 2, 3);

    //key已经存在
    if (hashTypeExists(o, c->argv[2])) {
        addReply(c, shared.czero);
    }
    else {
        //转换键值对的string编码类型，节省内存
        hashTypeTryObjectEncoding(o, &c->argv[2], &c->argv[3]);
        //添加键值对
        hashTypeSet(o, c->argv[2], c->argv[3]);

        addReply(c, shared.cone);
        signalModifiedKey(c->db, c->argv[1]);
        notifyKeyspaceEvent(NOTIFY_HASH, "hset", c->argv[1], c->db->id);
        server.dirty++;
    }
}

//命令实现
//HMSET key field value [field value ...]
void hmsetCommand(client *c) {
    int i;
    robj *o;

    //参数数量必定是偶数
    if ((c->argc % 2) == 1) {
        addReplyError(c, "wrong number of arguments for HMSET");
        return;
    }

    //寻找key，若不存在创建并添加到db
    if ((o = hashTypeLookupWriteOrCreate(c, c->argv[1])) == NULL) {
        return;
    }

    //是否需要转换底层实现？
    hashTypeTryConversion(o, c->argv, 2, c->argc - 1);

    //遍历参数添加到hash
    for (i = 2; i < c->argc; i += 2) {
        //转换键值对的string编码类型，节省内存
        hashTypeTryObjectEncoding(o, &c->argv[i], &c->argv[i + 1]);
        //添加/更新
        hashTypeSet(o, c->argv[i], c->argv[i + 1]);
    }

    addReply(c, shared.ok);
    signalModifiedKey(c->db, c->argv[1]);
    notifyKeyspaceEvent(NOTIFY_HASH, "hset", c->argv[1], c->db->id);
    server.dirty++;
}

//命令实现
//HINCRBY key field increment
void hincrbyCommand(client *c) {
    long long value, incr, oldvalue;
    robj *o, *current, *new;

    //获取increment
    if (getLongLongFromObjectOrReply(c, c->argv[3], &incr, NULL) != C_OK) {
        return;
    }

    //寻找key，若不存在创建并添加到db
    if ((o = hashTypeLookupWriteOrCreate(c, c->argv[1])) == NULL) {
        return;
    }

    //从hash中获取field对应的value，以对象形式返回
    if ((current = hashTypeGetObject(o, c->argv[2])) != NULL) {
        //获取value的整数值
        if (getLongLongFromObjectOrReply(c, current, &value, "hash value is not an integer") != C_OK) {
            decrRefCount(current);
            return;
        }
        decrRefCount(current);
    }
    else {
        value = 0;
    }

    oldvalue = value;
    //incr后是否会溢出？
    if ((incr < 0 && oldvalue < 0 && incr < (LLONG_MIN - oldvalue)) ||
        (incr > 0 && oldvalue > 0 && incr > (LLONG_MAX - oldvalue))) {
        addReplyError(c, "increment or decrement would overflow");
        return;
    }

    //执行incr操作
    value += incr;
    //为incr后的值创建新的string对象
    new = createStringObjectFromLongLong(value);
    //转换编码类型，节省内存
    hashTypeTryObjectEncoding(o, &c->argv[2], NULL);
    //更新field
    hashTypeSet(o, c->argv[2], new);

    decrRefCount(new);
    addReplyLongLong(c, value);
    signalModifiedKey(c->db, c->argv[1]);
    notifyKeyspaceEvent(NOTIFY_HASH, "hincrby", c->argv[1], c->db->id);
    server.dirty++;
}

//命令实现
//HINCRBYFLOAT key field increment
void hincrbyfloatCommand(client *c) {
    double long value, incr;
    robj *o, *current, *new, *aux;

    //获取increment
    if (getLongDoubleFromObjectOrReply(c, c->argv[3], &incr, NULL) != C_OK) {
        return;
    }

    //寻找key，若不存在创建并添加到db
    if ((o = hashTypeLookupWriteOrCreate(c, c->argv[1])) == NULL) {
        return;
    }

    //从hash中获取field对应的value
    if ((current = hashTypeGetObject(o, c->argv[2])) != NULL) {
        //获取value的double值
        if (getLongDoubleFromObjectOrReply(c, current, &value, "hash value is not a valid float") != C_OK) {
            decrRefCount(current);
            return;
        }
        decrRefCount(current);
    }
    else {
        value = 0;
    }

    //执行incr操作
    value += incr;

    //为incr后的值创建string对象
    new = createStringObjectFromLongDouble(value, 1);
    //转换编码类型，节省内存
    hashTypeTryObjectEncoding(o, &c->argv[2], NULL);
    //更新filed
    hashTypeSet(o, c->argv[2], new);

    addReplyBulk(c, new);
    signalModifiedKey(c->db, c->argv[1]);
    notifyKeyspaceEvent(NOTIFY_HASH, "hincrbyfloat", c->argv[1], c->db->id);
    server.dirty++;

    /* Always replicate HINCRBYFLOAT as an HSET command with the final value
     * in order to make sure that differences in float pricision or formatting
     * will not create differences in replicas or after an AOF restart. */
    aux = createStringObject("HSET", 4);
    rewriteClientCommandArgument(c, 0, aux);
    decrRefCount(aux);
    rewriteClientCommandArgument(c, 3, new);
    decrRefCount(new);
}

//获取field对应的value，写入回复缓冲
static void addHashFieldToReply(client *c, robj *o, robj *field) {
    int ret;

    if (o == NULL) {
        addReply(c, shared.nullbulk);
        return;
    }

    if (o->encoding == OBJ_ENCODING_ZIPLIST) {
        unsigned char *vstr = NULL;
        unsigned int vlen = UINT_MAX;
        long long vll = LLONG_MAX;

        //从ziplist获取field的value
        ret = hashTypeGetFromZiplist(o, field, &vstr, &vlen, &vll);
        if (ret < 0) {
            //field不存在
            addReply(c, shared.nullbulk);
        }
        else {
            if (vstr) {
                //添加长度为len的c字符串到回复缓冲
                addReplyBulkCBuffer(c, vstr, vlen);
            }
            else {
                //添加long long到回复缓冲，以bulk的形式
                addReplyBulkLongLong(c, vll);
            }
        }

        //从dict获取field对应的value
    }
    else if (o->encoding == OBJ_ENCODING_HT) {
        robj *value;

        ret = hashTypeGetFromHashTable(o, field, &value);
        if (ret < 0) {
            //field不存在
            addReply(c, shared.nullbulk);
        }
        else {
            //添加对象到回复缓冲，以bulk的形式
            addReplyBulk(c, value);
        }

    }
    else {
        serverPanic("Unknown hash encoding");
    }
}

//命令实现
//HGET key field
void hgetCommand(client *c) {
    robj *o;

    //key不存在或类型错误
    if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.nullbulk)) == NULL || checkType(c, o, OBJ_HASH)) {
        return;
    }

    //添加field的value到回复缓冲
    addHashFieldToReply(c, o, c->argv[2]);
}

//命令实现
//HMGET key field [field ...]
void hmgetCommand(client *c) {
    robj *o;
    int i;

    /* Don't abort when the key cannot be found. Non-existing keys are empty
     * hashes, where HMGET should respond with a series of null bulks. */
    //key是否存在？
    o = lookupKeyRead(c->db, c->argv[1]);
    //不存在添加错误信息到回复缓冲
    if (o != NULL && o->type != OBJ_HASH) {
        addReply(c, shared.wrongtypeerr);
        return;
    }

    //添加返回的数组长度到回复缓冲，-2减去hmget和key
    addReplyMultiBulkLen(c, c->argc - 2);
    //遍历参数添加field的value到回复缓冲
    for (i = 2; i < c->argc; i++) {
        addHashFieldToReply(c, o, c->argv[i]);
    }
}

//命令实现
//HDEL key field [field ...]
void hdelCommand(client *c) {
    robj *o;
    int j, deleted = 0, keyremoved = 0;

    //key不存在或类型错误
    if ((o = lookupKeyWriteOrReply(c, c->argv[1], shared.czero)) == NULL || checkType(c, o, OBJ_HASH)) {
        return;
    }

    //遍历参数删除field
    for (j = 2; j < c->argc; j++) {
        if (hashTypeDelete(o, c->argv[j])) {
            deleted++;
            //若键值对全都删光了，从数据库删除key
            if (hashTypeLength(o) == 0) {
                dbDelete(c->db, c->argv[1]);
                keyremoved = 1;
                break;
            }
        }
    }

    if (deleted) {
        signalModifiedKey(c->db, c->argv[1]);
        notifyKeyspaceEvent(NOTIFY_HASH, "hdel", c->argv[1], c->db->id);
        if (keyremoved) {
            notifyKeyspaceEvent(NOTIFY_GENERIC, "del", c->argv[1],
                                c->db->id);
        }
        server.dirty += deleted;
    }

    addReplyLongLong(c, deleted);
}

//命令实现
//HLEN key
void hlenCommand(client *c) {
    robj *o;

    //key不存在或类型错误
    if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.czero)) == NULL || checkType(c, o, OBJ_HASH)) {
        return;
    }

    //添加键值对个数到回复缓冲
    addReplyLongLong(c, hashTypeLength(o));
}

//命令实现
//HSTRLEN key field
void hstrlenCommand(client *c) {
    robj *o;

    //key不存在或类型错误
    if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.czero)) == NULL ||
        checkType(c, o, OBJ_HASH)) {
        return;
    }
    addReplyLongLong(c, hashTypeGetValueLength(o, c->argv[2]));
}

//将迭代器当前迭代到的键值对写到回复缓冲
static void addHashIteratorCursorToReply(client *c, hashTypeIterator *hi, int what) {
    if (hi->encoding == OBJ_ENCODING_ZIPLIST) {
        unsigned char *vstr = NULL;
        unsigned int vlen = UINT_MAX;
        long long vll = LLONG_MAX;

        //将迭代器当前迭代的键/值写入vstr/vll
        hashTypeCurrentFromZiplist(hi, what, &vstr, &vlen, &vll);
        if (vstr) {
            addReplyBulkCBuffer(c, vstr, vlen);
        }
        else {
            addReplyBulkLongLong(c, vll);
        }

    }
    else if (hi->encoding == OBJ_ENCODING_HT) {
        robj *value;
        //将迭代器当前迭代到的键/值写入dst
        hashTypeCurrentFromHashTable(hi, what, &value);
        addReplyBulk(c, value);

    }
    else {
        serverPanic("Unknown hash encoding");
    }
}

//hkeys, hvals, hgetall命令的底层实现
//HGETALL key
void genericHgetallCommand(client *c, int flags) {
    robj *o;
    hashTypeIterator *hi;
    int multiplier = 0;
    int length, count = 0;

    //key不存在或类型错误
    if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.emptymultibulk)) == NULL
        || checkType(c, o, OBJ_HASH)) {
        return;
    }

    if (flags & OBJ_HASH_KEY) {
        multiplier++;
    }
    if (flags & OBJ_HASH_VALUE) {
        multiplier++;
    }

    //返回数组的长度
    length = hashTypeLength(o) * multiplier;
    //添加数组长度到回复缓冲
    addReplyMultiBulkLen(c, length);

    hi = hashTypeInitIterator(o);
    while (hashTypeNext(hi) != C_ERR) {
        if (flags & OBJ_HASH_KEY) {
            //field写入回复缓冲
            addHashIteratorCursorToReply(c, hi, OBJ_HASH_KEY);
            count++;
        }
        if (flags & OBJ_HASH_VALUE) {
            //value写入回复缓冲
            addHashIteratorCursorToReply(c, hi, OBJ_HASH_VALUE);
            count++;
        }
    }

    //释放迭代器
    hashTypeReleaseIterator(hi);

    serverAssert(count == length);
}

//命令实现
//HKEYS key
void hkeysCommand(client *c) {
    genericHgetallCommand(c, OBJ_HASH_KEY);
}

//命令实现
//HVALS key
void hvalsCommand(client *c) {
    genericHgetallCommand(c, OBJ_HASH_VALUE);
}

//命令实现
//HGETALL key
void hgetallCommand(client *c) {
    genericHgetallCommand(c, OBJ_HASH_KEY | OBJ_HASH_VALUE);
}

//命令实现
//HEXISTS key fielda
void hexistsCommand(client *c) {
    robj *o;
    //key不存在或类型错误
    if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.czero)) == NULL ||
        checkType(c, o, OBJ_HASH)) {
        return;
    }

    addReply(c, hashTypeExists(o, c->argv[2]) ? shared.cone : shared.czero);
}

//todo
void hscanCommand(client *c) {
    robj *o;
    unsigned long cursor;

    if (parseScanCursorOrReply(c, c->argv[2], &cursor) == C_ERR) { return; }
    if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.emptyscan)) == NULL ||
        checkType(c, o, OBJ_HASH)) {
        return;
    }
    scanGenericCommand(c, o, cursor);
}
