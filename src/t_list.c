/*
 * Copyright (c) 2009-2012, Salvatore Sanfilippo <antirez at gmail dot com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Redis nor the names of its contributors may be used
 *     to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "server.h"

/*-----------------------------------------------------------------------------
 * List API
 *----------------------------------------------------------------------------*/

/* The function pushes an element to the specified list object 'subject',
 * at head or tail position as specified by 'where'.
 *
 * There is no need for the caller to increment the refcount of 'value' as
 * the function takes care of it if needed. */
//在表头/表尾插入新元素到list
void listTypePush(robj *subject, robj *value, int where) {
    if (subject->encoding == OBJ_ENCODING_QUICKLIST) {
        //插入位置
        int pos = (where == LIST_HEAD) ? QUICKLIST_HEAD : QUICKLIST_TAIL;

        //为int编码的string对象创建一个embstr/raw编码的新对象
        value = getDecodedObject(value);

        //value长度
        size_t len = sdslen(value->ptr);

        //插入到list
        quicklistPush(subject->ptr, value->ptr, len, pos);

        //因为插入的是value底层的sds，所以可以减少value引用计数
        decrRefCount(value);
    }
    else {
        serverPanic("Unknown list encoding");
    }
}

//为弹出的字符串元素创建string对象
void *listPopSaver(unsigned char *data, unsigned int sz) {
    return createStringObject((char *) data, sz);
}

//从list表头或表尾弹出元素
robj *listTypePop(robj *subject, int where) {
    long long vlong;
    robj *value = NULL;

    //表头或表尾弹出？
    int ql_where = where == LIST_HEAD ? QUICKLIST_HEAD : QUICKLIST_TAIL;

    if (subject->encoding == OBJ_ENCODING_QUICKLIST) {
        //从表头/表尾弹出元素，值写入value/vlong
        if (quicklistPopCustom(subject->ptr, ql_where, (unsigned char **) &value, NULL, &vlong, listPopSaver)) {
            if (!value) {
                //元素保存的是整数值
                value = createStringObjectFromLongLong(vlong);
            }
        }
    }
    else {
        serverPanic("Unknown list encoding");
    }

    return value;
}

//获取list元素个数
unsigned long listTypeLength(robj *subject) {
    if (subject->encoding == OBJ_ENCODING_QUICKLIST) {
        //获取quicklist元素个数
        return quicklistCount(subject->ptr);
    }
    else {
        serverPanic("Unknown list encoding");
    }
}

/* Initialize an iterator at the specified index. */
//创建list迭代器
listTypeIterator *listTypeInitIterator(robj *subject, long index, unsigned char direction) {
    listTypeIterator *li = zmalloc(sizeof(listTypeIterator));
    li->subject = subject;
    li->encoding = subject->encoding;
    li->direction = direction;
    li->iter = NULL;

    /* LIST_HEAD means start at TAIL and move *towards* head.
     * LIST_TAIL means start at HEAD and move *towards tail. */
    //迭代方向
    int iter_direction = direction == LIST_HEAD ? AL_START_TAIL : AL_START_HEAD;

    //绑定quicklist迭代器
    if (li->encoding == OBJ_ENCODING_QUICKLIST) {
        li->iter = quicklistGetIteratorAtIdx(li->subject->ptr, iter_direction, index);
    }
    else {
        serverPanic("Unknown list encoding");
    }

    return li;
}

/* Clean up the iterator. */
//释放list迭代器
void listTypeReleaseIterator(listTypeIterator *li) {
    //释放quicklist迭代器
    zfree(li->iter);
    zfree(li);
}

/* Stores pointer to current the entry in the provided entry structure
 * and advances the position of the iterator. Returns 1 when the current
 * entry is in fact an entry, 0 otherwise. */
//list迭代器的下一个元素
int listTypeNext(listTypeIterator *li, listTypeEntry *entry) {
    /* Protect from converting when iterating */
    serverAssert(li->subject->encoding == li->encoding);

    entry->li = li;
    if (li->encoding == OBJ_ENCODING_QUICKLIST) {
        //获取quicklist迭代器的下一个元素
        return quicklistNext(li->iter, &entry->entry);
    }
    else {
        serverPanic("Unknown list encoding");
    }
    return 0;
}

/* Return entry or NULL at the current position of the iterator. */
//返回当前迭代元素的值
robj *listTypeGet(listTypeEntry *entry) {
    robj *value = NULL;
    if (entry->li->encoding == OBJ_ENCODING_QUICKLIST) {
        //为字符串数据创建string对象
        if (entry->entry.value) {
            value = createStringObject((char *) entry->entry.value,
                                       entry->entry.sz);
        }
            //为整数数据创建string对象
        else {
            value = createStringObjectFromLongLong(entry->entry.longval);
        }
    }
    else {
        serverPanic("Unknown list encoding");
    }
    return value;
}

//插入value到entry之前或之后
void listTypeInsert(listTypeEntry *entry, robj *value, int where) {
    if (entry->li->encoding == OBJ_ENCODING_QUICKLIST) {
        //为int编码的string对象创建一个embstr/raw编码的新对象
        value = getDecodedObject(value);

        //value保存的数据和数据长度
        sds str = value->ptr;
        size_t len = sdslen(str);

        //插入到entry之前或之后
        if (where == LIST_TAIL) {
            quicklistInsertAfter((quicklist *) entry->entry.quicklist, &entry->entry, str, len);
        }
        else if (where == LIST_HEAD) {
            quicklistInsertBefore((quicklist *) entry->entry.quicklist, &entry->entry, str, len);
        }
        decrRefCount(value);
    }
    else {
        serverPanic("Unknown list encoding");
    }
}

/* Compare the given object with the entry at the current position. */
//比较当前元素entry和对象o
int listTypeEqual(listTypeEntry *entry, robj *o) {
    if (entry->li->encoding == OBJ_ENCODING_QUICKLIST) {
        serverAssertWithInfo(NULL, o, sdsEncodedObject(o));
        //比较底层的ziplist存储的数据是否相等
        return quicklistCompare(entry->entry.zi, o->ptr, sdslen(o->ptr));
    }
    else {
        serverPanic("Unknown list encoding");
    }
}

/* Delete the element pointed to. */
//从list中删除entry元素
void listTypeDelete(listTypeIterator *iter, listTypeEntry *entry) {
    if (entry->li->encoding == OBJ_ENCODING_QUICKLIST) {
        //从quicklist删除entry所代表的元素
        quicklistDelEntry(iter->iter, &entry->entry);
    }
    else {
        serverPanic("Unknown list encoding");
    }
}

/* Create a quicklist from a single ziplist */
//以给定的ziplist创建新的quicklist
//subject为底层是ziplist实现的list对象
//不过最新版的redis，底层都是quicklist了
void listTypeConvert(robj *subject, int enc) {
    serverAssertWithInfo(NULL, subject, subject->type == OBJ_LIST);
    serverAssertWithInfo(NULL, subject, subject->encoding == OBJ_ENCODING_ZIPLIST);

    if (enc == OBJ_ENCODING_QUICKLIST) {
        size_t zlen = server.list_max_ziplist_size;
        int depth = server.list_compress_depth;
        subject->ptr = quicklistCreateFromZiplist(zlen, depth, subject->ptr);
        subject->encoding = OBJ_ENCODING_QUICKLIST;
    }
    else {
        serverPanic("Unsupported list conversion");
    }
}

/*-----------------------------------------------------------------------------
 * List Commands
 *----------------------------------------------------------------------------*/

//LPUSH/PRUSH命令底层实现
//LPUSH key value [value ...]
void pushGenericCommand(client *c, int where) {
    int j, waiting = 0, pushed = 0;

    robj *lobj = lookupKeyWrite(c->db, c->argv[1]);
    //类型错误
    if (lobj && lobj->type != OBJ_LIST) {
        addReply(c, shared.wrongtypeerr);
        return;
    }

    //遍历插入value
    for (j = 2; j < c->argc; j++) {
        //尝试将string对象编码类型由raw转换为embstr或int，节省内存
        c->argv[j] = tryObjectEncoding(c->argv[j]);

        //key不存在，先创建list对象
        if (!lobj) {
            lobj = createQuicklistObject();
            quicklistSetOptions(lobj->ptr, server.list_max_ziplist_size, server.list_compress_depth);
            //添加list对象到数据库
            dbAdd(c->db, c->argv[1], lobj);
        }

        //插入新元素到list
        listTypePush(lobj, c->argv[j], where);
        pushed++;
    }

    addReplyLongLong(c, waiting + (lobj ? listTypeLength(lobj) : 0));
    if (pushed) {
        char *event = (where == LIST_HEAD) ? "lpush" : "rpush";
        //该key已经被修改，通知watch该key的所有客户端
        signalModifiedKey(c->db, c->argv[1]);
        notifyKeyspaceEvent(NOTIFY_LIST, event, c->argv[1], c->db->id);
    }

    server.dirty += pushed;
}

//命令实现
//LPUSH key value [value ...]
void lpushCommand(client *c) {
    pushGenericCommand(c, LIST_HEAD);
}

//命令实现
//RPUSH key value [value ...]
void rpushCommand(client *c) {
    pushGenericCommand(c, LIST_TAIL);
}

//LPUSHX/RPUSHX/LINSERT的底层实现
//refval表示LINSERT命令的pivot
//LPUSHX key value
//LINSERT key BEFORE|AFTER pivot value
void pushxGenericCommand(client *c, robj *refval, robj *val, int where) {
    robj *subject;
    listTypeIterator *iter;
    listTypeEntry entry;
    int inserted = 0;

    //key不存在或类型错误
    if ((subject = lookupKeyWriteOrReply(c, c->argv[1], shared.czero)) == NULL ||
        checkType(c, subject, OBJ_LIST)) {
        return;
    }

    //LINSERT命令
    if (refval != NULL) {
        /* Seek refval from head to tail */
        //遍历list寻找pivot
        iter = listTypeInitIterator(subject, 0, LIST_TAIL);
        while (listTypeNext(iter, &entry)) {
            //是否等于pivot？
            if (listTypeEqual(&entry, refval)) {
                //插入到pivot之前或之后
                listTypeInsert(&entry, val, where);
                inserted = 1;
                break;
            }
        }
        //释放迭代器
        listTypeReleaseIterator(iter);

        if (inserted) {
            //该key已经被修改，通知watch该key的所有客户端
            signalModifiedKey(c->db, c->argv[1]);
            notifyKeyspaceEvent(NOTIFY_LIST, "linsert",
                                c->argv[1], c->db->id);
            server.dirty++;
        }
        else {
            /* Notify client of a failed insert */
            addReply(c, shared.cnegone);
            return;
        }
    }
        //LPUSHX/RPUSHX命令
    else {
        char *event = (where == LIST_HEAD) ? "lpush" : "rpush";

        //将val插入到list表头或表尾
        listTypePush(subject, val, where);

        //该key已经被修改，通知watch该key的所有客户端
        signalModifiedKey(c->db, c->argv[1]);

        notifyKeyspaceEvent(NOTIFY_LIST, event, c->argv[1], c->db->id);
        server.dirty++;
    }

    addReplyLongLong(c, listTypeLength(subject));
}

//命令实现
//LPUSHX key value
void lpushxCommand(client *c) {
    //尝试将string对象编码类型由raw转换为embstr或int，节省内存
    c->argv[2] = tryObjectEncoding(c->argv[2]);
    //插入元素到list
    pushxGenericCommand(c, NULL, c->argv[2], LIST_HEAD);
}

//命令实现
//RPUSHX key value
void rpushxCommand(client *c) {
    //尝试将string对象编码类型由raw转换为embstr或int，节省内存
    c->argv[2] = tryObjectEncoding(c->argv[2]);
    //插入元素到list
    pushxGenericCommand(c, NULL, c->argv[2], LIST_TAIL);
}

//命令实现
//LINSERT key BEFORE|AFTER pivot value
void linsertCommand(client *c) {
    //尝试将string对象编码类型由raw转换为embstr或int，节省内存
    c->argv[4] = tryObjectEncoding(c->argv[4]);

    //在pivot之前或之后插入value
    if (strcasecmp(c->argv[2]->ptr, "after") == 0) {
        pushxGenericCommand(c, c->argv[3], c->argv[4], LIST_TAIL);
    }
    else if (strcasecmp(c->argv[2]->ptr, "before") == 0) {
        pushxGenericCommand(c, c->argv[3], c->argv[4], LIST_HEAD);
    }
    else {
        //语法错误
        addReply(c, shared.syntaxerr);
    }
}

//命令实现
//LLEN key
void llenCommand(client *c) {
    //key不存在或类型错误
    robj *o = lookupKeyReadOrReply(c, c->argv[1], shared.czero);
    if (o == NULL || checkType(c, o, OBJ_LIST)) {
        return;
    }

    //写入list元素个数到回复缓冲
    addReplyLongLong(c, listTypeLength(o));
}

//命令实现
//LINDEX key index
void lindexCommand(client *c) {
    //key不存在或类型错误
    robj *o = lookupKeyReadOrReply(c, c->argv[1], shared.nullbulk);
    if (o == NULL || checkType(c, o, OBJ_LIST)) {
        return;
    }

    long index;
    robj *value = NULL;

    //获取index
    if ((getLongFromObjectOrReply(c, c->argv[2], &index, NULL) != C_OK)) {
        return;
    }

    if (o->encoding == OBJ_ENCODING_QUICKLIST) {
        quicklistEntry entry;
        //获取第index个元素，写入entry
        if (quicklistIndex(o->ptr, index, &entry)) {
            //保存的是字符串数据
            if (entry.value) {
                value = createStringObject((char *) entry.value, entry.sz);
            }
                //保存的是整数数据
            else {
                value = createStringObjectFromLongLong(entry.longval);
            }
            //添加value到回复缓冲
            addReplyBulk(c, value);
            decrRefCount(value);
        }
        else {
            addReply(c, shared.nullbulk);
        }
    }
    else {
        serverPanic("Unknown list encoding");
    }
}

//命令实现
//LSET key index value
void lsetCommand(client *c) {
    //key不存在或类型错误
    robj *o = lookupKeyWriteOrReply(c, c->argv[1], shared.nokeyerr);
    if (o == NULL || checkType(c, o, OBJ_LIST)) {
        return;
    }

    long index;
    robj *value = c->argv[3];

    //获取index
    if ((getLongFromObjectOrReply(c, c->argv[2], &index, NULL) != C_OK)) {
        return;
    }

    if (o->encoding == OBJ_ENCODING_QUICKLIST) {
        quicklist *ql = o->ptr;

        //更新第index个元素的值
        int replaced = quicklistReplaceAtIndex(ql, index, value->ptr, sdslen(value->ptr));

        if (!replaced) {
            addReply(c, shared.outofrangeerr);
        }
        else {
            addReply(c, shared.ok);
            signalModifiedKey(c->db, c->argv[1]);
            notifyKeyspaceEvent(NOTIFY_LIST, "lset", c->argv[1], c->db->id);
            server.dirty++;
        }
    }
    else {
        serverPanic("Unknown list encoding");
    }
}

//LPOP/RPOP命令的底层实现
//LPOP key
void popGenericCommand(client *c, int where) {
    //key不存在或类型错误
    robj *o = lookupKeyWriteOrReply(c, c->argv[1], shared.nullbulk);
    if (o == NULL || checkType(c, o, OBJ_LIST)) {
        return;
    }

    //从list表头或表尾弹出元素
    robj *value = listTypePop(o, where);
    if (value == NULL) {
        addReply(c, shared.nullbulk);
    }
    else {
        char *event = (where == LIST_HEAD) ? "lpop" : "rpop";

        addReplyBulk(c, value);
        decrRefCount(value);
        notifyKeyspaceEvent(NOTIFY_LIST, event, c->argv[1], c->db->id);

        //如果弹出的是list的最后一个元素，将list从数据库删除
        if (listTypeLength(o) == 0) {
            notifyKeyspaceEvent(NOTIFY_GENERIC, "del", c->argv[1], c->db->id);
            dbDelete(c->db, c->argv[1]);
        }
        signalModifiedKey(c->db, c->argv[1]);
        server.dirty++;
    }
}

//命令实现
//LPOP key
void lpopCommand(client *c) {
    popGenericCommand(c, LIST_HEAD);
}

//命令实现
//RPOP key
void rpopCommand(client *c) {
    popGenericCommand(c, LIST_TAIL);
}

//命令实现
//LRANGE key start stop
void lrangeCommand(client *c) {
    robj *o;
    long start, end, llen, rangelen;

    //获取start和stop
    if ((getLongFromObjectOrReply(c, c->argv[2], &start, NULL) != C_OK) ||
        (getLongFromObjectOrReply(c, c->argv[3], &end, NULL) != C_OK)) {
        return;
    }

    //key不存在或类型错误
    if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.emptymultibulk)) == NULL
        || checkType(c, o, OBJ_LIST)) {
        return;
    }

    //list元素个数
    llen = listTypeLength(o);

    /* convert negative indexes */
    //处理负数start/end
    if (start < 0) { start = llen + start; }
    if (end < 0) { end = llen + end; }
    if (start < 0) { start = 0; }

    /* Invariant: start >= 0, so this test will be true when end < 0.
     * The range is empty when start > end or start >= length. */
    //不合法的start/end
    if (start > end || start >= llen) {
        addReply(c, shared.emptymultibulk);
        return;
    }

    //end不能越界
    if (end >= llen) {
        end = llen - 1;
    }

    //range的元素个数
    rangelen = (end - start) + 1;

    /* Return the result in form of a multi-bulk reply */
    //添加返回的元素个数到回复缓冲
    addReplyMultiBulkLen(c, rangelen);

    if (o->encoding == OBJ_ENCODING_QUICKLIST) {
        //创建从start开始的list迭代器
        listTypeIterator *iter = listTypeInitIterator(o, start, LIST_TAIL);

        //从start开始，获取rangelen个元素
        while (rangelen--) {
            listTypeEntry entry;
            listTypeNext(iter, &entry);
            quicklistEntry *qe = &entry.entry;
            //将元素值写入回复缓冲
            if (qe->value) {
                //字符串值
                addReplyBulkCBuffer(c, qe->value, qe->sz);
            }
            else {
                //整数值
                addReplyBulkLongLong(c, qe->longval);
            }
        }
        //释放迭代器
        listTypeReleaseIterator(iter);
    }
    else {
        serverPanic("List encoding is not QUICKLIST!");
    }
}

//命令实现
//LTRIM key start stop
void ltrimCommand(client *c) {
    robj *o;
    long start, end, llen, ltrim, rtrim;

    //获取start和stop
    if ((getLongFromObjectOrReply(c, c->argv[2], &start, NULL) != C_OK) ||
        (getLongFromObjectOrReply(c, c->argv[3], &end, NULL) != C_OK)) {
        return;
    }

    //key不存在或类型错误
    if ((o = lookupKeyWriteOrReply(c, c->argv[1], shared.ok)) == NULL ||
        checkType(c, o, OBJ_LIST)) {
        return;
    }

    //list元素个数
    llen = listTypeLength(o);

    /* convert negative indexes */
    //处理负数的start/end
    if (start < 0) { start = llen + start; }
    if (end < 0) { end = llen + end; }
    if (start < 0) { start = 0; }

    /* Invariant: start >= 0, so this test will be true when end < 0.
     * The range is empty when start > end or start >= length. */
    //根据start和end，计算ltrim和rtrim
    if (start > end || start >= llen) {
        /* Out of range start or start > end result in empty list */
        ltrim = llen;
        rtrim = 0;
    }
    else {
        if (end >= llen) { end = llen - 1; }
        ltrim = start;
        rtrim = llen - end - 1;
    }

    /* Remove list elements to perform the trim */
    //从list中删除start/end左右两边的元素
    if (o->encoding == OBJ_ENCODING_QUICKLIST) {
        quicklistDelRange(o->ptr, 0, ltrim);
        quicklistDelRange(o->ptr, -rtrim, rtrim);
    }
    else {
        serverPanic("Unknown list encoding");
    }

    notifyKeyspaceEvent(NOTIFY_LIST, "ltrim", c->argv[1], c->db->id);

    //如果list为空了，将list从数据库删除
    if (listTypeLength(o) == 0) {
        dbDelete(c->db, c->argv[1]);
        notifyKeyspaceEvent(NOTIFY_GENERIC, "del", c->argv[1], c->db->id);
    }

    signalModifiedKey(c->db, c->argv[1]);
    server.dirty++;
    addReply(c, shared.ok);
}

//命令实现
//LREM key count value
void lremCommand(client *c) {
    robj *subject, *obj;

    //value
    obj = c->argv[3];

    long toremove;
    long removed = 0;

    //获取count
    if ((getLongFromObjectOrReply(c, c->argv[2], &toremove, NULL) != C_OK)) {
        return;
    }

    //key不存在或类型错误
    subject = lookupKeyWriteOrReply(c, c->argv[1], shared.czero);
    if (subject == NULL || checkType(c, subject, OBJ_LIST)) {
        return;
    }

    //创建list迭代器
    //count > 0从头到尾，反之从尾到头
    //LIST_HEAD表示从尾到头迭代
    listTypeIterator *li;
    if (toremove < 0) {
        toremove = -toremove;
        li = listTypeInitIterator(subject, -1, LIST_HEAD);
    }
    else {
        li = listTypeInitIterator(subject, 0, LIST_TAIL);
    }

    //遍历list，删除count个value
    listTypeEntry entry;
    while (listTypeNext(li, &entry)) {
        if (listTypeEqual(&entry, obj)) {
            //删除等于value的元素
            listTypeDelete(li, &entry);
            server.dirty++;
            removed++;
            //已经删除了count个value，停止删除操作
            if (toremove && removed == toremove) {
                break;
            }
        }
    }

    //释放迭代器
    listTypeReleaseIterator(li);

    if (removed) {
        //该key已经被修改，通知watch该key的所有客户端
        signalModifiedKey(c->db, c->argv[1]);

        notifyKeyspaceEvent(NOTIFY_GENERIC, "lrem", c->argv[1], c->db->id);
    }

    //list没有元素了，将list从数据库删除
    if (listTypeLength(subject) == 0) {
        dbDelete(c->db, c->argv[1]);
        notifyKeyspaceEvent(NOTIFY_GENERIC, "del", c->argv[1], c->db->id);
    }

    addReplyLongLong(c, removed);
}

/* This is the semantic of this command:
 *  RPOPLPUSH srclist dstlist:
 *    IF LLEN(srclist) > 0
 *      element = RPOP srclist
 *      LPUSH dstlist element
 *      RETURN element
 *    ELSE
 *      RETURN nil
 *    END
 *  END
 *
 * The idea is to be able to get an element from a list in a reliable way
 * since the element is not just returned but pushed against another list
 * as well. This command was originally proposed by Ezra Zygmuntowicz.
 */

//rpoplpushCommand的辅助函数
//将source弹出的value插入到destination表头
void rpoplpushHandlePush(client *c, robj *dstkey, robj *dstobj, robj *value) {
    /* Create the list if the key does not exist */
    //若destination不存在，创建并添加到数据库
    if (!dstobj) {
        dstobj = createQuicklistObject();
        quicklistSetOptions(dstobj->ptr, server.list_max_ziplist_size, server.list_compress_depth);
        dbAdd(c->db, dstkey, dstobj);
    }

    signalModifiedKey(c->db, dstkey);

    //将弹出的value插入到destination表头
    listTypePush(dstobj, value, LIST_HEAD);

    notifyKeyspaceEvent(NOTIFY_LIST, "lpush", dstkey, c->db->id);
    /* Always send the pushed value to the client. */
    addReplyBulk(c, value);
}

//命令实现
//RPOPLPUSH source destination
void rpoplpushCommand(client *c) {
    robj *sobj, *value;

    //source不存在或类型错误
    if ((sobj = lookupKeyWriteOrReply(c, c->argv[1], shared.nullbulk)) == NULL ||
        checkType(c, sobj, OBJ_LIST)) {
        return;
    }

    //source长度为0
    //只有读取rdb文件的时候才会出现这种情况，因为redis会将长度为0的list从数据库删除
    if (listTypeLength(sobj) == 0) {
        /* This may only happen after loading very old RDB files. Recent
         * versions of Redis delete keys of empty lists. */
        addReply(c, shared.nullbulk);
    }
    else {
        //获取destination
        robj *dobj = lookupKeyWrite(c->db, c->argv[2]);

        robj *touchedkey = c->argv[1];

        //destination类型错误
        if (dobj && checkType(c, dobj, OBJ_LIST)) {
            return;
        }

        //从source表尾弹出元素
        value = listTypePop(sobj, LIST_TAIL);
        /* We saved touched key, and protect it, since rpoplpushHandlePush
         * may change the client command argument vector (it does not
         * currently). */

        incrRefCount(touchedkey);

        //将弹出元素插入到destination表头
        rpoplpushHandlePush(c, c->argv[2], dobj, value);

        /* listTypePop returns an object with its refcount incremented */
        decrRefCount(value);

        /* Delete the source list when it is empty */
        notifyKeyspaceEvent(NOTIFY_LIST, "rpop", touchedkey, c->db->id);

        //如果source为空，从数据库删除source
        if (listTypeLength(sobj) == 0) {
            dbDelete(c->db, touchedkey);
            notifyKeyspaceEvent(NOTIFY_GENERIC, "del", touchedkey, c->db->id);
        }

        signalModifiedKey(c->db, touchedkey);
        decrRefCount(touchedkey);
        server.dirty++;
    }
}

/*-----------------------------------------------------------------------------
 * Blocking POP operations
 *----------------------------------------------------------------------------*/

/* This is how the current blocking POP works, we use BLPOP as example:
 * - If the user calls BLPOP and the key exists and contains a non empty list
 *   then LPOP is called instead. So BLPOP is semantically the same as LPOP
 *   if blocking is not required.
 * - If instead BLPOP is called and the key does not exists or the list is
 *   empty we need to block. In order to do so we remove the notification for
 *   new data to read in the client socket (so that we'll not serve new
 *   requests if the blocking request is not served). Also we put the client
 *   in a dictionary (db->blocking_keys) mapping keys to a list of clients
 *   blocking for this keys.
 * - If a PUSH operation against a key with blocked clients waiting is
 *   performed, we mark this key as "ready", and after the current command,
 *   MULTI/EXEC block, or script, is executed, we serve all the clients waiting
 *   for this list, from the one that blocked first, to the last, accordingly
 *   to the number of elements we have in the ready list.
 */

//执行阻塞命令，比如blpop时，若对应的key不存在，则更新客户端为阻塞状态
//将客户端添加到数据库blocking_keys字典中对应key的链表
//同时也将key添加到客户端阻塞key集合中

//执行诸如push命令时，将相应的key放到服务器的ready_keys链表和数据库ready_keys集合
//processCommand函数执行完命令后，会检查并处理服务器ready_keys链表
//ready_keys可看作队列，若多个客户端阻塞于同一个key，阻塞时间最长的优先得到处理

//当处于阻塞状态时，若用户发送新的命令过来，命令数据会堆积在客户端的querybuf中
//当客户端解除阻塞状态后，beforeSleep优先处理这些客户端在阻塞期间发送的命令

//todo 英文注释中这一段
//todo In order to do so we remove the notification for
//todo new data to read in the client socket
//todo 当客户端阻塞时，删除客户端在事件循环中注册的可读事件，不再读取用户发送过来的数据
//todo 但是实际代码中好像没有发现这个操作？
//todo 阻塞期间正常读取用户发送的数据
//todo 但是不去分析这些数据，而是堆积在querybuf

/* Set a client in blocking mode for the specified key, with the specified
 * timeout */
//设置客户端为阻塞状态
void blockForKeys(client *c, robj **keys, int numkeys, mstime_t timeout, robj *target) {
    dictEntry *de;
    list *l;
    int j;

    c->bpop.timeout = timeout;
    c->bpop.target = target;

    if (target != NULL) {
        incrRefCount(target);
    }

    //将要阻塞的key添加到客户端阻塞key集合
    for (j = 0; j < numkeys; j++) {
        /* If the key already exists in the dict ignore it. */
        if (dictAdd(c->bpop.keys, keys[j], NULL) != DICT_OK) {
            continue;
        }
        incrRefCount(keys[j]);

        /* And in the other "side", to map keys -> clients */
        //将客户端添加到数据库阻塞字典
        de = dictFind(c->db->blocking_keys, keys[j]);
        //没有其他客户端阻塞在该key上
        if (de == NULL) {
            int retval;
            /* For every key we take a list of clients blocked for it */
            //创建该key的阻塞链表
            l = listCreate();
            //添加到数据库阻塞字典
            retval = dictAdd(c->db->blocking_keys, keys[j], l);
            incrRefCount(keys[j]);
            serverAssertWithInfo(c, keys[j], retval == DICT_OK);
        }
        else {
            //获取该key的客户端阻塞链表
            l = dictGetVal(de);
        }
        //添加客户端到阻塞链表
        listAddNodeTail(l, c);
    }

    //修改客户端为阻塞状态
    blockClient(c, BLOCKED_LIST);
}

/* Unblock a client that's waiting in a blocking operation such as BLPOP.
 * You should never call this function directly, but unblockClient() instead. */
//取消一个客户端的阻塞状态
void unblockClientWaitingData(client *c) {
    dictEntry *de;
    dictIterator *di;
    list *l;

    serverAssertWithInfo(c, NULL, dictSize(c->bpop.keys) != 0);

    //客户端阻塞key集合迭代器
    di = dictGetIterator(c->bpop.keys);
    /* The client may wait for multiple keys, so unblock it for every key. */

    //遍历客户端阻塞key集合
    while ((de = dictNext(di)) != NULL) {
        robj *key = dictGetKey(de);

        /* Remove this client from the list of clients waiting for this key. */
        //阻塞在该key的客户端链表
        l = dictFetchValue(c->db->blocking_keys, key);
        serverAssertWithInfo(c, key, l != NULL);

        //在链表中找到客户端并从链表中删除
        listDelNode(l, listSearchKey(l, c));

        /* If the list is empty we need to remove it to avoid wasting memory */
        //如果没有任何客户端阻塞在该key了，将key从服务器阻塞字典删除
        if (listLength(l) == 0) {
            dictDelete(c->db->blocking_keys, key);
        }
    }
    //释放迭代器
    dictReleaseIterator(di);

    /* Cleanup the client structure */
    //清空客户端阻塞key集合
    dictEmpty(c->bpop.keys, NULL);

    //减少target对象的引用计数
    if (c->bpop.target) {
        decrRefCount(c->bpop.target);
        c->bpop.target = NULL;
    }
}

/* If the specified key has clients blocked waiting for list pushes, this
 * function will put the key reference into the server.ready_keys list.
 * Note that db->ready_keys is a hash table that allows us to avoid putting
 * the same key again and again in the list in case of multiple pushes
 * made by a script or in the context of MULTI/EXEC.
 *
 * The list will be finally processed by handleClientsBlockedOnLists() */
//key元素(list, set, ...)已经不为空了，将key放入服务器ready_keys链表和数据库ready_keys集合中
//processCommand函数执行完命令后会检查ready_keys
void signalListAsReady(redisDb *db, robj *key) {
    readyList *rl;

    /* No clients blocking for this key? No need to queue it. */
    //没有客户端阻塞在该key上面
    if (dictFind(db->blocking_keys, key) == NULL) {
        return;
    }

    /* Key was already signaled? No need to queue it again. */
    //key已经放到ready_keys了
    if (dictFind(db->ready_keys, key) != NULL) {
        return;
    }

    /* Ok, we need to queue this key into server.ready_keys. */
    //将key放入服务器ready_keys链表
    rl = zmalloc(sizeof(*rl));
    rl->key = key;
    rl->db = db;
    incrRefCount(key);
    listAddNodeTail(server.ready_keys, rl);

    /* We also add the key in the db->ready_keys dictionary in order
     * to avoid adding it multiple times into a list with a simple O(1)
     * check. */
    //将key放入数据库ready_keys集合
    //数据库ready_keys集合唯一的作用就是避免重复添加key到服务器ready_keys链表
    incrRefCount(key);
    serverAssert(dictAdd(db->ready_keys, key, NULL) == DICT_OK);
}

/* This is a helper function for handleClientsBlockedOnLists(). It's work
 * is to serve a specific client (receiver) that is blocked on 'key'
 * in the context of the specified 'db', doing the following:
 *
 * 1) Provide the client with the 'value' element.
 * 2) If the dstkey is not NULL (we are serving a BRPOPLPUSH) also push the
 *    'value' element on the destination list (the LPUSH side of the command).
 * 3) Propagate the resulting BRPOP, BLPOP and additional LPUSH if any into
 *    the AOF and replication channel.
 *
 * The argument 'where' is LIST_TAIL or LIST_HEAD, and indicates if the
 * 'value' element was popped fron the head (BLPOP) or tail (BRPOP) so that
 * we can propagate the command properly.
 *
 * The function returns C_OK if we are able to serve the client, otherwise
 * C_ERR is returned to signal the caller that the list POP operation
 * should be undone as the client was not served: This only happens for
 * BRPOPLPUSH that fails to push the value to the destination key as it is
 * of the wrong type. */
//主要做三件事
//1. 将弹出的元素添加到回复缓存
//2. 向aof/从服务器传播普通的pop命令
//3. 若执行的是BRPOPLPUSH命令，将弹出的元素push到destination
int serveClientBlockedOnList(client *receiver, robj *key, robj *dstkey, redisDb *db, robj *value, int where) {
    robj *argv[3];

    if (dstkey == NULL) {
        /* Propagate the [LR]POP operation. */
        argv[0] = (where == LIST_HEAD) ? shared.lpop : shared.rpop;
        argv[1] = key;

        //传播命令
        propagate((where == LIST_HEAD) ?
                  server.lpopCommand : server.rpopCommand,
                  db->id, argv, 2, PROPAGATE_AOF | PROPAGATE_REPL);

        /* BRPOP/BLPOP */
        //添加弹出元素到回复缓冲
        addReplyMultiBulkLen(receiver, 2);
        addReplyBulk(receiver, key);
        addReplyBulk(receiver, value);
    }
    else {
        /* BRPOPLPUSH */
        //执行的是BRPOPLPUSH命令

        //获取destination
        robj *dstobj = lookupKeyWrite(receiver->db, dstkey);

        if (!(dstobj && checkType(receiver, dstobj, OBJ_LIST))) {
            /* Propagate the RPOP operation. */
            argv[0] = shared.rpop;
            argv[1] = key;

            //传播pop命令
            propagate(server.rpopCommand, db->id, argv, 2, PROPAGATE_AOF | PROPAGATE_REPL);

            //push弹出的value到destination
            rpoplpushHandlePush(receiver, dstkey, dstobj, value);

            /* Propagate the LPUSH operation. */
            //同时传播push命令
            argv[0] = shared.lpush;
            argv[1] = dstkey;
            argv[2] = value;
            propagate(server.lpushCommand, db->id, argv, 3, PROPAGATE_AOF | PROPAGATE_REPL);
        }
        else {
            /* BRPOPLPUSH failed because of wrong
             * destination type. */
            //destination类型错误
            return C_ERR;
        }
    }

    return C_OK;
}

/* This function should be called by Redis every time a single command,
 * a MULTI/EXEC block, or a Lua script, terminated its execution after
 * being called by a client.
 *
 * All the keys with at least one client blocked that received at least
 * one new element via some PUSH operation are accumulated into
 * the server.ready_keys list. This function will run the list and will
 * serve clients accordingly. Note that the function will iterate again and
 * again as a result of serving BRPOPLPUSH we can have new blocking clients
 * to serve because of the PUSH side of BRPOPLPUSH. */
//处理ready_keys，解除部分客户端的阻塞状态
//被processCommand函数调用
void handleClientsBlockedOnLists(void) {
    //遍历处理ready_keys链表
    while (listLength(server.ready_keys) != 0) {
        list *l;

        /* Point server.ready_keys to a fresh list and save the current one
         * locally. This way as we run the old list we are free to call
         * signalListAsReady() that may push new elements in server.ready_keys
         * when handling clients blocked into BRPOPLPUSH. */
        l = server.ready_keys;
        server.ready_keys = listCreate();

        //遍历处理ready_keys链表
        while (listLength(l) != 0) {
            //获取key
            listNode *ln = listFirst(l);
            readyList *rl = ln->value;

            /* First of all remove this key from db->ready_keys so that
             * we can safely call signalListAsReady() against this key. */
            //将key从数据库ready_keys集合删除
            dictDelete(rl->db->ready_keys, rl->key);

            /* If the key exists and it's a list, serve blocked clients
             * with data. */
            //从数据库获取key
            robj *o = lookupKeyWrite(rl->db, rl->key);

            //key是list类型
            if (o != NULL && o->type == OBJ_LIST) {
                dictEntry *de;

                /* We serve clients in the same order they blocked for
                 * this key, from the first blocked to the last. */
                //获取阻塞在该key上的客户端链表
                de = dictFind(rl->db->blocking_keys, rl->key);
                if (de) {
                    //客户端链表
                    list *clients = dictGetVal(de);

                    //阻塞在该key的客户端数量
                    int numclients = listLength(clients);

                    //遍历处理阻塞在当前key上面的客户端
                    //先进先出
                    while (numclients--) {
                        //获取链表头节点
                        listNode *clientnode = listFirst(clients);

                        //获取客户端
                        //下面的unblockClient函数会将该客户端从blocking_keys链表删除
                        client *receiver = clientnode->value;

                        //BRPOPLPUSH命令中的destination
                        robj *dstkey = receiver->bpop.target;

                        //执行的阻塞命令是BLPOP还是BRPOP？
                        int where = (receiver->lastcmd &&
                                     receiver->lastcmd->proc == blpopCommand) ?
                                    LIST_HEAD : LIST_TAIL;
                        //从list表头或表尾弹出元素
                        robj *value = listTypePop(o, where);

                        if (value) {
                            /* Protect receiver->bpop.target, that will be
                             * freed by the next unblockClient()
                             * call. */
                            if (dstkey) {
                                //unblockClient会减少target的引用计数
                                incrRefCount(dstkey);
                            }
                            //取消客户端的阻塞状态
                            unblockClient(receiver);

                            //主要做三件事
                            //1. 将弹出的元素添加到回复缓存
                            //2. 向aof/从服务器传播普通的pop命令
                            //3. 若执行的是BRPOPLPUSH命令，将弹出的元素push到destination
                            if (serveClientBlockedOnList(receiver, rl->key, dstkey, rl->db, value, where) == C_ERR) {
                                /* If we failed serving the client we need
                                 * to also undo the POP operation. */
                                //若serveClientBlockedOnList返回错误，重新push弹出的元素
                                listTypePush(o, value, where);
                            }

                            //减少target的引用计数
                            if (dstkey) {
                                decrRefCount(dstkey);
                            }

                            decrRefCount(value);
                        }
                        else {
                            //弹出元素失败，可能是list空了
                            //break跳出处理当前key客户端链表的while循环
                            break;
                        }
                    } //处理当前key客户端链表的while循环在这里结束
                }

                //若key对应的list为空，将key从数据库删除
                if (listTypeLength(o) == 0) {
                    dbDelete(rl->db, rl->key);
                }

                /* We don't call signalModifiedKey() as it was already called
                 * when an element was pushed on the list. */
            }

            /* Free this item. */
            decrRefCount(rl->key);
            zfree(rl);

            //将key从ready_keys链表删除
            listDelNode(l, ln);

        } //遍历处理ready_keys链表的while循环在这里结束

        listRelease(l); /* We have the new list on place at this point. */
    }
}

/* Blocking RPOP/LPOP */
//BLPOP/BRPOP命令底层实现
//BLPOP key [key ...] timeout
void blockingPopGenericCommand(client *c, int where) {
    robj *o;
    mstime_t timeout;
    int j;

    //获取超时时间
    if (getTimeoutFromObjectOrReply(c, c->argv[c->argc - 1], &timeout, UNIT_SECONDS) != C_OK) {
        return;
    }

    //遍历要pop的list
    for (j = 1; j < c->argc - 1; j++) {
        o = lookupKeyWrite(c->db, c->argv[j]);
        //key类型错误
        if (o != NULL) {
            if (o->type != OBJ_LIST) {
                addReply(c, shared.wrongtypeerr);
                return;
            }
            else {
                //list不为空，则等同于普通的POP命令
                if (listTypeLength(o) != 0) {
                    /* Non empty list, this is like a non normal [LR]POP. */
                    char *event = (where == LIST_HEAD) ? "lpop" : "rpop";
                    //从list表头或表尾弹出元素
                    robj *value = listTypePop(o, where);
                    serverAssert(value != NULL);

                    //添加回复到缓冲区
                    addReplyMultiBulkLen(c, 2);
                    addReplyBulk(c, c->argv[j]);
                    addReplyBulk(c, value);

                    decrRefCount(value);
                    notifyKeyspaceEvent(NOTIFY_LIST, event, c->argv[j], c->db->id);

                    //如果弹出后list为空了，从数据库删除list
                    if (listTypeLength(o) == 0) {
                        dbDelete(c->db, c->argv[j]);
                        notifyKeyspaceEvent(NOTIFY_GENERIC, "del", c->argv[j], c->db->id);
                    }

                    signalModifiedKey(c->db, c->argv[j]);
                    server.dirty++;

                    /* Replicate it as an [LR]POP instead of B[LR]POP. */
                    //传播到aof/从服务器之前重写命令
                    //因为成功弹出，所以把阻塞的命令重写为普通的pop命令
                    rewriteClientCommandVector(c, 2, (where == LIST_HEAD) ? shared.lpop : shared.rpop, c->argv[j]);

                    //只要有一个list成功弹出元素，就不会阻塞客户端，不管剩下的list是否为空
                    return;
                }
            }
        }
    }

    /* If we are inside a MULTI/EXEC and the list is empty the only thing
     * we can do is treating it as a timeout (even with timeout 0). */
    //客户端处于事务状态且list为空，则以阻塞超时处理
    if (c->flags & CLIENT_MULTI) {
        addReply(c, shared.nullmultibulk);
        return;
    }

    /* If the list is empty or the key does not exists we must block */
    //list为空，阻塞客户端
    blockForKeys(c, c->argv + 1, c->argc - 2, timeout, NULL);
}

//命令实现
//BLPOP key [key ...] timeout
void blpopCommand(client *c) {
    blockingPopGenericCommand(c, LIST_HEAD);
}

//命令实现
//BRPOP key [key ...] timeout
void brpopCommand(client *c) {
    blockingPopGenericCommand(c, LIST_TAIL);
}

//命令实现
//BRPOPLPUSH source destination timeout
void brpoplpushCommand(client *c) {
    mstime_t timeout;

    //获取超时时间
    if (getTimeoutFromObjectOrReply(c, c->argv[3], &timeout, UNIT_SECONDS)
        != C_OK) {
        return;
    }

    //获取source
    robj *key = lookupKeyWrite(c->db, c->argv[1]);
    //source不存在
    if (key == NULL) {
        //处于事务状态，按超时处理
        if (c->flags & CLIENT_MULTI) {
            /* Blocking against an empty list in a multi state
             * returns immediately. */
            addReply(c, shared.nullbulk);
        }
            //否则，阻塞客户端
        else {
            /* The list is empty and the client blocks. */
            blockForKeys(c, c->argv + 1, 1, timeout, c->argv[2]);
        }
    }
        //source存在
        //因为redis会将空list从数据库删除，所以list存在的话必定不为空
    else {
        //类型错误
        if (key->type != OBJ_LIST) {
            addReply(c, shared.wrongtypeerr);
        }
        else {
            /* The list exists and has elements, so
             * the regular rpoplpushCommand is executed. */
            //source存在的话就是普通的rpoplpush命令
            serverAssertWithInfo(c, key, listTypeLength(key) > 0);
            rpoplpushCommand(c);
        }
    }
}
