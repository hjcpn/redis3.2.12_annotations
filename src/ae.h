/* A simple event-driven programming library. Originally I wrote this code
 * for the Jim's event-loop (Jim is a Tcl interpreter) but later translated
 * it in form of a library for easy reuse.
 *
 * Copyright (c) 2006-2012, Salvatore Sanfilippo <antirez at gmail dot com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Redis nor the names of its contributors may be used
 *     to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __AE_H__
#define __AE_H__

#include <time.h>

#define AE_OK 0
#define AE_ERR -1

//事件类型
#define AE_NONE 0       /* No events registered. */
#define AE_READABLE 1   /* Fire when descriptor is readable. */
#define AE_WRITABLE 2   /* Fire when descriptor is writable. */
#define AE_BARRIER 4    /* With WRITABLE, never fire the event if the
                           READABLE event already fired in the same event
                           loop iteration. Useful when you want to persist
                           things to disk before sending replies, and want
                           to do that in a group fashion. */

#define AE_FILE_EVENTS 1
#define AE_TIME_EVENTS 2
#define AE_ALL_EVENTS (AE_FILE_EVENTS|AE_TIME_EVENTS)
#define AE_DONT_WAIT 4

#define AE_NOMORE -1
#define AE_DELETED_EVENT_ID -1

/* Macros */
#define AE_NOTUSED(V) ((void) V)

struct aeEventLoop;

/* Types and data structures */
//文件事件处理函数
typedef void aeFileProc(struct aeEventLoop *eventLoop, int fd, void *clientData, int mask);

//时间事件处理函数
typedef int aeTimeProc(struct aeEventLoop *eventLoop, long long id, void *clientData);

typedef void aeEventFinalizerProc(struct aeEventLoop *eventLoop, void *clientData);

typedef void aeBeforeSleepProc(struct aeEventLoop *eventLoop);

/* File event structure */
//文件事件
typedef struct aeFileEvent {
    //事件类型
    int mask; /* one of AE_(READABLE|WRITABLE|BARRIER) */

    //处理函数
    aeFileProc *rfileProc;
    aeFileProc *wfileProc;

    //clientData一般为每个连接对应的Client结构体
    void *clientData;
} aeFileEvent;

/* Time event structure */
//时间事件
typedef struct aeTimeEvent {
    long long id; /* time event identifier. */

    //发生时间
    long when_sec; /* seconds */
    long when_ms; /* milliseconds */

    //处理函数
    aeTimeProc *timeProc;

    aeEventFinalizerProc *finalizerProc;
    void *clientData;

    //时间事件链表上的下一个事件
    //链表是无序的，指向的下一个事件不一定是最近将要发生的
    struct aeTimeEvent *next;
} aeTimeEvent;

/* A fired event */
//已发生事件
typedef struct aeFiredEvent {
    int fd;
    int mask;
} aeFiredEvent;

/* State of an event based program */
//事件循环结构体
typedef struct aeEventLoop {
    //当前注册的最大描述符
    int maxfd;   /* highest file descriptor currently registered */

    //可注册文件描述符数量
    //也是可注册的最大描述符大小
    int setsize; /* max number of file descriptors tracked */

    //时间事件的下一个可用id
    long long timeEventNextId;

    time_t lastTime;     /* Used to detect system clock skew */

    //文件事件数组(socket，普通文件， ...)
    //数组保存了描述符和其相应事件处理函数等数据的对应关系
    //注册的描述符fd即是数组索引
    aeFileEvent *events; /* Registered events */

    //已发生事件数组
    //apidata将监听到的已发生事件放入该数组中
    //事件循环从这里获取这些事件
    aeFiredEvent *fired; /* Fired events */
    aeTimeEvent *timeEventHead;

    //事件循环停止标志
    int stop;

    //多路复用io封装
    void *apidata; /* This is used for polling API specific data */

    //每次阻塞等待事件之前执行该函数
    aeBeforeSleepProc *beforesleep;
} aeEventLoop;

/* Prototypes */
//创建新的事件循环结构
aeEventLoop *aeCreateEventLoop(int setsize);

//释放事件循环
void aeDeleteEventLoop(aeEventLoop *eventLoop);

//置停止标志为true
void aeStop(aeEventLoop *eventLoop);

//注册文件事件
int aeCreateFileEvent(aeEventLoop *eventLoop, int fd, int mask,
                      aeFileProc *proc, void *clientData);

//删除已注册事件
void aeDeleteFileEvent(aeEventLoop *eventLoop, int fd, int mask);

//获取fd对应的事件类型
int aeGetFileEvents(aeEventLoop *eventLoop, int fd);

//创建时间事件
long long aeCreateTimeEvent(aeEventLoop *eventLoop, long long milliseconds,
                            aeTimeProc *proc, void *clientData,
                            aeEventFinalizerProc *finalizerProc);

//从链表删除时间事件
int aeDeleteTimeEvent(aeEventLoop *eventLoop, long long id);

//等待并处理发生的文件和时间事件
int aeProcessEvents(aeEventLoop *eventLoop, int flags);

int aeWait(int fd, int mask, long long milliseconds);

//事件循环主体
void aeMain(aeEventLoop *eventLoop);

//io多路复用库
char *aeGetApiName(void);

void aeSetBeforeSleepProc(aeEventLoop *eventLoop, aeBeforeSleepProc *beforesleep);

//设置钩子函数
int aeGetSetSize(aeEventLoop *eventLoop);

//调整可注册文件描述符数量
int aeResizeSetSize(aeEventLoop *eventLoop, int setsize);

#endif
