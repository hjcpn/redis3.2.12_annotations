/* Hash Tables Implementation.
 *
 * This file implements in-memory hash tables with insert/del/replace/find/
 * get-random-element operations. Hash tables will auto-resize if needed
 * tables of power of two in size are used, collisions are handled by
 * chaining. See the source code for more information... :)
 *
 * Copyright (c) 2006-2012, Salvatore Sanfilippo <antirez at gmail dot com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Redis nor the names of its contributors may be used
 *     to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdint.h>

#ifndef __DICT_H
#define __DICT_H

#define DICT_OK 0
#define DICT_ERR 1

/* Unused arguments generate annoying warnings... */
#define DICT_NOTUSED(V) ((void) V)

//字典元素结构体
typedef struct dictEntry {
    void *key;
    union {
        void *val;
        uint64_t u64;
        int64_t s64;
        double d;
    } v;
    //当前bucket的下一个元素
    struct dictEntry *next;
} dictEntry;

//特定类型字典的相关函数
typedef struct dictType {
    //key哈希函数
    unsigned int (*hashFunction)(const void *key);

    //key拷贝函数
    void *(*keyDup)(void *privdata, const void *key);

    //val拷贝函数
    void *(*valDup)(void *privdata, const void *obj);

    //key比较函数
    int (*keyCompare)(void *privdata, const void *key1, const void *key2);

    //key释放函数
    void (*keyDestructor)(void *privdata, void *key);

    //val释放函数
    void (*valDestructor)(void *privdata, void *obj);
} dictType;

/* This is our hash table structure. Every dictionary has two of this as we
 * implement incremental rehashing, for the old to the new table. */
//字典内部哈希表
typedef struct dictht {
    dictEntry **table;

    //数组大小
    unsigned long size;

    //用于计算索引，等于size - 1
    unsigned long sizemask;

    //元素个数
    unsigned long used;
} dictht;

//字典
typedef struct dict {
    //字典类型
    dictType *type;
    void *privdata;

    //两个哈希表用于重哈希
    dictht ht[2];

    //重哈希进行到哪个位置？
    long rehashidx; /* rehashing not in progress if rehashidx == -1 */

    //当前字典迭代器
    int iterators; /* number of iterators currently running */
} dict;

/* If safe is set to 1 this is a safe iterator, that means, you can call
 * dictAdd, dictFind, and other functions against the dictionary even while
 * iterating. Otherwise it is a non safe iterator, and only dictNext()
 * should be called while iterating. */
//字典迭代器
typedef struct dictIterator {
    //当前迭代字典
    dict *d;

    //当前迭代的bucket索引
    long index;

    int table, safe;

    //当前迭代的元素
    dictEntry *entry, *nextEntry;

    /* unsafe iterator fingerprint for misuse detection. */
    long long fingerprint;
} dictIterator;

typedef void (dictScanFunction)(void *privdata, const dictEntry *de);

/* This is the initial size of every hash table */
#define DICT_HT_INITIAL_SIZE     4

/* ------------------------------- Macros ------------------------------------*/

//释放val
#define dictFreeVal(d, entry) \
    if ((d)->type->valDestructor) \
        (d)->type->valDestructor((d)->privdata, (entry)->v.val)

//设置v.val
#define dictSetVal(d, entry, _val_) do { \
    if ((d)->type->valDup) \
        entry->v.val = (d)->type->valDup((d)->privdata, _val_); \
    else \
        entry->v.val = (_val_); \
} while(0)

//设置v.s64
#define dictSetSignedIntegerVal(entry, _val_) \
    do { entry->v.s64 = _val_; } while(0)

//设置v.u64
#define dictSetUnsignedIntegerVal(entry, _val_) \
    do { entry->v.u64 = _val_; } while(0)

//设置v.d
#define dictSetDoubleVal(entry, _val_) \
    do { entry->v.d = _val_; } while(0)

#define dictFreeKey(d, entry) \
    if ((d)->type->keyDestructor) \
        (d)->type->keyDestructor((d)->privdata, (entry)->key)

//设置key
#define dictSetKey(d, entry, _key_) do { \
    if ((d)->type->keyDup) \
        entry->key = (d)->type->keyDup((d)->privdata, _key_); \
    else \
        entry->key = (_key_); \
} while(0)


//比较两个key是否相等
#define dictCompareKeys(d, key1, key2) \
    (((d)->type->keyCompare) ? \
        (d)->type->keyCompare((d)->privdata, key1, key2) : \
        (key1) == (key2))

//计算key的哈希值
#define dictHashKey(d, key) (d)->type->hashFunction(key)

//获取key
#define dictGetKey(he) ((he)->key)

//获取v.val
#define dictGetVal(he) ((he)->v.val)

//获取v.s64
#define dictGetSignedIntegerVal(he) ((he)->v.s64)

//获取v.u64
#define dictGetUnsignedIntegerVal(he) ((he)->v.u64)

//获取v.d
#define dictGetDoubleVal(he) ((he)->v.d)

//字典总共的bucket，包括了两个哈希表
#define dictSlots(d) ((d)->ht[0].size+(d)->ht[1].size)

//返回字典元素个数
#define dictSize(d) ((d)->ht[0].used+(d)->ht[1].used)

//当前是否在重哈希？
#define dictIsRehashing(d) ((d)->rehashidx != -1)

/* API */
//创建新字典
dict *dictCreate(dictType *type, void *privDataPtr);

//重哈希字典，可能是变小也可能是变大
int dictExpand(dict *d, unsigned long size);

//向字典添加新的元素
int dictAdd(dict *d, void *key, void *val);

//向字典添加新元素
//只设置key，不设置val
dictEntry *dictAddRaw(dict *d, void *key);

//添加或更新元素val
int dictReplace(dict *d, void *key, void *val);

//尝试添加新元素，如果元素已经存在，则返回旧元素
dictEntry *dictReplaceRaw(dict *d, void *key);

//删除元素，并释放key/val
int dictDelete(dict *d, const void *key);

//删除元素，不释放key/val
int dictDeleteNoFree(dict *d, const void *key);

//释放哈希表
void dictRelease(dict *d);

//获取key元素
dictEntry *dictFind(dict *d, const void *key);

void *dictFetchValue(dict *d, const void *key);

//获取key元素的val
int dictResize(dict *d);

//创建当前字典的迭代器
dictIterator *dictGetIterator(dict *d);

//创建当前字典的安全迭代器
dictIterator *dictGetSafeIterator(dict *d);

//返回迭代器的下一个元素
dictEntry *dictNext(dictIterator *iter);

//释放迭代器
void dictReleaseIterator(dictIterator *iter);

//随机返回一个元素
dictEntry *dictGetRandomKey(dict *d);

// 从dict中随机返回count个元素
// 这个函数无法保证获取的元素是不重复的
// 也无法保证一定能返回count个元素
unsigned int dictGetSomeKeys(dict *d, dictEntry **des, unsigned int count);

void dictGetStats(char *buf, size_t bufsize, dict *d);

//murmurhash2哈希算法
unsigned int dictGenHashFunction(const void *key, int len);

//djb哈希算法
unsigned int dictGenCaseHashFunction(const unsigned char *buf, int len);

//清空字典
//只是释放了哈希表，没有释放字典
void dictEmpty(dict *d, void(callback)(void *));

//允许重哈希
void dictEnableResize(void);

//禁止重哈希
void dictDisableResize(void);

//进行n步重哈希
//"步"指不为空的bucket
int dictRehash(dict *d, int n);

//在ms时间内进行步长为100的重哈希
int dictRehashMilliseconds(dict *d, int ms);

void dictSetHashFunctionSeed(unsigned int initval);

unsigned int dictGetHashFunctionSeed(void);

unsigned long dictScan(dict *d, unsigned long v, dictScanFunction *fn, void *privdata);

/* Hash table types */
extern dictType dictTypeHeapStringCopyKey;
extern dictType dictTypeHeapStrings;
extern dictType dictTypeHeapStringCopyKeyValue;

#endif /* __DICT_H */
