/*
 * Copyright (c) 2009-2012, Salvatore Sanfilippo <antirez at gmail dot com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Redis nor the names of its contributors may be used
 *     to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "server.h"

/* ================================ MULTI/EXEC ============================== */

/* Client state initialization for MULTI/EXEC */
//重置事务状态
void initClientMultiState(client *c) {
    c->mstate.commands = NULL;
    c->mstate.count = 0;
}

/* Release all the resources associated with MULTI/EXEC state */
//释放事务状态结构体中的命令队列
void freeClientMultiState(client *c) {
    int j;

    //遍历事务命令，释放(减少引用计数)每个命令的参数对象/数组
    for (j = 0; j < c->mstate.count; j++) {
        int i;
        multiCmd *mc = c->mstate.commands + j;

        //减少命令参数对象的引用计数
        for (i = 0; i < mc->argc; i++)
            decrRefCount(mc->argv[i]);
        //释放命令参数数组
        zfree(mc->argv);
    }

    //释放命令队列
    zfree(c->mstate.commands);
}

/* Add a new command into the MULTI commands queue */
//新命令加入事务命令队列
void queueMultiCommand(client *c) {
    multiCmd *mc;
    int j;

    //为新命令分配内存
    c->mstate.commands = zrealloc(c->mstate.commands, sizeof(multiCmd) * (c->mstate.count + 1));

    //指向命令队列队尾
    mc = c->mstate.commands + c->mstate.count;

    //更新当前事务命令相关属性
    mc->cmd = c->cmd;
    mc->argc = c->argc;
    mc->argv = zmalloc(sizeof(robj *) * c->argc);
    memcpy(mc->argv, c->argv, sizeof(robj *) * c->argc);
    for (j = 0; j < c->argc; j++)
        incrRefCount(mc->argv[j]);

    //更新事务命令数
    c->mstate.count++;
}

//终止事务，释放/重置相关属性/状态
void discardTransaction(client *c) {
    //释放事务状态结构体中的命令队列
    freeClientMultiState(c);
    //重置事务状态
    initClientMultiState(c);
    //重置客户端状态
    c->flags &= ~(CLIENT_MULTI | CLIENT_DIRTY_CAS | CLIENT_DIRTY_EXEC);
    unwatchAllKeys(c);
}

/* Flag the transacation as DIRTY_EXEC so that EXEC will fail.
 * Should be called every time there is an error while queueing a command. */
//新命令入队时发生错误
//比如命令参数错误
// 比如命令需要大量内存，但是当前没有足够内存
//比如aof/rdb持久化发生错误
//具体见processCommand函数
void flagTransaction(client *c) {
    //更新事务状态
    if (c->flags & CLIENT_MULTI) {
        c->flags |= CLIENT_DIRTY_EXEC;
    }
}

//命令实现
//MULTI
void multiCommand(client *c) {
    //客户端已经处于事务状态
    if (c->flags & CLIENT_MULTI) {
        addReplyError(c, "MULTI calls can not be nested");
        return;
    }
    //更新状态
    c->flags |= CLIENT_MULTI;
    addReply(c, shared.ok);
}

//命令实现
//discard
void discardCommand(client *c) {
    //当前没有处于事务状态
    if (!(c->flags & CLIENT_MULTI)) {
        addReplyError(c, "DISCARD without MULTI");
        return;
    }
    //取消事务
    discardTransaction(c);
    addReply(c, shared.ok);
}

/* Send a MULTI command to all the slaves and AOF file. Check the execCommand
 * implementation for more information. */
//传播multi命令到aof/从服务器
void execCommandPropagateMulti(client *c) {
    robj *multistring = createStringObject("MULTI", 5);

    propagate(server.multiCommand, c->db->id, &multistring, 1,
              PROPAGATE_AOF | PROPAGATE_REPL);
    decrRefCount(multistring);
}

//命令实现
//exec
void execCommand(client *c) {
    int j;
    robj **orig_argv;
    int orig_argc;
    struct redisCommand *orig_cmd;
    int must_propagate = 0; /* Need to propagate MULTI/EXEC to AOF / slaves? */

    //客户端没有处于事务状态
    if (!(c->flags & CLIENT_MULTI)) {
        addReplyError(c, "EXEC without MULTI");
        return;
    }

    /* Check if we need to abort the EXEC because:
     * 1) Some WATCHed key was touched.
     * 2) There was a previous error while queueing commands.
     * A failed EXEC in the first case returns a multi bulk nil object
     * (technically it is not an error but a special behavior), while
     * in the second an EXECABORT error is returned. */
    //watch的key发生改变，或者命令入队的时候发生错误
    if (c->flags & (CLIENT_DIRTY_CAS | CLIENT_DIRTY_EXEC)) {
        addReply(c, c->flags & CLIENT_DIRTY_EXEC ? shared.execaborterr : shared.nullmultibulk);
        //终止事务
        discardTransaction(c);
        goto handle_monitor;
    }

    /* Exec all the queued commands */
    //不需要watch key了
    unwatchAllKeys(c); /* Unwatch ASAP otherwise we'll waste CPU cycles */

    //保存执行事务之前客户端的命令参数个数，命令参数数组，命令
    orig_argv = c->argv;
    orig_argc = c->argc;
    orig_cmd = c->cmd;

    //添加当前事务命令个数到回复缓冲
    addReplyMultiBulkLen(c, c->mstate.count);

    //遍历事务命令队列，执行每一条命令
    for (j = 0; j < c->mstate.count; j++) {
        c->argc = c->mstate.commands[j].argc;
        c->argv = c->mstate.commands[j].argv;
        c->cmd = c->mstate.commands[j].cmd;

        /* Propagate a MULTI request once we encounter the first write op.
         * This way we'll deliver the MULTI/..../EXEC block as a whole and
         * both the AOF and the replication link will have the same consistency
         * and atomicity guarantees. */
        //遇到第一条写命令时，传播multi到从服务器/aof
        if (!must_propagate && !(c->cmd->flags & CMD_READONLY)) {
            execCommandPropagateMulti(c);
            must_propagate = 1;
        }

        //执行命令
        call(c, CMD_CALL_FULL);

        /* Commands may alter argc/argv, restore mstate. */
        c->mstate.commands[j].argc = c->argc;
        c->mstate.commands[j].argv = c->argv;
        c->mstate.commands[j].cmd = c->cmd;
    }

    //恢复执行事务之前的客户端状态
    c->argv = orig_argv;
    c->argc = orig_argc;
    c->cmd = orig_cmd;

    //终止事务，释放/重置相关属性/状态
    discardTransaction(c);

    /* Make sure the EXEC command will be propagated as well if MULTI
     * was already propagated. */
    if (must_propagate) {
        server.dirty++;
    }

    handle_monitor:
    /* Send EXEC to clients waiting data from MONITOR. We do it here
     * since the natural order of commands execution is actually:
     * MUTLI, EXEC, ... commands inside transaction ...
     * Instead EXEC is flagged as CMD_SKIP_MONITOR in the command
     * table, and we do it here with correct ordering. */
    if (listLength(server.monitors) && !server.loading) {
        replicationFeedMonitors(c, server.monitors, c->db->id, c->argv, c->argc);
    }
}

/* ===================== WATCH (CAS alike for MULTI/EXEC) ===================
 *
 * The implementation uses a per-DB hash table mapping keys to list of clients
 * WATCHing those keys, so that given a key that is going to be modified
 * we can mark all the associated clients as dirty.
 *
 * Also every client contains a list of WATCHed keys so that's possible to
 * un-watch such keys when the client is freed or when UNWATCH is called. */

/* In the client->watched_keys list we need to use watchedKey structures
 * as in order to identify a key in Redis we need both the key name and the
 * DB */
//客户端watch链表的节点
//需要db和key的名字来唯一地标识一个watched key
typedef struct watchedKey {
    robj *key;
    //key所在数据库
    redisDb *db;
} watchedKey;

/* Watch for the specified key */
//watch命令底层实现
void watchForKey(client *c, robj *key) {
    list *clients = NULL;
    listIter li;
    listNode *ln;
    watchedKey *wk;

    /* Check if we are already watching for this key */
    //遍历客户端watch链表，检查key是否已经watch？
    listRewind(c->watched_keys, &li);
    while ((ln = listNext(&li))) {
        wk = listNodeValue(ln);
        if (wk->db == c->db && equalStringObjects(key, wk->key)) {
            return;
        } /* Key already watched */
    }

    /* This key is not already watched in this DB. Let's add it */
    //添加到全局watch字典
    clients = dictFetchValue(c->db->watched_keys, key);
    //该key还没有其他客户端watch，添加新的键(key)值(客户端链表)对到全局watch字典
    if (!clients) {
        clients = listCreate();
        dictAdd(c->db->watched_keys, key, clients);
        incrRefCount(key);
    }
    //添加当前客户端到当前key的watch链表
    listAddNodeTail(clients, c);

    /* Add the new key to the list of keys watched by this client */
    //添加到当前客户端watch链表
    //需要db变量，因为客户端可以watch不同数据库的同名key
    wk = zmalloc(sizeof(*wk));
    wk->key = key;
    wk->db = c->db;
    incrRefCount(key);
    listAddNodeTail(c->watched_keys, wk);
}

/* Unwatch all the keys watched by this client. To clean the EXEC dirty
 * flag is up to the caller. */
//顾名思义，unwatch客户端watch的所有key
void unwatchAllKeys(client *c) {
    listIter li;
    listNode *ln;

    //客户端没有watch任何key
    if (listLength(c->watched_keys) == 0) {
        return;
    }

    //遍历unwatch该客户端的watch链表
    listRewind(c->watched_keys, &li);
    while ((ln = listNext(&li))) {
        list *clients;
        watchedKey *wk;

        /* Lookup the watched key -> clients list and remove the client
         * from the list */
        wk = listNodeValue(ln);

        //全局watch字典的当前watched key - 客户端链表
        clients = dictFetchValue(wk->db->watched_keys, wk->key);
        serverAssertWithInfo(c, NULL, clients != NULL);
        //将客户端从全局watch字典对应的key链表删除
        listDelNode(clients, listSearchKey(clients, c));

        /* Kill the entry at all if this was the only client */
        //没有客户端在watch这个key了，从全局watch字典删除该键值对
        if (listLength(clients) == 0) {
            dictDelete(wk->db->watched_keys, wk->key);
        }

        /* Remove this watched key from the client->watched list */
        //将key从客户端watch链表删除
        listDelNode(c->watched_keys, ln);
        decrRefCount(wk->key);
        zfree(wk);
    }
}

/* "Touch" a key, so that if this key is being WATCHed by some client the
 * next EXEC will fail. */
//key已被修改，更新所有watch该key的客户端的事务状态
void touchWatchedKey(redisDb *db, robj *key) {
    list *clients;
    listIter li;
    listNode *ln;

    //没有任何客户端在watch任何key
    if (dictSize(db->watched_keys) == 0) {
        return;
    }

    //watch该key的客户端链表
    clients = dictFetchValue(db->watched_keys, key);
    //没有客户端在watch该key
    if (!clients) {
        return;
    }

    /* Mark all the clients watching this key as CLIENT_DIRTY_CAS */
    /* Check if we are already watching for this key */
    //遍历客户端链表，修改客户端事务状态为dirty
    listRewind(clients, &li);
    while ((ln = listNext(&li))) {
        client *c = listNodeValue(ln);
        c->flags |= CLIENT_DIRTY_CAS;
    }
}

/* On FLUSHDB or FLUSHALL all the watched keys that are present before the
 * flush but will be deleted as effect of the flushing operation should
 * be touched. "dbid" is the DB that's getting the flush. -1 if it is
 * a FLUSHALL operation (all the DBs flushed). */
//更新flush命令影响的客户端的事务状态
void touchWatchedKeysOnFlush(int dbid) {
    listIter li1, li2;
    listNode *ln;

    /* For every client, check all the waited keys */
    //遍历所有客户端的watch链表，检查是否有watch被flush的数据库的key
    listRewind(server.clients, &li1);
    while ((ln = listNext(&li1))) {
        client *c = listNodeValue(ln);
        //当前客户端的watch链表
        listRewind(c->watched_keys, &li2);

        //遍历当前客户端的watch链表的key
        while ((ln = listNext(&li2))) {
            watchedKey *wk = listNodeValue(ln);

            /* For every watched key matching the specified DB, if the
             * key exists, mark the client as dirty, as the key will be
             * removed. */
            //当前key被flush了，更新当前客户端事务状态
            if (dbid == -1 || wk->db->id == dbid) {
                if (dictFind(wk->db->dict, wk->key->ptr) != NULL) {
                    c->flags |= CLIENT_DIRTY_CAS;
                }
            }
        }
    }
}

//命令实现
//WATCH key [key ...]
void watchCommand(client *c) {
    int j;

    //必须在进入事务状态之前watch
    if (c->flags & CLIENT_MULTI) {
        addReplyError(c, "WATCH inside MULTI is not allowed");
        return;
    }

    //添加到客户端watch链表和服务器watch字典
    for (j = 1; j < c->argc; j++)
        watchForKey(c, c->argv[j]);

    addReply(c, shared.ok);
}

//命令实现
//unwatch
void unwatchCommand(client *c) {
    unwatchAllKeys(c);
    c->flags &= (~CLIENT_DIRTY_CAS);
    addReply(c, shared.ok);
}
