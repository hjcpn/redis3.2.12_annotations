/*
 * Copyright (c) 2009-2012, Pieter Noordhuis <pcnoordhuis at gmail dot com>
 * Copyright (c) 2009-2012, Salvatore Sanfilippo <antirez at gmail dot com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Redis nor the names of its contributors may be used
 *     to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "intset.h"
#include "zmalloc.h"
#include "endianconv.h"

/* Note that these encodings are ordered, so:
 * INTSET_ENC_INT16 < INTSET_ENC_INT32 < INTSET_ENC_INT64. */
//集合编码类型
#define INTSET_ENC_INT16 (sizeof(int16_t))
#define INTSET_ENC_INT32 (sizeof(int32_t))
#define INTSET_ENC_INT64 (sizeof(int64_t))

/* Return the required encoding for the provided value. */
//判断保存v所需的编码
static uint8_t _intsetValueEncoding(int64_t v) {
    if (v < INT32_MIN || v > INT32_MAX) {
        return INTSET_ENC_INT64;
    }
    else if (v < INT16_MIN || v > INT16_MAX) {
        return INTSET_ENC_INT32;
    }
    else {
        return INTSET_ENC_INT16;
    }
}

/* Return the value at pos, given an encoding. */
//获取索引pos处的value
static int64_t _intsetGetEncoded(intset *is, int pos, uint8_t enc) {
    int64_t v64;
    int32_t v32;
    int16_t v16;

    if (enc == INTSET_ENC_INT64) {
        memcpy(&v64, ((int64_t *) is->contents) + pos, sizeof(v64));
        memrev64ifbe(&v64);
        return v64;
    }
    else if (enc == INTSET_ENC_INT32) {
        memcpy(&v32, ((int32_t *) is->contents) + pos, sizeof(v32));
        memrev32ifbe(&v32);
        return v32;
    }
    else {
        memcpy(&v16, ((int16_t *) is->contents) + pos, sizeof(v16));
        memrev16ifbe(&v16);
        return v16;
    }
}

/* Return the value at pos, using the configured encoding. */
//获取索引pos处的value
static int64_t _intsetGet(intset *is, int pos) {
    return _intsetGetEncoded(is, pos, intrev32ifbe(is->encoding));
}

/* Set the value at pos, using the configured encoding. */
//将value写入索引pos处
static void _intsetSet(intset *is, int pos, int64_t value) {
    //获取集合编码类型
    uint32_t encoding = intrev32ifbe(is->encoding);

    //写入value
    if (encoding == INTSET_ENC_INT64) {
        ((int64_t *) is->contents)[pos] = value;
        memrev64ifbe(((int64_t *) is->contents) + pos);
    }
    else if (encoding == INTSET_ENC_INT32) {
        ((int32_t *) is->contents)[pos] = value;
        memrev32ifbe(((int32_t *) is->contents) + pos);
    }
    else {
        ((int16_t *) is->contents)[pos] = value;
        memrev16ifbe(((int16_t *) is->contents) + pos);
    }
}

/* Create an empty intset. */
//创建新的整数集合
intset *intsetNew(void) {
    intset *is = zmalloc(sizeof(intset));
    is->encoding = intrev32ifbe(INTSET_ENC_INT16);
    is->length = 0;
    return is;
}

/* Resize the intset */
//调整集合(数组)大小
static intset *intsetResize(intset *is, uint32_t len) {
    uint32_t size = len * intrev32ifbe(is->encoding);
    is = zrealloc(is, sizeof(intset) + size);
    return is;
}

/* Search for the position of "value". Return 1 when the value was found and
 * sets "pos" to the position of the value within the intset. Return 0 when
 * the value is not present in the intset and sets "pos" to the position
 * where "value" can be inserted. */
//在集合中二分搜索value
// 如果找到，pos保存value位置，返回1
//否则pos保存value应该插入的位置，返回0
static uint8_t intsetSearch(intset *is, int64_t value, uint32_t *pos) {
    int min = 0, max = intrev32ifbe(is->length) - 1, mid = -1;
    int64_t cur = -1;

    /* The value can never be found when the set is empty */
    //如果数组为空，插入位置为0
    if (intrev32ifbe(is->length) == 0) {
        if (pos) {
            *pos = 0;
        }
        return 0;
    }
    else {
        /* Check for the case where we know we cannot find the value,
         * but do know the insert position. */
        //如果value大于最后一个元素或小于第一个元素，分别插入尾部/头部
        if (value > _intsetGet(is, intrev32ifbe(is->length) - 1)) {
            if (pos) {
                *pos = intrev32ifbe(is->length);
            }
            return 0;
        }
        else if (value < _intsetGet(is, 0)) {
            if (pos) { *pos = 0; }
            return 0;
        }
    }

    //否则二分搜索
    while (max >= min) {
        mid = ((unsigned int) min + (unsigned int) max) >> 1;
        cur = _intsetGet(is, mid);
        if (value > cur) {
            min = mid + 1;
        }
        else if (value < cur) {
            max = mid - 1;
        }
        else {
            break;
        }
    }

    //找到value，pos保存value位置
    if (value == cur) {
        if (pos) {
            *pos = mid;
        }
        return 1;
    }
    else {
        //value不存在，pos保存value应该插入的位置
        if (pos) {
            *pos = min;
        }
        return 0;
    }
}

/* Upgrades the intset to a larger encoding and inserts the given integer. */
//升级现有编码以添加value
static intset *intsetUpgradeAndAdd(intset *is, int64_t value) {
    //获取新老编码类型
    uint8_t curenc = intrev32ifbe(is->encoding);
    uint8_t newenc = _intsetValueEncoding(value);

    int length = intrev32ifbe(is->length);

    //value小于0则头部插入，否则尾部插入
    int prepend = value < 0 ? 1 : 0;

    /* First set new encoding and resize */
    //更新编码类型并realloc集合
    is->encoding = intrev32ifbe(newenc);
    is = intsetResize(is, intrev32ifbe(is->length) + 1);

    /* Upgrade back-to-front so we don't overwrite values.
     * Note that the "prepend" variable is used to make sure we have an empty
     * space at either the beginning or the end of the intset. */
    //遍历并以新编码类型重写现有元素
    //从尾到头重写，避免覆盖元素
    while (length--)
        _intsetSet(is, length + prepend, _intsetGetEncoded(is, length, curenc));

    /* Set the value at the beginning or the end. */
    //插入新元素
    if (prepend) {
        _intsetSet(is, 0, value);
    }
    else {
        _intsetSet(is, intrev32ifbe(is->length), value);
    }

    //更新长度
    is->length = intrev32ifbe(intrev32ifbe(is->length) + 1);

    return is;
}

//前移/后移元素，从from到to
static void intsetMoveTail(intset *is, uint32_t from, uint32_t to) {
    void *src, *dst;

    //这里的bytes表示需要移动的元素个数
    uint32_t bytes = intrev32ifbe(is->length) - from;

    uint32_t encoding = intrev32ifbe(is->encoding);

    //计算起始/目标内存位置及需要移动的字节数
    if (encoding == INTSET_ENC_INT64) {
        //起始内存位置
        src = (int64_t *) is->contents + from;
        //目标内存位置
        dst = (int64_t *) is->contents + to;
        //这里的bytes才表示需要移动的字节数
        bytes *= sizeof(int64_t);
    }
    else if (encoding == INTSET_ENC_INT32) {
        src = (int32_t *) is->contents + from;
        dst = (int32_t *) is->contents + to;
        bytes *= sizeof(int32_t);
    }
    else {
        src = (int16_t *) is->contents + from;
        dst = (int16_t *) is->contents + to;
        bytes *= sizeof(int16_t);
    }

    //移动内存
    memmove(dst, src, bytes);
}

/* Insert an integer in the intset */
//往集合中添加新元素
//每次添加都需要realloc cur_size + 1的空间，不会预分配更大的空间
intset *intsetAdd(intset *is, int64_t value, uint8_t *success) {
    //判断保存v所需的编码
    uint8_t valenc = _intsetValueEncoding(value);

    uint32_t pos;

    if (success) {
        *success = 1;
    }

    /* Upgrade encoding if necessary. If we need to upgrade, we know that
     * this value should be either appended (if > 0) or prepended (if < 0),
     * because it lies outside the range of existing values. */
    //如果新元素超过现有编码类型所能表示的范围，需要更改编码类型
    if (valenc > intrev32ifbe(is->encoding)) {
        /* This always succeeds, so we don't need to curry *success. */
        return intsetUpgradeAndAdd(is, value);
    }
    else {
        /* Abort if the value is already present in the set.
         * This call will populate "pos" with the right position to insert
         * the value when it cannot be found. */
        //否则检查元素是否已经存在
        if (intsetSearch(is, value, &pos)) {
            if (success) {
                *success = 0;
            }
            return is;
        }

        //到这里说明不需要升级编码，元素也不存在
        //为集合再分配+1个元素的内存
        is = intsetResize(is, intrev32ifbe(is->length) + 1);
        //如果不是在尾部插入，需要移动插入位置后面的元素
        if (pos < intrev32ifbe(is->length)) {
            intsetMoveTail(is, pos, pos + 1);
        }
    }

    //写入新元素到指定索引
    _intsetSet(is, pos, value);

    //更新元素个数
    is->length = intrev32ifbe(intrev32ifbe(is->length) + 1);

    return is;
}

/* Delete integer from intset */
//从集合中删除value
//每次删除都要realloc，不会保留多余的空闲空间
intset *intsetRemove(intset *is, int64_t value, int *success) {
    //获取编码类型
    uint8_t valenc = _intsetValueEncoding(value);

    uint32_t pos;

    if (success) {
        *success = 0;
    }

    //元素编码不会大于集合的编码&&二分搜索找到该元素
    if (valenc <= intrev32ifbe(is->encoding) && intsetSearch(is, value, &pos)) {
        uint32_t len = intrev32ifbe(is->length);

        /* We know we can delete */
        if (success) {
            *success = 1;
        }

        /* Overwrite value with tail and update length */
        //如果不是表尾删除，需要前移删除元素后面的元素
        if (pos < (len - 1)) {
            intsetMoveTail(is, pos + 1, pos);
        }

        //调整集合数组大小
        is = intsetResize(is, len - 1);
        //更新长度
        is->length = intrev32ifbe(len - 1);
    }

    return is;
}

/* Determine whether a value belongs to this set */
//检查元素是否存在于集合中
uint8_t intsetFind(intset *is, int64_t value) {
    uint8_t valenc = _intsetValueEncoding(value);
    return valenc <= intrev32ifbe(is->encoding) && intsetSearch(is, value, NULL);
}

/* Return random member */
//随机返回一个元素
int64_t intsetRandom(intset *is) {
    return _intsetGet(is, rand() % intrev32ifbe(is->length));
}

/* Sets the value to the value at the given position. When this position is
 * out of range the function returns 0, when in range it returns 1. */
//获取索引pos处的值，写入value
uint8_t intsetGet(intset *is, uint32_t pos, int64_t *value) {
    //检查pos是否合法
    if (pos < intrev32ifbe(is->length)) {
        *value = _intsetGet(is, pos);
        return 1;
    }
    return 0;
}

/* Return intset length */
//返回集合元素个数
uint32_t intsetLen(intset *is) {
    return intrev32ifbe(is->length);
}

/* Return intset blob size in bytes. */
//返回集合使用内存大小
//等于集合结构体大小+所有元素大小
size_t intsetBlobLen(intset *is) {
    return sizeof(intset) + intrev32ifbe(is->length) * intrev32ifbe(is->encoding);
}

#ifdef REDIS_TEST
#include <sys/time.h>
#include <time.h>

#if 0
static void intsetRepr(intset *is) {
    for (uint32_t i = 0; i < intrev32ifbe(is->length); i++) {
        printf("%lld\n", (uint64_t)_intsetGet(is,i));
    }
    printf("\n");
}

static void error(char *err) {
    printf("%s\n", err);
    exit(1);
}
#endif

static void ok(void) {
    printf("OK\n");
}

static long long usec(void) {
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return (((long long)tv.tv_sec)*1000000)+tv.tv_usec;
}

#define assert(_e) ((_e)?(void)0:(_assert(#_e,__FILE__,__LINE__),exit(1)))
static void _assert(char *estr, char *file, int line) {
    printf("\n\n=== ASSERTION FAILED ===\n");
    printf("==> %s:%d '%s' is not true\n",file,line,estr);
}

static intset *createSet(int bits, int size) {
    uint64_t mask = (1<<bits)-1;
    uint64_t value;
    intset *is = intsetNew();

    for (int i = 0; i < size; i++) {
        if (bits > 32) {
            value = (rand()*rand()) & mask;
        } else {
            value = rand() & mask;
        }
        is = intsetAdd(is,value,NULL);
    }
    return is;
}

static void checkConsistency(intset *is) {
    for (uint32_t i = 0; i < (intrev32ifbe(is->length)-1); i++) {
        uint32_t encoding = intrev32ifbe(is->encoding);

        if (encoding == INTSET_ENC_INT16) {
            int16_t *i16 = (int16_t*)is->contents;
            assert(i16[i] < i16[i+1]);
        } else if (encoding == INTSET_ENC_INT32) {
            int32_t *i32 = (int32_t*)is->contents;
            assert(i32[i] < i32[i+1]);
        } else {
            int64_t *i64 = (int64_t*)is->contents;
            assert(i64[i] < i64[i+1]);
        }
    }
}

#define UNUSED(x) (void)(x)
int intsetTest(int argc, char **argv) {
    uint8_t success;
    int i;
    intset *is;
    srand(time(NULL));

    UNUSED(argc);
    UNUSED(argv);

    printf("Value encodings: "); {
        assert(_intsetValueEncoding(-32768) == INTSET_ENC_INT16);
        assert(_intsetValueEncoding(+32767) == INTSET_ENC_INT16);
        assert(_intsetValueEncoding(-32769) == INTSET_ENC_INT32);
        assert(_intsetValueEncoding(+32768) == INTSET_ENC_INT32);
        assert(_intsetValueEncoding(-2147483648) == INTSET_ENC_INT32);
        assert(_intsetValueEncoding(+2147483647) == INTSET_ENC_INT32);
        assert(_intsetValueEncoding(-2147483649) == INTSET_ENC_INT64);
        assert(_intsetValueEncoding(+2147483648) == INTSET_ENC_INT64);
        assert(_intsetValueEncoding(-9223372036854775808ull) ==
                    INTSET_ENC_INT64);
        assert(_intsetValueEncoding(+9223372036854775807ull) ==
                    INTSET_ENC_INT64);
        ok();
    }

    printf("Basic adding: "); {
        is = intsetNew();
        is = intsetAdd(is,5,&success); assert(success);
        is = intsetAdd(is,6,&success); assert(success);
        is = intsetAdd(is,4,&success); assert(success);
        is = intsetAdd(is,4,&success); assert(!success);
        ok();
    }

    printf("Large number of random adds: "); {
        uint32_t inserts = 0;
        is = intsetNew();
        for (i = 0; i < 1024; i++) {
            is = intsetAdd(is,rand()%0x800,&success);
            if (success) inserts++;
        }
        assert(intrev32ifbe(is->length) == inserts);
        checkConsistency(is);
        ok();
    }

    printf("Upgrade from int16 to int32: "); {
        is = intsetNew();
        is = intsetAdd(is,32,NULL);
        assert(intrev32ifbe(is->encoding) == INTSET_ENC_INT16);
        is = intsetAdd(is,65535,NULL);
        assert(intrev32ifbe(is->encoding) == INTSET_ENC_INT32);
        assert(intsetFind(is,32));
        assert(intsetFind(is,65535));
        checkConsistency(is);

        is = intsetNew();
        is = intsetAdd(is,32,NULL);
        assert(intrev32ifbe(is->encoding) == INTSET_ENC_INT16);
        is = intsetAdd(is,-65535,NULL);
        assert(intrev32ifbe(is->encoding) == INTSET_ENC_INT32);
        assert(intsetFind(is,32));
        assert(intsetFind(is,-65535));
        checkConsistency(is);
        ok();
    }

    printf("Upgrade from int16 to int64: "); {
        is = intsetNew();
        is = intsetAdd(is,32,NULL);
        assert(intrev32ifbe(is->encoding) == INTSET_ENC_INT16);
        is = intsetAdd(is,4294967295,NULL);
        assert(intrev32ifbe(is->encoding) == INTSET_ENC_INT64);
        assert(intsetFind(is,32));
        assert(intsetFind(is,4294967295));
        checkConsistency(is);

        is = intsetNew();
        is = intsetAdd(is,32,NULL);
        assert(intrev32ifbe(is->encoding) == INTSET_ENC_INT16);
        is = intsetAdd(is,-4294967295,NULL);
        assert(intrev32ifbe(is->encoding) == INTSET_ENC_INT64);
        assert(intsetFind(is,32));
        assert(intsetFind(is,-4294967295));
        checkConsistency(is);
        ok();
    }

    printf("Upgrade from int32 to int64: "); {
        is = intsetNew();
        is = intsetAdd(is,65535,NULL);
        assert(intrev32ifbe(is->encoding) == INTSET_ENC_INT32);
        is = intsetAdd(is,4294967295,NULL);
        assert(intrev32ifbe(is->encoding) == INTSET_ENC_INT64);
        assert(intsetFind(is,65535));
        assert(intsetFind(is,4294967295));
        checkConsistency(is);

        is = intsetNew();
        is = intsetAdd(is,65535,NULL);
        assert(intrev32ifbe(is->encoding) == INTSET_ENC_INT32);
        is = intsetAdd(is,-4294967295,NULL);
        assert(intrev32ifbe(is->encoding) == INTSET_ENC_INT64);
        assert(intsetFind(is,65535));
        assert(intsetFind(is,-4294967295));
        checkConsistency(is);
        ok();
    }

    printf("Stress lookups: "); {
        long num = 100000, size = 10000;
        int i, bits = 20;
        long long start;
        is = createSet(bits,size);
        checkConsistency(is);

        start = usec();
        for (i = 0; i < num; i++) intsetSearch(is,rand() % ((1<<bits)-1),NULL);
        printf("%ld lookups, %ld element set, %lldusec\n",
               num,size,usec()-start);
    }

    printf("Stress add+delete: "); {
        int i, v1, v2;
        is = intsetNew();
        for (i = 0; i < 0xffff; i++) {
            v1 = rand() % 0xfff;
            is = intsetAdd(is,v1,NULL);
            assert(intsetFind(is,v1));

            v2 = rand() % 0xfff;
            is = intsetRemove(is,v2,NULL);
            assert(!intsetFind(is,v2));
        }
        checkConsistency(is);
        ok();
    }

    return 0;
}
#endif
