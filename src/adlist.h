/* adlist.h - A generic doubly linked list implementation
 *
 * Copyright (c) 2006-2012, Salvatore Sanfilippo <antirez at gmail dot com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Redis nor the names of its contributors may be used
 *     to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __ADLIST_H__
#define __ADLIST_H__

/* Node, List, and Iterator are the only data structures used currently. */

//链表节点
typedef struct listNode {
    struct listNode *prev;
    struct listNode *next;
    void *value;
} listNode;

//链表迭代器
typedef struct listIter {
    listNode *next;
    int direction;
} listIter;

//链表
typedef struct list {
    listNode *head;
    listNode *tail;

    //value拷贝函数
    void *(*dup)(void *ptr);

    //value释放函数
    void (*free)(void *ptr);

    //value比较函数
    int (*match)(void *ptr, void *key);

    //节点个数
    unsigned long len;
} list;

/* Functions implemented as macros */
//返回节点个数
#define listLength(l) ((l)->len)

//返回头节点
#define listFirst(l) ((l)->head)

//返回尾节点
#define listLast(l) ((l)->tail)

//返回上一个节点
#define listPrevNode(n) ((n)->prev)

//返回下一个节点
#define listNextNode(n) ((n)->next)

//返回节点value
#define listNodeValue(n) ((n)->value)

//设定拷贝函数
#define listSetDupMethod(l, m) ((l)->dup = (m))

//设定释放函数
#define listSetFreeMethod(l, m) ((l)->free = (m))

//设定比较函数
#define listSetMatchMethod(l, m) ((l)->match = (m))

#define listGetDupMethod(l) ((l)->dup)
#define listGetFree(l) ((l)->free)
#define listGetMatchMethod(l) ((l)->match)

/* Prototypes */
//创建新链表
list *listCreate(void);

//释放链表
void listRelease(list *list);

//在头部插入新节点
list *listAddNodeHead(list *list, void *value);

//在尾部插入新节点
list *listAddNodeTail(list *list, void *value);

//在old_node之前/之后插入新节点
list *listInsertNode(list *list, listNode *old_node, void *value, int after);

//删除node节点
void listDelNode(list *list, listNode *node);

//创建方向为direction的链表迭代器
listIter *listGetIterator(list *list, int direction);

//返回迭代器的下一个节点
listNode *listNext(listIter *iter);

//释放迭代器
void listReleaseIterator(listIter *iter);

//拷贝链表
list *listDup(list *orig);

//在链表中搜寻key
listNode *listSearchKey(list *list, void *key);

//返回第index个节点，index < 0则从表尾开始
listNode *listIndex(list *list, long index);

//绑定现有迭代器到list，遍历方向从头到尾
void listRewind(list *list, listIter *li);

//绑定现有迭代器到list，遍历方向从尾到头
void listRewindTail(list *list, listIter *li);

//将链表尾节点移动到最前面
void listRotate(list *list);

/* Directions for iterators */
#define AL_START_HEAD 0
#define AL_START_TAIL 1

#endif /* __ADLIST_H__ */
