/* Hash Tables Implementation.
 *
 * This file implements in memory hash tables with insert/del/replace/find/
 * get-random-element operations. Hash tables will auto resize if needed
 * tables of power of two in size are used, collisions are handled by
 * chaining. See the source code for more information... :)
 *
 * Copyright (c) 2006-2012, Salvatore Sanfilippo <antirez at gmail dot com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Redis nor the names of its contributors may be used
 *     to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "fmacros.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <limits.h>
#include <sys/time.h>
#include <ctype.h>

#include "dict.h"
#include "zmalloc.h"
#include "redisassert.h"

/* Using dictEnableResize() / dictDisableResize() we make possible to
 * enable/disable resizing of the hash table as needed. This is very important
 * for Redis, as we use copy-on-write and don't want to move too much memory
 * around when there is a child performing saving operations.
 *
 * Note that even when dict_can_resize is set to 0, not all resizes are
 * prevented: a hash table is still allowed to grow if the ratio between
 * the number of elements and the buckets > dict_force_resize_ratio. */
//是否允许重哈希
//当有子进程时为了不触发cow，通常不进行重哈希
//但是当负载因子超过dict_force_resize_ratio时还是会强制重哈希
static int dict_can_resize = 1;
static unsigned int dict_force_resize_ratio = 5;

/* -------------------------- private prototypes ---------------------------- */

static int _dictExpandIfNeeded(dict *ht);

static unsigned long _dictNextPower(unsigned long size);

static int _dictKeyIndex(dict *ht, const void *key);

static int _dictInit(dict *ht, dictType *type, void *privDataPtr);

/* -------------------------- hash functions -------------------------------- */

/* Thomas Wang's 32 bit Mix Function */
unsigned int dictIntHashFunction(unsigned int key) {
    key += ~(key << 15);
    key ^= (key >> 10);
    key += (key << 3);
    key ^= (key >> 6);
    key += ~(key << 11);
    key ^= (key >> 16);
    return key;
}

static uint32_t dict_hash_function_seed = 5381;

void dictSetHashFunctionSeed(uint32_t seed) {
    dict_hash_function_seed = seed;
}

uint32_t dictGetHashFunctionSeed(void) {
    return dict_hash_function_seed;
}

/* MurmurHash2, by Austin Appleby
 * Note - This code makes a few assumptions about how your machine behaves -
 * 1. We can read a 4-byte value from any address without crashing
 * 2. sizeof(int) == 4
 *
 * And it has a few limitations -
 *
 * 1. It will not work incrementally.
 * 2. It will not produce the same results on little-endian and big-endian
 *    machines.
 */
//murmurhash2哈希算法
unsigned int dictGenHashFunction(const void *key, int len) {
    /* 'm' and 'r' are mixing constants generated offline.
     They're not really 'magic', they just happen to work well.  */
    uint32_t seed = dict_hash_function_seed;
    const uint32_t m = 0x5bd1e995;
    const int r = 24;

    /* Initialize the hash to a 'random' value */
    uint32_t h = seed ^len;

    /* Mix 4 bytes at a time into the hash */
    const unsigned char *data = (const unsigned char *) key;

    while (len >= 4) {
        uint32_t k = *(uint32_t *) data;

        k *= m;
        k ^= k >> r;
        k *= m;

        h *= m;
        h ^= k;

        data += 4;
        len -= 4;
    }

    /* Handle the last few bytes of the input array  */
    switch (len) {
        case 3:
            h ^= data[2] << 16;
        case 2:
            h ^= data[1] << 8;
        case 1:
            h ^= data[0];
            h *= m;
    };

    /* Do a few final mixes of the hash to ensure the last few
     * bytes are well-incorporated. */
    h ^= h >> 13;
    h *= m;
    h ^= h >> 15;

    return (unsigned int) h;
}

/* And a case insensitive hash function (based on djb hash) */
//djb哈希算法
unsigned int dictGenCaseHashFunction(const unsigned char *buf, int len) {
    unsigned int hash = (unsigned int) dict_hash_function_seed;

    while (len--)
        hash = ((hash << 5) + hash) + (tolower(*buf++)); /* hash * 33 + c */
    return hash;
}

/* ----------------------------- API implementation ------------------------- */

/* Reset a hash table already initialized with ht_init().
 * NOTE: This function should only be called by ht_destroy(). */
//初始化哈希表
static void _dictReset(dictht *ht) {
    ht->table = NULL;
    ht->size = 0;
    ht->sizemask = 0;
    ht->used = 0;
}

/* Create a new hash table */
//创建新字典
dict *dictCreate(dictType *type, void *privDataPtr) {
    dict *d = zmalloc(sizeof(*d));
    _dictInit(d, type, privDataPtr);
    return d;
}

/* Initialize the hash table */
//初始化字典
int _dictInit(dict *d, dictType *type, void *privDataPtr) {
    _dictReset(&d->ht[0]);
    _dictReset(&d->ht[1]);
    d->type = type;
    d->privdata = privDataPtr;
    d->rehashidx = -1;
    d->iterators = 0;
    return DICT_OK;
}

/* Resize the table to the minimal size that contains all the elements,
 * but with the invariant of a USED/BUCKETS ratio near to <= 1 */
//调整字典大小，通常是缩小
//被hash，set等类型调用
int dictResize(dict *d) {
    int minimal;

    //不允许重哈希或正在进行重哈希
    if (!dict_can_resize || dictIsRehashing(d)) {
        return DICT_ERR;
    }

    //计算需要的size大小
    minimal = d->ht[0].used;
    if (minimal < DICT_HT_INITIAL_SIZE) {
        minimal = DICT_HT_INITIAL_SIZE;
    }

    return dictExpand(d, minimal);
}

/* Expand or create the hash table */
//重哈希字典，可能是变小也可能是变大
int dictExpand(dict *d, unsigned long size) {
    dictht n; /* the new hash table */

    //实际要扩张的大小
    unsigned long realsize = _dictNextPower(size);

    /* the size is invalid if it is smaller than the number of
     * elements already inside the hash table */
    if (dictIsRehashing(d) || d->ht[0].used > size) {
        return DICT_ERR;
    }

    /* Rehashing to the same table size is not useful. */
    //要扩张的大小和不能现有大小相同
    if (realsize == d->ht[0].size) {
        return DICT_ERR;
    }

    /* Allocate the new hash table and initialize all pointers to NULL */
    //初始化哈希表各项属性
    //同时创建table数组
    n.size = realsize;
    n.sizemask = realsize - 1;
    n.table = zcalloc(realsize * sizeof(dictEntry *));
    n.used = 0;

    /* Is this the first initialization? If so it's not really a rehashing
     * we just set the first hash table so that it can accept keys. */
    //0号哈希表table为NULL说明这是初始化，不需要重哈希，将哈希表赋给0号哈希表
    //具体例子比如t_set.c的setTypeConvert函数
    if (d->ht[0].table == NULL) {
        d->ht[0] = n;
        return DICT_OK;
    }

    /* Prepare a second hash table for incremental rehashing */
    //否则赋给1号哈希表，进行重哈希
    d->ht[1] = n;
    d->rehashidx = 0;

    return DICT_OK;
}

/* Performs N steps of incremental rehashing. Returns 1 if there are still
 * keys to move from the old to the new hash table, otherwise 0 is returned.
 *
 * Note that a rehashing step consists in moving a bucket (that may have more
 * than one key as we use chaining) from the old to the new hash table, however
 * since part of the hash table may be composed of empty spaces, it is not
 * guaranteed that this function will rehash even a single bucket, since it
 * will visit at max N*10 empty buckets in total, otherwise the amount of
 * work it does would be unbound and the function may block for a long time. */
//进行n步重哈希
//"步"指不为空的bucket
int dictRehash(dict *d, int n) {
    int empty_visits = n * 10; /* Max number of empty buckets to visit. */
    if (!dictIsRehashing(d)) {
        return 0;
    }

    while (n-- && d->ht[0].used != 0) {
        dictEntry *de, *nextde;

        /* Note that rehashidx can't overflow as we are sure there are more
         * elements because ht[0].used != 0 */
        //rehashidx理应不会越界，因为while循环会检查used!=0
        assert(d->ht[0].size > (unsigned long) d->rehashidx);

        //寻找不为空的bucket
        while (d->ht[0].table[d->rehashidx] == NULL) {
            d->rehashidx++;
            //当遇到的空bucket超过一定数量后，强制停止重哈希，避免函数运行太长时间
            if (--empty_visits == 0) {
                return 1;
            }
        }

        //非空bucket
        de = d->ht[0].table[d->rehashidx];
        /* Move all the keys in this bucket from the old to the new hash HT */
        //将bucket的元素重哈希到1号哈希表
        while (de) {
            unsigned int h;

            nextde = de->next;
            /* Get the index in the new hash table */
            //计算元素在1号哈希表的索引
            h = dictHashKey(d, de->key) & d->ht[1].sizemask;
            de->next = d->ht[1].table[h];
            d->ht[1].table[h] = de;
            d->ht[0].used--;
            d->ht[1].used++;
            de = nextde;
        }

        //置当前bucket为空
        d->ht[0].table[d->rehashidx] = NULL;
        //更新重哈希当前索引
        d->rehashidx++;
    }

    /* Check if we already rehashed the whole table... */
    //是否已经重哈希完毕？
    if (d->ht[0].used == 0) {
        //释放0号哈希表的table
        zfree(d->ht[0].table);
        d->ht[0] = d->ht[1];
        _dictReset(&d->ht[1]);
        d->rehashidx = -1;
        return 0;
    }

    /* More to rehash... */
    //尚未重哈希完成
    return 1;
}

//获取当前时间
long long timeInMilliseconds(void) {
    struct timeval tv;

    gettimeofday(&tv, NULL);
    return (((long long) tv.tv_sec) * 1000) + (tv.tv_usec / 1000);
}

/* Rehash for an amount of time between ms milliseconds and ms+1 milliseconds */
//在ms时间内进行步长为100的重哈希
//该函数被定时函数serverCron调用，对db和exipres进行重哈希
int dictRehashMilliseconds(dict *d, int ms) {
    long long start = timeInMilliseconds();
    int rehashes = 0;

    while (dictRehash(d, 100)) {
        rehashes += 100;
        //已经超过ms时间则停止重哈希
        if (timeInMilliseconds() - start > ms) {
            break;
        }
    }
    return rehashes;
}

/* This function performs just a step of rehashing, and only if there are
 * no safe iterators bound to our hash table. When we have iterators in the
 * middle of a rehashing we can't mess with the two hash tables otherwise
 * some element can be missed or duplicated.
 *
 * This function is called by common lookup or update operations in the
 * dictionary so that the hash table automatically migrates from H1 to H2
 * while it is actively used. */
//执行一次单步重哈希，在添加/获取/更新/删除key等操作时会调用该函数
static void _dictRehashStep(dict *d) {
    if (d->iterators == 0) {
        dictRehash(d, 1);
    }
}

/* Add an element to the target hash table */
//向字典添加新的元素
int dictAdd(dict *d, void *key, void *val) {
    dictEntry *entry = dictAddRaw(d, key);

    //key已经存在
    if (!entry) {
        return DICT_ERR;
    }
    //设置val
    dictSetVal(d, entry, val);

    return DICT_OK;
}

/* Low level add. This function adds the entry but instead of setting
 * a value returns the dictEntry structure to the user, that will make
 * sure to fill the value field as he wishes.
 *
 * This function is also directly exposed to the user API to be called
 * mainly in order to store non-pointers inside the hash value, example:
 *
 * entry = dictAddRaw(dict,mykey);
 * if (entry != NULL) dictSetSignedIntegerVal(entry,1000);
 *
 * Return values:
 *
 * If key already exists NULL is returned.
 * If key was added, the hash entry is returned to be manipulated by the caller.
 */
//向字典添加新元素
//只设置key，不设置val
dictEntry *dictAddRaw(dict *d, void *key) {
    int index;
    dictEntry *entry;
    dictht *ht;

    //进行单步重哈希
    if (dictIsRehashing(d)) {
        _dictRehashStep(d);
    }

    /* Get the index of the new element, or -1 if
     * the element already exists. */
    //计算key要插入到哪个bucket，如果key已经存在函数返回-1
    //该函数会同时检查是否需要扩大哈希表
    if ((index = _dictKeyIndex(d, key)) == -1) {
        return NULL;
    }

    /* Allocate the memory and store the new entry.
     * Insert the element in top, with the assumption that in a database
     * system it is more likely that recently added entries are accessed
     * more frequently. */
    //如果正在进行重哈希，新元素总是插入到1号哈希表
    ht = dictIsRehashing(d) ? &d->ht[1] : &d->ht[0];

    //表头插入
    entry = zmalloc(sizeof(*entry));
    entry->next = ht->table[index];
    ht->table[index] = entry;
    ht->used++;

    /* Set the hash entry fields. */
    //设置key
    dictSetKey(d, entry, key);

    return entry;
}

/* Add an element, discarding the old if the key already exists.
 * Return 1 if the key was added from scratch, 0 if there was already an
 * element with such key and dictReplace() just performed a value update
 * operation. */
//添加或更新元素val
int dictReplace(dict *d, void *key, void *val) {
    dictEntry *entry, auxentry;

    /* Try to add the element. If the key
     * does not exists dictAdd will suceed. */
    //尝试添加元素
    if (dictAdd(d, key, val) == DICT_OK) {
        return 1;
    }

    /* It already exists, get the entry */
    //到这里说明key已经存在
    //先获取entry
    entry = dictFind(d, key);
    /* Set the new value and free the old one. Note that it is important
     * to do that in this order, as the value may just be exactly the same
     * as the previous one. In this context, think to reference counting,
     * you want to increment (set), and then decrement (free), and not the
     * reverse. */
    //然后设置新的val并释放旧的val
    //注意，不能先释放旧的val再设置新的val，因为两个val可能是同一个
    auxentry = *entry;
    dictSetVal(d, entry, val);
    dictFreeVal(d, &auxentry);
    return 0;
}

/* dictReplaceRaw() is simply a version of dictAddRaw() that always
 * returns the hash entry of the specified key, even if the key already
 * exists and can't be added (in that case the entry of the already
 * existing key is returned.)
 *
 * See dictAddRaw() for more information. */
//尝试添加新元素，如果元素已经存在，则返回旧元素
dictEntry *dictReplaceRaw(dict *d, void *key) {
    dictEntry *entry = dictFind(d, key);

    return entry ? entry : dictAddRaw(d, key);
}

/* Search and remove an element */
//删除元素，nofree指示删除的时候是否释放key/val
static int dictGenericDelete(dict *d, const void *key, int nofree) {
    unsigned int h, idx;
    dictEntry *he, *prevHe;
    int table;

    //字典为空
    if (d->ht[0].size == 0) {
        return DICT_ERR;
    } /* d->ht[0].table is NULL */

    //进行单步重哈希
    if (dictIsRehashing(d)) {
        _dictRehashStep(d);
    }

    //计算key哈希值
    h = dictHashKey(d, key);

    for (table = 0; table <= 1; table++) {
        //key所在bucket
        idx = h & d->ht[table].sizemask;

        he = d->ht[table].table[idx];
        prevHe = NULL;

        //搜索key并删除
        while (he) {
            if (key == he->key || dictCompareKeys(d, key, he->key)) {
                /* Unlink the element from the list */
                //从链表移除
                if (prevHe) {
                    prevHe->next = he->next;
                }
                else {
                    d->ht[table].table[idx] = he->next;
                }
                //是否释放key/val？
                if (!nofree) {
                    dictFreeKey(d, he);
                    dictFreeVal(d, he);
                }
                //释放节点本身
                zfree(he);
                d->ht[table].used--;
                return DICT_OK;
            }
            prevHe = he;
            he = he->next;
        }

        //如果在进行重哈希，需要继续搜索1号哈希表，否则跳出
        if (!dictIsRehashing(d)) { break; }
    }

    return DICT_ERR; /* not found */
}

//删除元素，并释放key/val
int dictDelete(dict *ht, const void *key) {
    return dictGenericDelete(ht, key, 0);
}

//删除元素，不释放key/val
int dictDeleteNoFree(dict *ht, const void *key) {
    return dictGenericDelete(ht, key, 1);
}

/* Destroy an entire dictionary */
//释放哈希表
int _dictClear(dict *d, dictht *ht, void(callback)(void *)) {
    unsigned long i;

    /* Free all the elements */
    //释放哈希表的所有元素
    for (i = 0; i < ht->size && ht->used > 0; i++) {
        dictEntry *he, *nextHe;

        //todo
        if (callback && (i & 65535) == 0) { callback(d->privdata); }

        //bucket为空
        if ((he = ht->table[i]) == NULL) {
            continue;
        }

        //释放bucket
        while (he) {
            nextHe = he->next;
            dictFreeKey(d, he);
            dictFreeVal(d, he);
            zfree(he);
            ht->used--;
            he = nextHe;
        }
    }

    /* Free the table and the allocated cache structure */
    //释放table数组
    zfree(ht->table);

    /* Re-initialize the table */
    _dictReset(ht);
    return DICT_OK; /* never fails */
}

/* Clear & Release the hash table */
//释放字典
void dictRelease(dict *d) {
    //释放两个哈希表
    _dictClear(d, &d->ht[0], NULL);
    _dictClear(d, &d->ht[1], NULL);

    //释放字典本身
    zfree(d);
}

//获取key元素
dictEntry *dictFind(dict *d, const void *key) {
    dictEntry *he;
    unsigned int h, idx, table;

    //字典为空
    if (d->ht[0].used + d->ht[1].used == 0) {
        return NULL;
    } /* dict is empty */

    //进行单步重哈希
    if (dictIsRehashing(d)) {
        _dictRehashStep(d);
    }

    //搜索key
    h = dictHashKey(d, key);
    for (table = 0; table <= 1; table++) {
        idx = h & d->ht[table].sizemask;
        he = d->ht[table].table[idx];
        while (he) {
            if (key == he->key || dictCompareKeys(d, key, he->key)) {
                return he;
            }
            he = he->next;
        }

        //如果在进行重哈希，需要继续搜索1号哈希表，否则说明key不存在
        if (!dictIsRehashing(d)) { return NULL; }
    }

    //key不存在
    return NULL;
}

//获取key元素的val
void *dictFetchValue(dict *d, const void *key) {
    dictEntry *he;

    he = dictFind(d, key);
    return he ? dictGetVal(he) : NULL;
}

/* A fingerprint is a 64 bit number that represents the state of the dictionary
 * at a given time, it's just a few dict properties xored together.
 * When an unsafe iterator is initialized, we get the dict fingerprint, and check
 * the fingerprint again when the iterator is released.
 * If the two fingerprints are different it means that the user of the iterator
 * performed forbidden operations against the dictionary while iterating. */
//todo
long long dictFingerprint(dict *d) {
    long long integers[6], hash = 0;
    int j;

    integers[0] = (long) d->ht[0].table;
    integers[1] = d->ht[0].size;
    integers[2] = d->ht[0].used;
    integers[3] = (long) d->ht[1].table;
    integers[4] = d->ht[1].size;
    integers[5] = d->ht[1].used;

    /* We hash N integers by summing every successive integer with the integer
     * hashing of the previous sum. Basically:
     *
     * Result = hash(hash(hash(int1)+int2)+int3) ...
     *
     * This way the same set of integers in a different order will (likely) hash
     * to a different number. */
    for (j = 0; j < 6; j++) {
        hash += integers[j];
        /* For the hashing step we use Tomas Wang's 64 bit integer hash. */
        hash = (~hash) + (hash << 21); // hash = (hash << 21) - hash - 1;
        hash = hash ^ (hash >> 24);
        hash = (hash + (hash << 3)) + (hash << 8); // hash * 265
        hash = hash ^ (hash >> 14);
        hash = (hash + (hash << 2)) + (hash << 4); // hash * 21
        hash = hash ^ (hash >> 28);
        hash = hash + (hash << 31);
    }
    return hash;
}

//创建当前字典的迭代器
dictIterator *dictGetIterator(dict *d) {
    dictIterator *iter = zmalloc(sizeof(*iter));

    iter->d = d;
    iter->table = 0;
    iter->index = -1;
    iter->safe = 0;
    iter->entry = NULL;
    iter->nextEntry = NULL;
    return iter;
}

//创建当前字典的安全迭代器
dictIterator *dictGetSafeIterator(dict *d) {
    dictIterator *i = dictGetIterator(d);

    i->safe = 1;
    return i;
}

//返回迭代器的下一个元素
dictEntry *dictNext(dictIterator *iter) {
    while (1) {
        //当前entry为NULL
        //可能是刚开始迭代，也可能是当前bucket迭代完毕，要开始迭代下一个bucket
        if (iter->entry == NULL) {
            dictht *ht = &iter->d->ht[iter->table];

            //迭代刚刚开始
            if (iter->index == -1 && iter->table == 0) {
                if (iter->safe) {
                    iter->d->iterators++;
                }
                else {
                    iter->fingerprint = dictFingerprint(iter->d);
                }
            }

            //指向下一个bucket
            iter->index++;

            //当前哈希表已经迭代完毕
            //如果当前迭代的是0号哈希表且正在进行重哈希
            //则开始迭代1号哈希表
            if (iter->index >= (long) ht->size) {
                if (dictIsRehashing(iter->d) && iter->table == 0) {
                    iter->table++;
                    iter->index = 0;
                    ht = &iter->d->ht[1];
                }
                else {
                    break;
                }
            }
            iter->entry = ht->table[iter->index];
        }
        else {
            //否则指向bucket的下一个元素
            iter->entry = iter->nextEntry;
        }

        if (iter->entry) {
            /* We need to save the 'next' here, the iterator user
             * may delete the entry we are returning. */
            //因为迭代过程可以修改字典，所有需要保存下一个元素
            iter->nextEntry = iter->entry->next;
            return iter->entry;
        }
    }

    return NULL;
}

//释放迭代器
void dictReleaseIterator(dictIterator *iter) {
    if (!(iter->index == -1 && iter->table == 0)) {
        if (iter->safe) {
            iter->d->iterators--;
        }
        else
            assert(iter->fingerprint == dictFingerprint(iter->d));
    }
    zfree(iter);
}

/* Return a random entry from the hash table. Useful to
 * implement randomized algorithms */
//随机返回一个元素
dictEntry *dictGetRandomKey(dict *d) {
    dictEntry *he, *orighe;
    unsigned int h;
    int listlen, listele;

    //字典为空
    if (dictSize(d) == 0) {
        return NULL;
    }

    //进行单步重哈希
    if (dictIsRehashing(d)) {
        _dictRehashStep(d);
    }

    //随机获取一个非空的bucket
    if (dictIsRehashing(d)) {
        do {
            /* We are sure there are no elements in indexes from 0
             * to rehashidx-1 */
            //如果正在进行重哈希，随机bucket从两个哈希表中获取
            //跳过0号哈希表rehashidx之前的bucket，因为这部分bucket已经重哈希到1号哈希表了
            h = d->rehashidx + (random() % (d->ht[0].size + d->ht[1].size - d->rehashidx));
            he = (h >= d->ht[0].size) ? d->ht[1].table[h - d->ht[0].size] :
                 d->ht[0].table[h];
        } while (he == NULL);
    }
    else {
        do {
            h = random() & d->ht[0].sizemask;
            he = d->ht[0].table[h];
        } while (he == NULL);
    }

    /* Now we found a non empty bucket, but it is a linked
     * list and we need to get a random element from the list.
     * The only sane way to do so is counting the elements and
     * select a random index. */
    //返回链表上的一个随机元素

    listlen = 0;
    orighe = he;

    //计算链表长度
    while (he) {
        he = he->next;
        listlen++;
    }

    //随机元素索引
    listele = random() % listlen;

    //获取随机元素
    he = orighe;
    while (listele--)
        he = he->next;

    return he;
}

/* This function samples the dictionary to return a few keys from random
 * locations.
 *
 * It does not guarantee to return all the keys specified in 'count', nor
 * it does guarantee to return non-duplicated elements, however it will make
 * some effort to do both things.
 *
 * Returned pointers to hash table entries are stored into 'des' that
 * points to an array of dictEntry pointers. The array must have room for
 * at least 'count' elements, that is the argument we pass to the function
 * to tell how many random elements we need.
 *
 * The function returns the number of items stored into 'des', that may
 * be less than 'count' if the hash table has less than 'count' elements
 * inside, or if not enough elements were found in a reasonable amount of
 * steps.
 *
 * Note that this function is not suitable when you need a good distribution
 * of the returned items, but only when you need to "sample" a given number
 * of continuous elements to run some kind of algorithm or to produce
 * statistics. However the function is much faster than dictGetRandomKey()
 * at producing N elements. */
// 从dict中随机返回count个元素
// 这个函数无法保证获取的元素是不重复的
// 也无法保证一定能返回count个元素
unsigned int dictGetSomeKeys(dict *d, dictEntry **des, unsigned int count) {
    unsigned long j; /* internal hash table id, 0 or 1. */

    // 需要的哈希表数目
    unsigned long tables; /* 1 or 2 tables? */

    // 实际返回的元素数量
    unsigned long stored = 0;

    // 掩码，如果使用两个哈希表，取其中更大的一个
    unsigned long maxsizemask;

    // 最大进行的趟数
    unsigned long maxsteps;

    // count只能取小于等于dict的大小
    if (dictSize(d) < count) {
        count = dictSize(d);
    }

    maxsteps = count * 10;

    /* Try to do a rehashing work proportional to 'count'. */
    // 进行count次单步重哈希
    for (j = 0; j < count; j++) {
        if (dictIsRehashing(d)) {
            _dictRehashStep(d);
        }
        else {
            break;
        }
    }

    // 需要的哈希表数目
    tables = dictIsRehashing(d) ? 2 : 1;

    // sizemask取两张哈希表中较大的那一个
    maxsizemask = d->ht[0].sizemask;
    if (tables > 1 && maxsizemask < d->ht[1].sizemask) {
        maxsizemask = d->ht[1].sizemask;
    }

    /* Pick a random point inside the larger table. */
    // 计算随机索引
    unsigned long i = random() & maxsizemask;

    // 连续遇到的空bucket数目
    unsigned long emptylen = 0; /* Continuous empty entries so far. */

    // 进行最多maxsteps趟或直到stored的元素已经等于count
    // 每一趟从一个 or 两个哈希表中，将当前索引i的bucket中的元素加入des
    // 如果store等于count，函数返回
    // 如果bucket为空，emptylen + 1
    // 一趟结束后，随机索引i + 1
    // 一趟中可能没有加入任何元素到des中
    // 如果连续遇到5个空bucket且emptylen > count，重新计算随机索引
    while (stored < count && maxsteps--) {
        for (j = 0; j < tables; j++) {
            /* Invariant of the dict.c rehashing: up to the indexes already
             * visited in ht[0] during the rehashing, there are no populated
             * buckets, so we can skip ht[0] for indexes between 0 and idx-1. */
            // 如果有两个哈希表且当前正处于0号哈希表且随机索引小于rehashidx
            // 因为0号哈希表小于rehashidx的bucket肯定是空的
            // 所以直接跳到1号哈希表
            if (tables == 2 && j == 0 && i < (unsigned long) d->rehashidx) {
                /* Moreover, if we are currently out of range in the second
                 * table, there will be no elements in both tables up to
                 * the current rehashing index, so we jump if possible.
                 * (this happens when going from big to small table). */
                // 但是如果索引i大于等于1号哈希表的size
                // 那么令i=rehashidx，尝试将0号哈希表索引为rehashidx的bucket中的元素加入store
                // 这种情况发生在缩小dict的时候
                if (i >= d->ht[1].size) {
                    i = d->rehashidx;
                }
                else {
                    continue;
                }
            }

            // 索引i超过该哈希表的size，跳过该哈希表
            // 只有一个哈希表时，不会出现这种情况
            // 假如索引i大于0号哈希表size，那么在i大于等于1号哈希表size之前
            // 因为每趟结束后i + 1，那么随机元素将一直只从1号哈希表获取
            // 如果索引i大于1号哈希表size，也是同理，随机元素将只从0号哈希表获取
            // 除非连续遇到多个空bucket导致重新计算索引i
            if (i >= d->ht[j].size) {
                continue;
            } /* Out of range for this table. */

            dictEntry *he = d->ht[j].table[i];

            /* Count contiguous empty buckets, and jump to other
             * locations if they reach 'count' (with a minimum of 5). */
            if (he == NULL) {
                emptylen++;
                // 连续遇到5个空bucket且emptylen > count，重新计算随机索引
                if (emptylen >= 5 && emptylen > count) {
                    i = random() & maxsizemask;
                    emptylen = 0;
                }
            }
            else {
                emptylen = 0;

                // 将当前bucket的所有元素加入des
                while (he) {
                    /* Collect all the elements of the buckets found non
                     * empty while iterating. */
                    *des = he;
                    des++;
                    he = he->next;
                    stored++;

                    // 达到count的要求了，返回stored
                    if (stored == count) {
                        return stored;
                    }
                }
            }
        }

        // 一趟结束，索引+1
        i = (i + 1) & maxsizemask;
    }

    return stored;
}

/* Function to reverse bits. Algorithm from:
 * http://graphics.stanford.edu/~seander/bithacks.html#ReverseParallel */
//todo
static unsigned long rev(unsigned long v) {
    unsigned long s = 8 * sizeof(v); // bit size; must be power of 2
    unsigned long mask = ~0;
    while ((s >>= 1) > 0) {
        mask ^= (mask << s);
        v = ((v >> s) & mask) | ((v << s) & ~mask);
    }
    return v;
}

/* dictScan() is used to iterate over the elements of a dictionary.
 *
 * Iterating works the following way:
 *
 * 1) Initially you call the function using a cursor (v) value of 0.
 * 2) The function performs one step of the iteration, and returns the
 *    new cursor value you must use in the next call.
 * 3) When the returned cursor is 0, the iteration is complete.
 *
 * The function guarantees all elements present in the
 * dictionary get returned between the start and end of the iteration.
 * However it is possible some elements get returned multiple times.
 *
 * For every element returned, the callback argument 'fn' is
 * called with 'privdata' as first argument and the dictionary entry
 * 'de' as second argument.
 *
 * HOW IT WORKS.
 *
 * The iteration algorithm was designed by Pieter Noordhuis.
 * The main idea is to increment a cursor starting from the higher order
 * bits. That is, instead of incrementing the cursor normally, the bits
 * of the cursor are reversed, then the cursor is incremented, and finally
 * the bits are reversed again.
 *
 * This strategy is needed because the hash table may be resized between
 * iteration calls.
 *
 * dict.c hash tables are always power of two in size, and they
 * use chaining, so the position of an element in a given table is given
 * by computing the bitwise AND between Hash(key) and SIZE-1
 * (where SIZE-1 is always the mask that is equivalent to taking the rest
 *  of the division between the Hash of the key and SIZE).
 *
 * For example if the current hash table size is 16, the mask is
 * (in binary) 1111. The position of a key in the hash table will always be
 * the last four bits of the hash output, and so forth.
 *
 * WHAT HAPPENS IF THE TABLE CHANGES IN SIZE?
 *
 * If the hash table grows, elements can go anywhere in one multiple of
 * the old bucket: for example let's say we already iterated with
 * a 4 bit cursor 1100 (the mask is 1111 because hash table size = 16).
 *
 * If the hash table will be resized to 64 elements, then the new mask will
 * be 111111. The new buckets you obtain by substituting in ??1100
 * with either 0 or 1 can be targeted only by keys we already visited
 * when scanning the bucket 1100 in the smaller hash table.
 *
 * By iterating the higher bits first, because of the inverted counter, the
 * cursor does not need to restart if the table size gets bigger. It will
 * continue iterating using cursors without '1100' at the end, and also
 * without any other combination of the final 4 bits already explored.
 *
 * Similarly when the table size shrinks over time, for example going from
 * 16 to 8, if a combination of the lower three bits (the mask for size 8
 * is 111) were already completely explored, it would not be visited again
 * because we are sure we tried, for example, both 0111 and 1111 (all the
 * variations of the higher bit) so we don't need to test it again.
 *
 * WAIT... YOU HAVE *TWO* TABLES DURING REHASHING!
 *
 * Yes, this is true, but we always iterate the smaller table first, then
 * we test all the expansions of the current cursor into the larger
 * table. For example if the current cursor is 101 and we also have a
 * larger table of size 16, we also test (0)101 and (1)101 inside the larger
 * table. This reduces the problem back to having only one table, where
 * the larger one, if it exists, is just an expansion of the smaller one.
 *
 * LIMITATIONS
 *
 * This iterator is completely stateless, and this is a huge advantage,
 * including no additional memory used.
 *
 * The disadvantages resulting from this design are:
 *
 * 1) It is possible we return elements more than once. However this is usually
 *    easy to deal with in the application level.
 * 2) The iterator must return multiple elements per call, as it needs to always
 *    return all the keys chained in a given bucket, and all the expansions, so
 *    we are sure we don't miss keys moving during rehashing.
 * 3) The reverse cursor is somewhat hard to understand at first, but this
 *    comment is supposed to help.
 */
//todo
unsigned long dictScan(dict *d,
                       unsigned long v,
                       dictScanFunction *fn,
                       void *privdata) {
    dictht *t0, *t1;
    const dictEntry *de;
    unsigned long m0, m1;

    if (dictSize(d) == 0) { return 0; }

    if (!dictIsRehashing(d)) {
        t0 = &(d->ht[0]);
        m0 = t0->sizemask;

        /* Emit entries at cursor */
        de = t0->table[v & m0];
        while (de) {
            fn(privdata, de);
            de = de->next;
        }

        /* Set unmasked bits so incrementing the reversed cursor
         * operates on the masked bits */
        v |= ~m0;

        /* Increment the reverse cursor */
        v = rev(v);
        v++;
        v = rev(v);

    }
    else {
        t0 = &d->ht[0];
        t1 = &d->ht[1];

        /* Make sure t0 is the smaller and t1 is the bigger table */
        if (t0->size > t1->size) {
            t0 = &d->ht[1];
            t1 = &d->ht[0];
        }

        m0 = t0->sizemask;
        m1 = t1->sizemask;

        /* Emit entries at cursor */
        de = t0->table[v & m0];
        while (de) {
            fn(privdata, de);
            de = de->next;
        }

        /* Iterate over indices in larger table that are the expansion
         * of the index pointed to by the cursor in the smaller table */
        do {
            /* Emit entries at cursor */
            de = t1->table[v & m1];
            while (de) {
                fn(privdata, de);
                de = de->next;
            }

            /* Increment the reverse cursor not covered by the smaller mask.*/
            v |= ~m1;
            v = rev(v);
            v++;
            v = rev(v);

            /* Continue while bits covered by mask difference is non-zero */
        } while (v & (m0 ^ m1));
    }

    return v;
}

/* ------------------------- private functions ------------------------------ */

/* Expand the hash table if needed */
//检查是否需要扩大字典
static int _dictExpandIfNeeded(dict *d) {
    /* Incremental rehashing already in progress. Return. */
    //已经在进行重哈希？
    if (dictIsRehashing(d)) {
        return DICT_OK;
    }

    /* If the hash table is empty expand it to the initial size. */
    if (d->ht[0].size == 0) {
        return dictExpand(d, DICT_HT_INITIAL_SIZE);
    }

    /* If we reached the 1:1 ratio, and we are allowed to resize the hash
     * table (global setting) or we should avoid it but the ratio between
     * elements/buckets is over the "safe" threshold, we resize doubling
     * the number of buckets. */
    //如果允许重哈希且负载因子大于等于1
    //或者不允许重哈希，但是负载因子大于dict_force_resize_ratio
    //则扩大哈希表，进行重哈希
    if (d->ht[0].used >= d->ht[0].size &&
        (dict_can_resize ||
         d->ht[0].used / d->ht[0].size > dict_force_resize_ratio)) {
        return dictExpand(d, d->ht[0].used * 2);
    }

    return DICT_OK;
}

/* Our hash table capability is a power of two */
//要扩张的哈希表大小，是第一个大于等于实际所需大小的2^n
static unsigned long _dictNextPower(unsigned long size) {
    unsigned long i = DICT_HT_INITIAL_SIZE;

    if (size >= LONG_MAX) {
        return LONG_MAX + 1LU;
    }

    while (1) {
        if (i >= size) {
            return i;
        }
        i *= 2;
    }
}

/* Returns the index of a free slot that can be populated with
 * a hash entry for the given 'key'.
 * If the key already exists, -1 is returned.
 *
 * Note that if we are in the process of rehashing the hash table, the
 * index is always returned in the context of the second (new) hash table. */
//计算key要插入到哪个bucket，同时会检查是否需要扩大字典
//该函数似乎只被dictAddRaw调用，也即只有在添加元素的时候检查是否需要扩大字典
static int _dictKeyIndex(dict *d, const void *key) {
    unsigned int h, idx, table;
    dictEntry *he;

    /* Expand the hash table if needed */
    //检查是否需要扩大字典
    if (_dictExpandIfNeeded(d) == DICT_ERR) {
        return -1;
    }

    /* Compute the key hash value */
    //计算key哈希值
    h = dictHashKey(d, key);

    for (table = 0; table <= 1; table++) {
        //计算bucket索引
        idx = h & d->ht[table].sizemask;
        /* Search if this slot does not already contain the given key */
        //检查key是否已经存在
        he = d->ht[table].table[idx];
        while (he) {
            if (key == he->key || dictCompareKeys(d, key, he->key)) {
                return -1;
            }
            he = he->next;
        }

        //如果当前正在进行重哈希，则需要继续检查1号哈希表，否则跳出
        if (!dictIsRehashing(d)) { break; }
    }

    return idx;
}

//清空字典
//只是释放了哈希表，没有释放字典
void dictEmpty(dict *d, void(callback)(void *)) {
    _dictClear(d, &d->ht[0], callback);
    _dictClear(d, &d->ht[1], callback);
    d->rehashidx = -1;
    d->iterators = 0;
}

//允许重哈希
//该函数及下面的函数被updateDictResizePolicy调用
//执行aof重写/保存rdb文件/serverCron都会调用updateDictResizePolicy函数
//updateDictResizePolicy检查当前是否有aof/rdb相关的子进程并更新重哈希选项
void dictEnableResize(void) {
    dict_can_resize = 1;
}

//禁止重哈希
void dictDisableResize(void) {
    dict_can_resize = 0;
}

/* ------------------------------- Debugging ---------------------------------*/

#define DICT_STATS_VECTLEN 50

//todo
size_t _dictGetStatsHt(char *buf, size_t bufsize, dictht *ht, int tableid) {
    unsigned long i, slots = 0, chainlen, maxchainlen = 0;
    unsigned long totchainlen = 0;
    unsigned long clvector[DICT_STATS_VECTLEN];
    size_t l = 0;

    if (ht->used == 0) {
        return snprintf(buf, bufsize,
                        "No stats available for empty dictionaries\n");
    }

    /* Compute stats. */
    for (i = 0; i < DICT_STATS_VECTLEN; i++) clvector[i] = 0;
    for (i = 0; i < ht->size; i++) {
        dictEntry *he;

        if (ht->table[i] == NULL) {
            clvector[0]++;
            continue;
        }
        slots++;
        /* For each hash entry on this slot... */
        chainlen = 0;
        he = ht->table[i];
        while (he) {
            chainlen++;
            he = he->next;
        }
        clvector[(chainlen < DICT_STATS_VECTLEN) ? chainlen : (DICT_STATS_VECTLEN - 1)]++;
        if (chainlen > maxchainlen) { maxchainlen = chainlen; }
        totchainlen += chainlen;
    }

    /* Generate human readable stats. */
    l += snprintf(buf + l, bufsize - l,
                  "Hash table %d stats (%s):\n"
                  " table size: %ld\n"
                  " number of elements: %ld\n"
                  " different slots: %ld\n"
                  " max chain length: %ld\n"
                  " avg chain length (counted): %.02f\n"
                  " avg chain length (computed): %.02f\n"
                  " Chain length distribution:\n",
                  tableid, (tableid == 0) ? "main hash table" : "rehashing target",
                  ht->size, ht->used, slots, maxchainlen,
                  (float) totchainlen / slots, (float) ht->used / slots);

    for (i = 0; i < DICT_STATS_VECTLEN - 1; i++) {
        if (clvector[i] == 0) { continue; }
        if (l >= bufsize) { break; }
        l += snprintf(buf + l, bufsize - l,
                      "   %s%ld: %ld (%.02f%%)\n",
                      (i == DICT_STATS_VECTLEN - 1) ? ">= " : "",
                      i, clvector[i], ((float) clvector[i] / ht->size) * 100);
    }

    /* Unlike snprintf(), teturn the number of characters actually written. */
    if (bufsize) { buf[bufsize - 1] = '\0'; }
    return strlen(buf);
}

//todo
void dictGetStats(char *buf, size_t bufsize, dict *d) {
    size_t l;
    char *orig_buf = buf;
    size_t orig_bufsize = bufsize;

    l = _dictGetStatsHt(buf, bufsize, &d->ht[0], 0);
    buf += l;
    bufsize -= l;
    if (dictIsRehashing(d) && bufsize > 0) {
        _dictGetStatsHt(buf, bufsize, &d->ht[1], 1);
    }
    /* Make sure there is a NULL term at the end. */
    if (orig_bufsize) { orig_buf[orig_bufsize - 1] = '\0'; }
}
